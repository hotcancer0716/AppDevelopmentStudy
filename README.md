# Android应用开发学习代码集合

介绍
学习Android开发的代码，基于AndroidStudio
View_Demo是Android基础控件和布局，包括TextView，Edittext，ImageView，Button，RadioButton，Checkbox，ProgressBar和拖动条。
Adapter_Demo 是Adapter相关的代码，包括Spinner，AutoCompleteTextView，ListView和GridView.

软件架构
软件架构说明


#### 安装教程

1.  View_Demo           安卓基础控件，包括TextView，Edittext，ImageView，Button，RadioButton，Checkbox，ProgressBar和拖动条
2.  Adapter_Demo        基于适配器的安卓控件，包括Spinner，AutoCompleteTextView，ListView和GridView.
3.  Dialog_Test         对话框，警告对话框，进度条对话框，日期和时间对话框
4.  Login_app           简单的登录验证测试
5.  
6.  DataStorage_Demo    数据存储测试，包括文件存储和读取。SharedPreference。
7.  SQLliteDemo         数据库，数据库的增删改查。
8.  ThreadSQLite        基于SQLite和Thread以及MaterDesign实现的学籍管理
 
使用说明

1.  xxxx
2.  xxxx
3.  xxxx

参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
