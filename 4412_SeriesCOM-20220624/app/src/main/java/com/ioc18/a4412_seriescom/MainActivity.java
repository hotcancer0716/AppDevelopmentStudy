package com.ioc18.a4412_seriescom;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Switch;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;

import casio.serial.SerialPort;

public class MainActivity extends AppCompatActivity {

    final String  TTY_DEV = "/dev/ttySAC1";
    final int 	  bps = 115200;
    SerialPort mSerialPort = null;		        //串口设备描述
    protected OutputStream mOutputStream;		//串口输出描述
    private InputStream mInputStream;
    Auto_Weight weight_thread = new Auto_Weight();
    boolean     auto_start = true;
    int         search_interval = 800;	//延时时间

    TextView tx_debug,tx_translate,tx_guangzhao,tx_chaoshengbo,tx_rentiganying;
    TextView tx_kerenqiti,tx_co;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findID();
        weight_thread.start();

    }

    private void findID() {
        tx_debug = findViewById(R.id.tx_debug);
        tx_translate = findViewById(R.id.tx_translate);
        tx_guangzhao = findViewById(R.id.guangzhao);
        tx_chaoshengbo = findViewById(R.id.chaoshengbo);
        tx_rentiganying = findViewById(R.id.rentiganying);
        tx_kerenqiti = findViewById(R.id.keranqiti);
        tx_co = findViewById(R.id.co);

    }

    class Auto_Weight extends Thread{
        @Override
        public void run(){
            try {
                mSerialPort = new SerialPort(new File(TTY_DEV), bps, 0);
                mOutputStream = mSerialPort.getOutputStream();
                mInputStream  = mSerialPort.getInputStream();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            int buf_num = 0;
            byte[] receive_buf = new byte[20]; 			//组织接收
            boolean receive_state = false;
            String guangzhao = "0";
            String chaoshengbo = "0";
            String rentiganying = "没人";
            String kernqiti = "0";
            String Co = "0";


            while(true)
            {
                if (auto_start) {
                    //每次接收一个字节,然后甩出去!累计13个字节,组成一个字符串
                    byte[] buf = new byte[1]; 			//组织接收
                    int size;
                    try {
                        size = mInputStream.read(buf);
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    //如果获取到了0x02,开始记录接下来的13个字节,最后组成字符串
                    if (receive_state || buf[0]== 0x02 ) {
                        receive_buf[buf_num] = (byte) (0xff&buf[0]);
                        buf_num ++;
                        receive_state = true;
                        if (buf_num == 13) {
                            String receice_string = byte2HexStr(receive_buf);
                            String receice_dev = "Zigbee";
                            Log.d("MMMMMMMMMM",receice_string);

                            //数据解析的部分如下：
                            //首先判断是来自哪个网关，wifi？Rola？Zigbee？BlueTooth？
                            //通过[2]和[3]来判断。
                            if (receive_buf[2] == (byte) 0xB8){
                                receice_dev = "Zigbee";
                                if (receive_buf[10] == (byte)0x06) {
                                    int smoke = Integer.parseInt(String.valueOf(receive_buf[11]));
                                    receice_dev = receice_dev + "     超声波传感器数据是："+ smoke;
                                    chaoshengbo = ""+smoke;
                                }
                            }

                            if (receive_buf[2] == (byte)0xBA){
                                receice_dev = "wifi";
                                if (receive_buf[9] == (byte)0x02) {
                                    int smoke = Integer.parseInt(String.valueOf(receive_buf[10]));
                                    receice_dev = receice_dev + "     光照传感器数据是："+ smoke;
                                    guangzhao = ""+smoke;
                                }
                            }

                            if (receive_buf[2] == (byte)0xBB){
                                receice_dev = "bluetooth";
                                if (receive_buf[9] == (byte)0x01) {
                                    int smoke = Integer.parseInt(String.valueOf(receive_buf[10]));
                                    if (smoke == 1) {
                                        rentiganying ="有人";
                                    }else {
                                        rentiganying ="没人";
                                    }
                                }
                            }

                            if (receive_buf[2] == (byte)0xBE){
                                receice_dev = "Lora";
                                if (receive_buf[9] == (byte)0x04) {
                                    int smoke = Integer.parseInt(String.valueOf(receive_buf[10]));
                                    receice_dev = receice_dev + "     烟雾传感器数据是："+ smoke;
                                    kernqiti = ""+smoke;
                                }
                                if (receive_buf[9] == (byte)0x0B) {
                                    int CO = Integer.parseInt(String.valueOf(receive_buf[10]));
                                    receice_dev = receice_dev + "     一氧化碳传感器数据是："+ CO;
                                    Co = ""+CO;
                                }

                                if (receive_buf[9] == (byte)0x10) {
                                    int CO = Integer.parseInt(String.valueOf(receive_buf[10]));
                                    receice_dev = receice_dev + "     震动传感器数据是："+ CO;
                                    Co = ""+CO;
                                }
                            }

                            Message msg = new Message();
                            String  get_weight = receice_string.toString();
                            //该部分是传参并更新控件
                            Bundle bundle = new Bundle();
                            msg.what = 0;
                            bundle.putString("from_dev", receice_dev );
                            bundle.putString("get_weight", get_weight);
                            bundle.putString("get_guangzhao", guangzhao);
                            bundle.putString("get_chaoshengbo",chaoshengbo);
                            bundle.putString("get_rentiganying",rentiganying);
                            bundle.putString("get_kerenqiti",kernqiti);
                            bundle.putString("get_co",Co);

                            msg.setData(bundle);
                            //发送消息到Handler
                            handler.sendMessage(msg);

                            buf_num =0;
                            receive_state = false;
                        } else {

                        }
                    } else {

                    }
                }
            }
        }
    }


    public Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case 0:
                {
                    //取出参数更新控件
                    tx_debug.setText(""+msg.getData().getString("get_weight"));
                    tx_translate.setText(""+msg.getData().getString("from_dev"));
                    tx_guangzhao.setText(""+msg.getData().getString("get_guangzhao"));
                    tx_chaoshengbo.setText(""+msg.getData().getString("get_chaoshengbo"));
                    tx_rentiganying.setText(""+msg.getData().getString("get_rentiganying"));
                    tx_kerenqiti.setText(""+msg.getData().getString("get_kerenqiti"));
                    tx_co.setText(""+msg.getData().getString("get_co"));

                }
                break;
                case 1:
                {
                    tx_debug.setText("0.00");
                }
                break;
                default:
                    break;
            }
            super.handleMessage(msg);
        }
    };

    /**
     * bytes转换成十六进制字符串
     * @return String 每个Byte值之间空格分隔
     */
    public static String byte2HexStr(byte[] b)
    {
        String stmp="";
        StringBuilder sb = new StringBuilder("");
        for (int n=0;n<b.length;n++)
        {
            stmp = Integer.toHexString(b[n] & 0xFF);
            sb.append((stmp.length()==1)? "0"+stmp : stmp);
            sb.append(" ");
        }
        return sb.toString().toUpperCase().trim();
    }

}
