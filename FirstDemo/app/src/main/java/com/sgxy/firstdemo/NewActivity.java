package com.sgxy.firstdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

public class NewActivity extends AppCompatActivity {
    //单选按钮，多选按钮
    //进度条，拖动条
    RadioButton Rd_Man,Rd_Woman;
    RadioGroup Rd_G;
    CheckBox CB_Sing,CB_Dance;

    ListView LV_Lesson;

    AutoCompleteTextView ACTX_Tel;

    Spinner Spi_Choose;

    Button Btn;

    String[] lesson = {"物联网移动应用开发","Python","嵌入式","Android开发","物联网移动应用开发",
            "Python","嵌入式","Android开发","物联网移动应用开发","Python","嵌入式","Android开发",
            "物联网移动应用开发","Python","嵌入式","Android开发"};

    String[] Tel={"135238774892","13533238774892","13525738774892","13235238774892",
            "13445238774892","135238774856492","13523877465892","135255538774892",
            "13523877489289798","13523877489244325","1352387748928969","135238774892875",};

    String[] Choose ={"汕头","清远","湛江","佛山","阳江","韶关","广州","揭阳","河源"};
    //适配器 Adapter
    //两个功能， 向下兼容数据；向上兼容布局（显示）
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);

        Rd_G = findViewById(R.id.rad_grop);
        Rd_Man = findViewById(R.id.rad_mam);
        Rd_Woman = findViewById(R.id.rad_womam);

        CB_Sing = findViewById(R.id.cb_sing);
        CB_Dance = findViewById(R.id.cb_dance);

        LV_Lesson = findViewById(R.id.lv_test);
        ACTX_Tel = findViewById(R.id.actx_lesson);
        Spi_Choose = findViewById(R.id.spi_choose);

        Btn = findViewById(R.id.btn_ok);

        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(NewActivity.this, ACTX_Tel.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
        CB_Sing.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override                                           //b是选中状态，True是被选中
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    Toast.makeText(NewActivity.this, CB_Sing.getText().toString()+" 被选中", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(NewActivity.this, CB_Sing.getText().toString()+" 被取消", Toast.LENGTH_SHORT).show();
            }
        });
        //单选按钮RadioButton，布局文件里面必须被RadioGroup包含，
        Rd_G.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                if (id == R.id.rad_mam)
                    Toast.makeText(NewActivity.this, "你选中了 男", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(NewActivity.this, "你选中了 女", Toast.LENGTH_SHORT).show();

            }
        });

        //初始化适配器
        //1.上下文（当前Activity）
        //2. item的样式
        //3. 要显示的数据
        ArrayAdapter adapter = new ArrayAdapter(
                NewActivity.this,
                android.R.layout.simple_list_item_single_choice,  //item样式
                lesson);

        LV_Lesson.setAdapter(adapter); //setAdapter是将适配器和显示控件关联
        //列表单击事件监听
        LV_Lesson.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(NewActivity.this, ""+lesson[i], Toast.LENGTH_SHORT).show();
            }
        });
        //自动补全编辑框
        ArrayAdapter adapter_tel = new ArrayAdapter(
                NewActivity.this,
                android.R.layout.simple_list_item_single_choice,
                Tel);

        ACTX_Tel.setAdapter(adapter_tel);

        //下拉列表
        ArrayAdapter adapter_spinner = new ArrayAdapter(
                NewActivity.this,
                android.R.layout.simple_list_item_1,
                Choose);

        Spi_Choose.setAdapter(adapter_spinner);

        Spi_Choose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d("TAG", "onItemSelected: "+ Choose[i]);
                Toast.makeText(NewActivity.this, "你选择的家乡是："+Choose[i], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}