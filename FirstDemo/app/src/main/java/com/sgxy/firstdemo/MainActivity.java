package com.sgxy.firstdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //1.声明控件，创建实例
    Button Btn_1,Btn_2,Btn_3,Btn_4,Btn_5,Btn_6,Btn_7,Btn_8,Btn_9;
    Button Btn_jia,Btn_jian,Btn_deng;
    TextView Tx_Input;
    String number = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_rela); //指定显示的布局文件

        //2.初始化控件 通过findViewById从xml文件里找到控件id，并绑定到声明的对象里
        Btn_1 = findViewById(R.id.btn_1);
        Btn_2 = findViewById(R.id.btn_2);
        Btn_3 = findViewById(R.id.btn_3);
        Btn_4 = findViewById(R.id.btn_4);
        Btn_5 = findViewById(R.id.btn_5);
        Btn_6 = findViewById(R.id.btn_6);
        Btn_7 = findViewById(R.id.btn_7);
        Btn_8 = findViewById(R.id.btn_8);
        Btn_9 = findViewById(R.id.btn_9);
        Btn_jia = findViewById(R.id.btn_jia);
        Btn_jian = findViewById(R.id.btn_jian);
        Btn_deng = findViewById(R.id.btn_deng);

        Tx_Input = findViewById(R.id.tx_input);

        //3.对按钮进行监听
        Btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //4. 按钮的动作
                number = number + Btn_1.getText().toString();
                Tx_Input.setText(number);
            }
        });

        Btn_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //4. 按钮的动作
                number = number + Btn_2.getText().toString();
                Tx_Input.setText(number);
            }
        });

        Btn_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //4. 按钮的动作
                number = number + Btn_3.getText().toString();
                Tx_Input.setText(number);
            }
        });

        Btn_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //4. 按钮的动作
                number = number + Btn_4.getText().toString();
                Tx_Input.setText(number);
            }
        });

        Btn_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //4. 按钮的动作
                number = number + Btn_5.getText().toString();
                Tx_Input.setText(number);
            }
        });

        Btn_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //4. 按钮的动作
                number = number + Btn_6.getText().toString();
                Tx_Input.setText(number);
            }
        });

        Btn_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //4. 按钮的动作
                number = number + Btn_7.getText().toString();
                Tx_Input.setText(number);
            }
        });

        Btn_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //4. 按钮的动作
                number = number + Btn_8.getText().toString();
                Tx_Input.setText(number);
            }
        });

        Btn_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //4. 按钮的动作
                number = number + Btn_9.getText().toString();
                Tx_Input.setText(number);
            }
        });
    }
}