package com.sgxy.firstdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    //1.声明控件
    EditText Edt_Name,Edt_Pw;
    Button Btn_Login;
    ImageView Img_head;
    SharedPreferences spf;
    String mypw = "123456";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        spf = getSharedPreferences("mydata",MODE_PRIVATE);
        Log.d("mymessage", "#########onCreate: ");
        //2. 绑定控件
        Edt_Name = findViewById(R.id.edt_user);
        Edt_Pw = findViewById(R.id.edt_pw);
        Btn_Login = findViewById(R.id.btn_login);
        Img_head = findViewById(R.id.img_head);

        //长按事件的监听
        Btn_Login.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                //长按动作
                Img_head.setImageResource(R.drawable.ypd_alert_upgrade);
                return false;
            }
        });

        //3. 监听按键
        Btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //4. 按键动作 （实现登录功能）
                String Str_user = Edt_Name.getText().toString();
                String Str_pw = Edt_Pw.getText().toString();

                if (Str_user == null | Str_user.equals(""))
                    Toast.makeText(LoginActivity.this, "请输入用户名", Toast.LENGTH_LONG).show();
                else {
                    if (Str_pw == null | Str_pw.equals(""))
                        Toast.makeText(LoginActivity.this, "请输入密码", Toast.LENGTH_LONG).show();
                    else {
                        if (Str_pw.equals(mypw)) {
                            Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_LONG).show();

                            //Activity跳转
                            Intent intent = new Intent(LoginActivity.this , MainActivity.class); //指定跳转的起点终点
                            startActivity(intent);

                        }else
                            Toast.makeText(LoginActivity.this, "密码错误", Toast.LENGTH_LONG).show();
                    }
                }

                //调试信息！！logcat
                Log.d("xx54",Str_user);
                //吐司
            }
        });
    }

    @Override
    protected void onStart() {
        //读取spf的信息，恢复数据
        Edt_Name.setText(spf.getString("key_name","user"));

        Log.d("mymessage", "#########onStart: ");

        super.onStart();
    }

    @Override
    protected void onPause() {
        Log.d("mymessage", "#########onPause: ");

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        //在Activity彻底销毁之前，把数据存到spf里面
        SharedPreferences.Editor  myedit = spf.edit();
        myedit.putString("key_name",Edt_Name.getText().toString());
        myedit.commit();
        Log.d("mymessage", "#########onDestroy: ");

        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        Log.d("mymessage", "#########onRestart: ");

        super.onRestart();
    }

    @Override
    protected void onStop() {
        Log.d("mymessage", "#########onStop: ");

        super.onStop();
    }

    @Override
    protected void onResume() {
        Log.d("mymessage", "#########onResume: ");

        super.onResume();
    }

    float x1,x2;
    int[] list_head_pic = {R.drawable.b39,R.drawable.bk5,
            R.drawable.bxp,R.drawable.bys,R.drawable.byu,
            R.drawable.bys,R.drawable.byv,R.drawable.byx,
            R.drawable.byy,R.drawable.byx};
    int index = 0;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //获取按下时的x轴坐标
        if(MotionEvent.ACTION_DOWN == event.getAction()){
            x1 = event.getX();
        }

        //获取抬起时的x轴坐标
        if(MotionEvent.ACTION_UP == event.getAction()) {
            x2 = event.getX();

            if (x2 - x1 > 50) {
                Toast.makeText(this, "右划", Toast.LENGTH_SHORT).show();
                //右划,显示上一张
                index--;
                if (index < 0 )
                    index = list_head_pic.length - 1;
                Img_head.setImageResource(list_head_pic[index]);
            }
            if (x1 - x2 > 50) {
                Toast.makeText(this, "左划", Toast.LENGTH_SHORT).show();
                index++;
                if (index == list_head_pic.length)
                    index = 0;
                Img_head.setImageResource(list_head_pic[index]);
            }
        }
        return super.onTouchEvent(event);
    }
}