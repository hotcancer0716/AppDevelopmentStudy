package com.sgxy.rk3568_gpio;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.DataOutputStream;

/**
 * 控制GPIO的打开和关闭
 */
public class GpioEntranceGuardTest {
    //gpio引脚编码变量，不同的功能有不同的引脚编码
    private final String gpio_number = "121";
    private final String TAG = "Wlei";

    /**
     * 初始化gpio
     */
    public void gpioInt() {
        //写入编号
        String exportPath = "echo " + gpio_number + " > /sys/class/gpio/export";
        chmod(exportPath);
        Log.d(TAG, exportPath);

        //定义输入输出方向
        String directionPath = "echo out > " + " /sys/class/gpio/gpio" + gpio_number
                + "/direction";
        chmod(directionPath);
        Log.d(TAG, directionPath);

        //赋予引脚编号的读写权限
        String permissionGpio = "chmod 0777 /sys/class/gpio/gpio/" + gpio_number + "/value";
        chmod(permissionGpio);
        Log.d(TAG, permissionGpio);

    }

    /**
     * 获取gpio编号对应的值，即是高电屏，或低电平
     *
     * @return
     */
    public int getValue() {
        File localFile = new File("/sys/class/gpio/gpio" + gpio_number
                + "/value");

       // String file_adc = "/sys/devices/platform/fe720000.saradc/iio:device0/in_voltage6_raw";
       // localFile = new File(file_adc);

        if (!localFile.exists())
            System.out.println(localFile.getAbsoluteFile() + " not exist!");
        while (true) {
            try {
                FileReader localFileReader = new FileReader(localFile);
                char[] arrayOfChar = new char[1];
                int i = localFileReader.read(arrayOfChar, 0, 1);
                localFileReader.close();
                if (i == 1) {
                    int j = arrayOfChar[0];
                    if (j == 48)
                        return 0;
                    return 1;
                }
            } catch (FileNotFoundException localFileNotFoundException) {
                localFileNotFoundException.printStackTrace();
                return -1;
            } catch (IOException localIOException) {
                localIOException.printStackTrace();
                return -1;
            }
        }
    }

    //发送指令，设置gpio值，即设置高电屏，或低电平
    public void setValue(int paramInt) {
        String exportPath1 = "echo " + paramInt + " > /sys/class/gpio/gpio" + gpio_number + "/value";
        chmod(exportPath1);
        Log.d(TAG, exportPath1);

    }

    /**
     * 执行外部程序指令
     *
     * @param instruct 指令
     */
    public void chmod(String instruct) {
        try {
            Process process;
            process = Runtime.getRuntime().exec("/system/bin/sh");
            //process = Runtime.getRuntime().exec("/system/xbin/su");
            DataOutputStream dataOutputStream = new DataOutputStream(process.getOutputStream());
            dataOutputStream.writeBytes(instruct);
            dataOutputStream.flush();
            dataOutputStream.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.d(TAG, "chmod: Error ");
        }
    }
}
