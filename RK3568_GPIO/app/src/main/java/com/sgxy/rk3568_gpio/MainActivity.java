package com.sgxy.rk3568_gpio;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class MainActivity extends AppCompatActivity {
    Button BtnADC;
    TextView Tx_adc;
    ProgressBar Pgr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GpioEntranceGuardTest gpioTest = new GpioEntranceGuardTest();
        gpioTest.gpioInt();

        ToggleButton sw = findViewById(R.id.myswitch);
        BtnADC = findViewById(R.id.adc);
        Tx_adc = findViewById(R.id.tx_adc);
        Pgr = findViewById(R.id.pgr);

        sw.setChecked(false);
        gpioTest.setValue(0);

        BtnADC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ADC模块的读取实现
                //通过Android的内部文件存储的方法
                String file_path = "/sys/devices/platform/fe720000.saradc/iio:device0/in_voltage6_raw";
                try{
                    File file = new File(file_path);
                    FileInputStream fis = new FileInputStream(file);
                    //配置读取缓存区
                    byte[] buffer = new byte[fis.available()];
                    fis.read(buffer);
                    String StrResult = new String(buffer);
                    BtnADC.setText(StrResult);
                    fis.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    gpioTest.setValue(1);
                }else {
                    gpioTest.setValue(0);
                }
                int gpio_status = gpioTest.getValue();
                Log.d("TAG", "LED状态是： "+gpio_status);
            }
        });

        //启动线程
        GetADCThread getADCThread = new GetADCThread();
        getADCThread.start();
    }

    //处理线程间通信
    Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case 999:
                    //3.接收子线程的消息 4.主线程显示
                    String result = msg.getData().getString("k","0");
                    Tx_adc.setText(result);
                    /*把字符串转换成int
                    int Num = Integer.parseInt(result);
                    */
                    //import java.util.regex.Matcher;
                    //import java.util.regex.Pattern;
                    Pattern pattern = Pattern.compile("\\d+"); // 匹配一个或多个数字
                    Matcher matcher = pattern.matcher(result);
                    if (matcher.find()) {
                        int num = Integer.parseInt(matcher.group()); // 提取匹配到的数字并转换为整数
                        Pgr.setProgress(num); //控制进度条的进度
                    }
                    break;
                default:
                    break;

            }
        }
    };

    //创建线程
    public class GetADCThread extends Thread{
        @Override
        public void run() {
            //1.创建子线程，2.在子线程循环读取，3.把读取的值，回传给主线程，4.主线程显示
            String file_path = "/sys/devices/platform/fe720000.saradc/iio:device0/in_voltage6_raw";

            while (true){
                FileInputStream fileInputStream = null;
                BufferedReader reader = null;
                StringBuilder stringBuilder = new StringBuilder();
                try {
                    fileInputStream = new FileInputStream(file_path);
                    reader = new BufferedReader(new InputStreamReader(fileInputStream));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line).append('\n');
                    }
                    String StrResult = stringBuilder.toString();

                    //发送消息到主线程
                    Message msg = new Message(); //创建消息对象
                    msg.what=999;  //定义消息的id
                    Bundle bundle = new Bundle(); //创建消息的容器
                    bundle.putString("k",StrResult); //把数据放到容器里
                    msg.setData(bundle); //把容器和消息关联，绑定
                    handler.sendMessage(msg);     //把消息发出去

                    reader.close();
                    sleep(200);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
}