package com.sgxy_ict.constellation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import java.net.URLEncoder;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.ImageView;
import java.io.ByteArrayOutputStream;


public class MainActivity extends AppCompatActivity {

    TextView TX_Show;
    Button BTN_OK,button2;
    String filepath = "/storage/emulated/0/Pictures/qqq.jpg";
    ImageView iv;
    byte[] image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //绑定控件
        TX_Show = findViewById(R.id.text_show);
        BTN_OK = findViewById(R.id.btn_go);
        iv = findViewById(R.id.img_1);
        button2=findViewById(R.id.but2);


        BTN_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //创建线程
                P_Thread p_thread = new P_Thread();
                p_thread.start();
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                C_Thread c_thread = new C_Thread();
                c_thread.start();
            }
        });
    }
    private class C_Thread extends Thread{
        @Override
        public void run() {
            dispatchTakePictureIntent();
        }
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            iv.setImageBitmap(imageBitmap);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            image = baos.toByteArray();
        }
    }

    private class P_Thread extends Thread{
        @Override
        public void run() {
            //请求url
            String url = "https://aip.baidubce.com/rest/2.0/image-classify/v1/classify/ingredient";
            try {
                //图片编码
        //        byte[] imgData = FileUtil.readFileByBytes(filepath);
                String imgStr = Base64Util.encode(image);
                String imgParam = URLEncoder.encode(imgStr, "UTF-8");
                Log.i("lyk", imgParam);
                String param = "image=" + imgParam;


                // 注意这里仅为了简化编码每一次请求都去获取access_token，线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。
                String accessToken = "24.7d274adb6ad80a3509fea904fe1b0a3c.2592000.1656515651.282335-26348290";
                //发出识别请求
                String result = HttpUtil.post(url, accessToken, param);
                Log.i("lyk", result);

                //请求返回的result用gson解析/
                Gson gson = new Gson();
                F_Name f_name = gson.fromJson(result,F_Name.class);
                //返回很多个相似果蔬的数据，这里用i=0仅获取第一个概率最大的果蔬
                int i = 0;
                String name = f_name.getResult().get(i).getName();


                Message msg = new Message();
                msg.what = 1;
                Bundle data = new Bundle();
                data.putString("k",name);
                msg.setData(data);
                handler.sendMessage(msg);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1:
                    TX_Show.setText( msg.getData().getString("k").toString()  );
            }
        }
    };
}
