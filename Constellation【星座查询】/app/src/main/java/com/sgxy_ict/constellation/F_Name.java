package com.sgxy_ict.constellation;

import java.util.List;

class F_Name {
    /**
     * log_id : 4654438231578404862
     * result_num : 5
     * result : [{"score":0.5271944403648376,"name":"番茄"},{"score":0.4640102982521057,"name":"西红柿"},{"score":0.0076364087872207165,"name":"圣女果"},{"score":7.92421109508723E-4,"name":"非果蔬食材"},{"score":1.0694196680560708E-4,"name":"醋栗"}]
     */

    private long log_id;
    private List<ResultBean> result;

    public long getLog_id() {
        return log_id;
    }

    public void setLog_id(long log_id) {
        this.log_id = log_id;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * score : 0.5271944403648376
         * name : 番茄
         */

        private double score;
        private String name;

        public double getScore() {
            return score;
        }

        public void setScore(double score) {
            this.score = score;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
