package com.sgxy_ict.onenet_sdk_demo;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.chinamobile.iot.onenet.edp.CmdMsg;
import com.chinamobile.iot.onenet.edp.Common;
import com.chinamobile.iot.onenet.edp.ConnectMsg;
import com.chinamobile.iot.onenet.edp.ConnectRespMsg;
import com.chinamobile.iot.onenet.edp.EdpMsg;
import com.chinamobile.iot.onenet.edp.PingRespMsg;
import com.chinamobile.iot.onenet.edp.PushDataMsg;
import com.chinamobile.iot.onenet.edp.SaveDataMsg;
import com.chinamobile.iot.onenet.edp.SaveRespMsg;
import com.chinamobile.iot.onenet.edp.toolbox.EdpClient;
import com.chinamobile.iot.onenet.edp.toolbox.Listener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    //根据你的项目，修改这里的代码，替换id和authInfo
    private String id = "707984602";           //设备ID
    private int connetcType = 1;      //连接类型，1或2，参考模拟器
    private String authInfo = "k8SZBe2xWkCero87zV1JLiLDeAs=";     //api-key
    private  int encryptType = -1;    //明文通信

    String WLDebug = "WLDebug";

    EditText ET_Temp,ET_Humi,ET_X,ET_Y;
    Button BTN_Save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        find_id();

        // 1、初始化SDK
        EdpClient.initialize(this, connetcType, id, authInfo);
        // 2、设置接收响应的回调
        EdpClient.getInstance().setListener(mEdpListener);
        // 3、设置自动发送心跳的周期（默认4min）
        EdpClient.getInstance().setPingInterval(3 * 60 * 1000);
        // 4、建立TCP连接
        EdpClient.getInstance().connect();

        if (Common.Algorithm.NO_ALGORITHM == encryptType) {
            // 5、如果使用明文通信，则建立连接后直接发送连接请求
            EdpClient.getInstance().sendConnectReq();
        } else if (Common.Algorithm.ALGORITHM_AES == encryptType) {
            // 6、如果使用加密通信，则先发送加密请求，然后在加密响应回调中发送连接请求
            EdpClient.getInstance().requestEncrypt(Common.Algorithm.ALGORITHM_AES);
        }

        BTN_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Str_temp = ET_Temp.getText().toString();
                String Str_Humi = ET_Humi.getText().toString();
                JSONObject temp = new JSONObject();
                try {
                    temp.put("temp",Integer.parseInt(Str_temp));
                    temp.put("humi",Integer.parseInt(Str_Humi));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                EdpClient.getInstance().saveData(
                        id,3,null,temp.toString().getBytes());

                String Xmove = ET_X.getText().toString();
                String Ymove = ET_Y.getText().toString();
                JSONObject gps = new JSONObject();
                JSONObject data = new JSONObject();
                try {
                    gps.put("lon",Float.parseFloat(Xmove));
                    gps.put("lat",Float.parseFloat(Ymove));
                    SaveDataMsg.packSaveData1Msg(
                            data,null,"GPS",null,gps);
                    EdpClient.getInstance().saveData(
                            id,1,null,data.toString().getBytes());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void find_id() {
        ET_Temp = findViewById(R.id.wendu);
        ET_Humi = findViewById(R.id.shidu);
        ET_X = findViewById(R.id.xmove);
        ET_Y = findViewById(R.id.ymove);
        BTN_Save = findViewById(R.id.btn_save);
    }

    private Listener mEdpListener = new Listener() {

        @Override
        public void onReceive(List<EdpMsg> msgList) {
            if (null == msgList) {
                return;
            }
            for (EdpMsg msg : msgList) {
                if (null == msg) {
                    continue;
                }
                switch (msg.getMsgType()) {
                    // 连接响应
                    case Common.MsgType.CONNRESP:
                        ConnectRespMsg connectRespMsg = (ConnectRespMsg) msg;
                        if ( connectRespMsg.getResCode() == Common.ConnResp.ACCEPTED ){
                            Toast.makeText(MainActivity.this,
                                    "连接成功", Toast.LENGTH_SHORT).show();
                            Log.d(WLDebug,"连接成功 getResCode: "+connectRespMsg.getResCode());
                        }
                        else{
                            Toast.makeText(MainActivity.this,
                                    "连接失败，返回值"+connectRespMsg.getResCode(), Toast.LENGTH_SHORT).show();
                            Log.d(WLDebug,"连接失败 getResCode: "+connectRespMsg.getResCode());
                        }
                        break;
                    // 心跳响应
                    case Common.MsgType.PINGRESP:
                        PingRespMsg pingRespMsg = (PingRespMsg) msg;
                        Toast.makeText(MainActivity.this,
                                "心跳响应", Toast.LENGTH_SHORT).show();
                        Log.d(WLDebug,"心跳响应");
                        break;
                    // 存储确认
                    case Common.MsgType.SAVERESP:
                        SaveRespMsg saveRespMsg = (SaveRespMsg) msg;
                        Toast.makeText(MainActivity.this,
                                "存储确认"+ new String(saveRespMsg.getData()), Toast.LENGTH_SHORT).show();
                        Log.d(WLDebug,"存储确认"+ new String(saveRespMsg.getData()));
                        break;
                    // 转发（透传）
                    case Common.MsgType.PUSHDATA:
                        PushDataMsg pushDataMsg = (PushDataMsg) msg;
                        Toast.makeText(MainActivity.this,
                                "透传："+ new String(pushDataMsg.getData()), Toast.LENGTH_SHORT).show();
                        Log.d(WLDebug,"透传："+ new String(pushDataMsg.getData()));
                        break;
                    // 存储（转发）
                    case Common.MsgType.SAVEDATA:
                        SaveDataMsg saveDataMsg = (SaveDataMsg) msg;
                        for (byte[] bytes : saveDataMsg.getDataList()){
                            Toast.makeText(MainActivity.this,
                                    "存储（转发）："+ new String(bytes), Toast.LENGTH_SHORT).show();
                            Log.d(WLDebug,"存储（转发）："+ new String(bytes));
                        }
                        break;
                    // 命令请求
                    case Common.MsgType.CMDREQ:
                        CmdMsg cmdMsg = (CmdMsg) msg;
                        Toast.makeText(MainActivity.this,
                                "命令请求：\n cmdId:"+ cmdMsg.getCmdId() +
                                        "\n cmdData："+cmdMsg.getData(), Toast.LENGTH_SHORT).show();
                        Log.d(WLDebug,"命令请求：\n cmdId:"+ cmdMsg.getCmdId() +
                                "\n cmdData："+cmdMsg.getData());
                        EdpClient.getInstance().sendCmdResp(cmdMsg.getCmdId(),"发送命令成功".getBytes());
                        break;
                    // 加密响应
                    case Common.MsgType.ENCRYPTRESP:
                        break;
                }
            }
        }

        @Override
        public void onFailed(Exception e) {
            e.printStackTrace();
        }

        @Override
        public void onDisconnect() {

        }
    };
}
