package com.sgxy_ict.netdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SocketActivity extends AppCompatActivity {

    TextView TX_Connect_Status;
    EditText TX_SentMessage;
    TextView TX_GetMessage;
    Button BTN_Sent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_socket);

        BTN_Sent = findViewById(R.id.socket_btn_client_sent);
        TX_SentMessage = findViewById(R.id.socket_et_client_message);
        TX_GetMessage = findViewById(R.id.socket_tx_getmsg);
        TX_Connect_Status = findViewById(R.id.socket_tx_connect_status);

        BTN_Sent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
}