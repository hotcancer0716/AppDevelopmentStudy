package com.sgxy_ict.netdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sgxy_ict.netdemo.databinding.ActivityGsonBinding;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GsonActivity extends AppCompatActivity {
    //组织http请求参数
    String http_Adress = "http://apis.juhe.cn/simpleWeather/query";
    //鉴权信息
    String Key = "";
    String CityName = "";
    String Http_URL = "";
    ActivityGsonBinding this_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this_ = ActivityGsonBinding.inflate(getLayoutInflater());
        setContentView(this_.getRoot());

        this_.gsonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CityName = this_.gsonEt.getText().toString();
                if (Key.equals("")&&Key.length()==0){
                    Toast.makeText(GsonActivity.this,"请录入鉴权信息",Toast.LENGTH_SHORT).show();
                }else if( CityName.equals("")&&CityName.length()==0 ) {
                    Toast.makeText(GsonActivity.this,"城市名不能为空",Toast.LENGTH_SHORT).show();
                }else {
                    Http_URL = http_Adress+"?"+"city="+CityName+"&"+"key="+Key;
                    //okhttp使用
                    Thread_get_weather thread_get_weather = new Thread_get_weather();
                    thread_get_weather.start();
               }
            }
        });
    }

    public class Thread_get_weather extends Thread{
        @Override
        public void run() {
            try {
                //实例化OkHttp
                OkHttpClient client = new OkHttpClient();
                //创建Request对象，并利用builder进行初始化
                Request request = new Request.Builder().url(Http_URL).build();
                //建立线程，并连接网络
                Response response = client.newCall(request).execute();
                //获取返回数据
                String json = response.body().string();

                Message msg = new Message();
                Bundle bundle = new Bundle();
                bundle.putString("weather_json",json);
                msg.setData(bundle);
                msg.what= 111;
                handler.sendMessage(msg);
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case 111:
                    String json = msg.getData().getString("weather_json");
                    Weather_Data weather_data = new Gson().fromJson(json , Weather_Data.class);
                    this_.gsonTemperatureValue.setText(weather_data.getResult().getRealtime().getTemperature());
                    this_.gsonHumidityValue.setText(weather_data.getResult().getRealtime().getHumidity());
                    this_.gsonDirectValue.setText(weather_data.getResult().getRealtime().getDirect());
                    this_.gsonInfoValue.setText(weather_data.getResult().getRealtime().getInfo());
                    this_.gsonPowerValue.setText(weather_data.getResult().getRealtime().getPower());
                    break;
                default:
                    break;
            }
            super.handleMessage(msg);
        }
    };
}