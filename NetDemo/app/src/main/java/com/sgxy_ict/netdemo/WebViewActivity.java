package com.sgxy_ict.netdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class WebViewActivity extends AppCompatActivity {
    EditText ET_web;
    Button BTN_web;
    WebView WB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_webview);
        ET_web = findViewById(R.id.web_tx);
        BTN_web = findViewById(R.id.web_btn);
        WB = findViewById(R.id.web_webview);

        ET_web.setText("www.baidu.com");
        WB.loadUrl("http://"+ET_web.getText().toString());
        WB.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl("http://"+ET_web.getText().toString());
                return super.shouldOverrideUrlLoading(view, request);
            }
        });

        BTN_web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WB.loadUrl("http://"+ET_web.getText().toString());
            }
        });

        ET_web.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keycode, KeyEvent keyEvent) {
                if (KeyEvent.KEYCODE_ENTER == keycode && keyEvent.getAction()==KeyEvent.ACTION_DOWN){
                    WB.loadUrl("http://"+ET_web.getText().toString());
                    return true;
                }
                return false;
            }
        });
    }
}