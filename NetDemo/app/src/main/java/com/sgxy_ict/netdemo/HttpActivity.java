package com.sgxy_ict.netdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpActivity extends AppCompatActivity {

    TextView TX_result;
    Button BTN_http;
    EditText ET_City;

    String CityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_http);

        ET_City = findViewById(R.id.http_tx);
        TX_result = findViewById(R.id.http_result);
        BTN_http = findViewById(R.id.http_btn);

        BTN_http.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CityName = ET_City.getText().toString();
                Thread_Weather threadWeather = new Thread_Weather();
                threadWeather.start();
            }
        });
    }

    public class Thread_Weather extends Thread {
        @Override
        public void run() {
            //组织http请求参数
            String http_Adress = "http://apis.juhe.cn/simpleWeather/query";
            //鉴权信息
            String Key = "";

            if (Key.equals("") && Key.length()==0){
                Message msg = new Message();
                msg.what = 777;
                handler.sendMessage(msg);
            }else{
                String Http_URL = http_Adress+"?"+"city="+CityName+"&"+"key="+Key;

                try {
                    //转化为URL
                    URL url = new URL(Http_URL);
                    //http连接请求
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    int Code = httpURLConnection.getResponseCode();
                    Log.d("TAG", "Code: "+ Code);
                    if (Code == 404){
                        Message msg = new Message();
                        msg.what = 404;
                        handler.sendMessage(msg);
                    }

                    //读取数据
                    InputStream inputStream = httpURLConnection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream , "utf-8");
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    String Str_json = bufferedReader.readLine();

                    Message msg = new Message();
                    msg.what = 898;
                    Bundle bundle = new Bundle();
                    bundle.putString("http_result",Str_json);
                    msg.setData(bundle);
                    handler.sendMessage(msg);

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }


    Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 898:
                    String Str = msg.getData().getString("http_result").toString();
                    TX_result.setText(Str);
                    break;

                case 777:
                    TX_result.setText("尚未在代码中添加用户key，用于鉴权！");
                    break;

                case 404:
                    TX_result.setText("服务器无响应，请检查网络或服务器域名");
                    break;
            }
        }
    };
}