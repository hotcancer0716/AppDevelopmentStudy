package com.sgxy_ict.netdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button BTN_1,BTN_2,BTN_3,BTN_4,BTN_5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BTN_1 = findViewById(R.id.btn_1);
        BTN_2 = findViewById(R.id.btn_2);
        BTN_3 = findViewById(R.id.btn_3);
        BTN_4 = findViewById(R.id.btn_4);
        BTN_5 = findViewById(R.id.btn_5);

        BtnClick clickBtn = new BtnClick();

        BTN_1.setOnClickListener(clickBtn);
        BTN_2.setOnClickListener(clickBtn);
        BTN_3.setOnClickListener(clickBtn);
        BTN_4.setOnClickListener(clickBtn);
        BTN_5.setOnClickListener(clickBtn);
    }

    public class BtnClick implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            switch (view.getId()){
                case R.id.btn_1:
                    intent.setClass(MainActivity.this,SocketActivity.class);
                    startActivity(intent);
                    break;

                case R.id.btn_2:
                    intent.setClass(MainActivity.this,URLActivity.class);
                    startActivity(intent);
                    break;

                case R.id.btn_3:
                    intent.setClass(MainActivity.this,HttpActivity.class);
                    startActivity(intent);
                    break;

                case R.id.btn_4:
                    intent.setClass(MainActivity.this,WebViewActivity.class);
                    startActivity(intent);
                    break;

                case R.id.btn_5:
                    intent.setClass(MainActivity.this,GsonActivity.class);
                    startActivity(intent);
                    break;
            }
        }
    }
}