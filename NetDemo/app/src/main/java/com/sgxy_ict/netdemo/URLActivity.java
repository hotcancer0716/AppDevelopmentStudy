package com.sgxy_ict.netdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class URLActivity extends AppCompatActivity {

    ImageView IMG_url;
    TextView TX_url;
    Button BTN_url;
    String StrUrl;
    Bitmap pic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_url);

        TX_url = findViewById(R.id.url_tx);
        BTN_url = findViewById(R.id.url_btn);
        IMG_url = findViewById(R.id.url_image);

        TX_url.setText("http://www.5h.com/d/file/20160112/1452565552906008.jpg");

        BTN_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StrUrl = TX_url.getText().toString();
                //线程启动
                Thread_URL thread_url = new Thread_URL();
                thread_url.start();
            }
        });

    }

    //1.创建线程
    public class Thread_URL extends Thread{
        @Override
        public void run() {
            super.run();
            try {
                //2. 转化成URL对象
                URL newUrl = new URL(StrUrl);
                //3. 根据URL连接网络
                URLConnection urlConnection = newUrl.openConnection();
                //4. 接收数据
                InputStream inputStream = urlConnection.getInputStream();
                //5. 接受的数据转化为图片
                pic = BitmapFactory.decodeStream(inputStream);
                //7. 发送消息
                Message msg = new Message();
                msg.what = 101;
                handler.sendMessage(msg);


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    //6. 创建Handler，用来接收数据
    Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            //8 接收消息
            switch (msg.what){
                case 101:
                    IMG_url.setImageBitmap(pic);
                    break;


                default:
                    throw new IllegalStateException("Unexpected value: " + msg.what);
            }
        }
    };

}