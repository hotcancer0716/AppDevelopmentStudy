package com.gdiot.usb_XArm;


import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.util.Log;

public class LF_PrinterDevice {

    private static final String LOG_TAG = "LF_PrinterDevice";
    private final UsbDeviceConnection mDeviceConnection;
    private final UsbEndpoint mEndpointOut;        //USB发送通道
    private final SentThread mSentThread = new SentThread();
    int COMMON_CUT = 1;
    byte[] buf_commod;

    public LF_PrinterDevice(byte[] content,
                            UsbDeviceConnection connection,
                            UsbInterface intf) {
        // TODO Auto-generated constructor stub
        mDeviceConnection = connection;
        connection.getSerial();
        buf_commod = content;

        UsbEndpoint epOut = null;
        UsbEndpoint epIn = null;
        // look for our bulk endpoints
        for (int i = 0; i < intf.getEndpointCount(); i++) {
            UsbEndpoint ep = intf.getEndpoint(i);
            if (ep.getType() == UsbConstants.USB_ENDPOINT_XFER_BULK) {  //Bulk USB的大容量传输模式
                if (ep.getDirection() == UsbConstants.USB_DIR_OUT) {  //OUT就是发送
                    epOut = ep;
                } else {
                    epIn = ep;
                }
            }
        }
        if (epOut == null || epIn == null) {
            throw new IllegalArgumentException("not all endpoints found");
        }
        mEndpointOut = epOut;
    }

    public void start() {
        mSentThread.start();
    }


    private class SentThread extends Thread {
        public void run() {
            Log.i(LOG_TAG, "SentThread start");

            print_commnd(buf_commod);

            Log.i(LOG_TAG, "SentThread finish");
        }
    }


    public void print_commnd(byte[] buf_commod) {
        // TODO Auto-generated method stub
        BufferedInputStream bufferInputStream = null;
        try {
            final int MAX_USBFS_BUFFER_SIZE = 16384; //16KiB.
            byte[] bytes = new byte[MAX_USBFS_BUFFER_SIZE];
            int bytesRead = 0;
            int bytesWrite = 0;
            //---------------------------------------------------------------------
            //20220814 控制机械臂完成1个组合动作
           // byte[] buf_qiezhi = {0x55, 0x55, 0x05, 0x06, 0x01, 0x01,0x00};  //控制机械臂完成1个组合动作
            InputStream qiezhi_InputStream = null;
            qiezhi_InputStream = new ByteArrayInputStream(buf_commod);
            bufferInputStream = new BufferedInputStream(qiezhi_InputStream);
            while ((bytesRead = bufferInputStream.read(bytes)) != -1) {
                //将读取的字节转为字符串对象
                String chunk = new String(bytes, 0, bytesRead);
                Log.i(LOG_TAG, chunk);

                // mEndpointOut USB发送通道     OUT/IN（Host to Device用OUT，Device to Host 用IN）
                // bytes 发送缓冲区   !将要发送/接收的指令或数据，当endpoint为OUT，buffer为你定义好的指令或数据，将下发给device，当endpoint为IN，buffer则是一个容器，用来存储device返回的应答指令或数据，此时一定要注意buffer的大小，以足够存储所有的数据
                // bytesRead 发送字节数累计! 发送/接收指令或数据的大小
                // 40 超时时间      !指令或数据的最长通讯时间，在通讯出现问题时，若超时还未通讯完成，视为通讯失败

                //bulkTransfer 在USB通讯里可以用做发送也可以用做接收
                bytesWrite = mDeviceConnection.bulkTransfer(mEndpointOut, bytes,
                        bytesRead, 40);

                Log.i(LOG_TAG, "bytesRead:" + bytesRead + " bytesWrite:" + bytesWrite);

                if(bytesWrite < 0) {

                    // 暂停此线程 排除故障 暂时做到这种程度，因为对Java多线程编程不是太熟悉。
                    // 打印被暂停时会进入到这个里面，可能引起的问题有：缺纸 卡纸等等。这个时候要有另外一个线程去获取Printer状态。
                    synchronized (this) {   //线程锁定
                        wait();
                        notifyAll();  //唤醒一个等待的线程
                    }
                }

                if(bytesWrite != bytesRead) {
                    Log.e(LOG_TAG, "bultTransfer error!");
                    break;
                }

                Thread.sleep(1000);
            }
            //切纸结束

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(bufferInputStream != null)
                    bufferInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}

