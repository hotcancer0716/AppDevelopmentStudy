package com.gdiot.usb_XArm;

import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {
    private UsbManager mManager;
    private UsbDevice mDevice;
    private UsbDeviceConnection mDeviceConnection;
    private UsbInterface mInterface;
    private PrinterDevice mPrinterDevice;
    private static String LOG_TAG = "HOT<WLEI>";

    AssetManager mAssetManager = null;
    InputStream mInputStream = null;
    //	UsbDevice device;
    static TextView TX_DEBUG;
    Button BT_TEST,BT_CLOSE;
    byte[] buf_commod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        find_id();
        key_things();
        init_usb();
        init_txt();
    }


    private void init_txt() {
        // TODO Auto-generated method stub
        //读取资源文件
        mAssetManager = getAssets(); //Android源码目录Assets下的内容
        try {
            mInputStream = mAssetManager.open("c1_printer.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void init_usb() {
        // TODO Auto-generated method stub
        mManager = (UsbManager) getSystemService(Context.USB_SERVICE);

        // listen for new devices
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);	//开始监控USB激活
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);	//开始监控USB拔出
        registerReceiver(mUsbReceiver, filter);   //启动事先声明的系统广播!
    }

    private void key_things() {
        // TODO Auto-generated method stub
        BT_TEST.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                byte[] buf_move = {0x55, 0x55, 0x05, 0x06, 0x01, 0x01,0x00};  //控制机械臂完成1个组合动作
                buf_commod = buf_move;

                for(UsbDevice device_new : mManager.getDeviceList().values()) {
                    //从device信息里提炼出UsbInterface接口信息
                    UsbInterface intf = findPrinterInterface(device_new);
                    //如果设置USB总线上设备为打印机类型成功,就退出循环,(上一条函数返回值为intf,如果不是打印机类型intf值为null)
                    if (setPrinterInterface(device_new, intf)) {
                        break;
                    }
                }
            }
        });

        BT_CLOSE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                byte[] buf_stop = {0x55, 0x55, 0x02, 0x07};
                buf_commod = buf_stop;

                for(UsbDevice device_new : mManager.getDeviceList().values()) {
                    //从device信息里提炼出UsbInterface接口信息
                    UsbInterface intf = findPrinterInterface(device_new);
                    //如果设置USB总线上设备为打印机类型成功,就退出循环,(上一条函数返回值为intf,如果不是打印机类型intf值为null)
                    if (setPrinterInterface(device_new, intf)) {
                        break;
                    }
                }
            }
        });
    }

    private void find_id() {
        // TODO Auto-generated method stub
        TX_DEBUG = (TextView) findViewById(R.id.tx_debug);
        BT_TEST  = (Button) findViewById(R.id.bt_debug);
        BT_CLOSE  = (Button) findViewById(R.id.bt_close);
    }

    BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            //获取到USB有设备插入,初始化
            if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                UsbDevice device = intent
                        .getParcelableExtra(UsbManager.EXTRA_DEVICE);
                String deviceName = device.getDeviceName();
                TX_DEBUG.setText("deviceName is "+deviceName+"  printer interface installed ");
				/*
				device = (UsbDevice) intent
						.getParcelableExtra(UsbManager.EXTRA_DEVICE);
				Log.d(LOG_TAG, "WL BroadcastReceiver -> UsbDevice");

				UsbInterface intf = findPrinterInterface(device);
				if (intf != null) {
					Log.i(LOG_TAG, "Found Printer interface " + intf);
					setPrinterInterface(device, intf);
				}
				*/
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                //USB设备拔出
                UsbDevice device = intent
                        .getParcelableExtra(UsbManager.EXTRA_DEVICE);
                String deviceName = device.getDeviceName();
                if (mDevice != null && mDevice.equals(deviceName)) {
                    TX_DEBUG.setText("printer interface removed ");

                    Log.i(LOG_TAG, "printer interface removed");     //拔出USB接口弹出!!
                    setPrinterInterface(null, null);
                }
            }
        }
    };

    // searches for an adb interface on the given USB device
    // 从一大堆USB设备里面筛选出打印机设备
    static private UsbInterface findPrinterInterface(UsbDevice device) {
        Log.d(LOG_TAG, "WL findPrinterInterface " + device);
        int count = device.getInterfaceCount(); //遍历USB总线上有几个设备
        for (int i = 0; i < count; i++) {
            UsbInterface intf = device.getInterface(i);
            Log.i("HOT Forever", "USB_CLASS is"+intf.getInterfaceClass());

            //经测试得到该USB机械臂设备是USB_CLASS_VENDOR_SPEC ，编号255 0xff
            //20220812 可以在下面进行判断，再进行串口通信。
            //USB_CLASS_VENDOR_SPEC判断intf里USB设备class是不是对应的类型! class=255 是该机械臂设备
            if (intf.getInterfaceClass() == UsbConstants.USB_CLASS_VENDOR_SPEC)
            {
                TX_DEBUG.setText("USBcount = "+ count +" !!! "+ device +" |||| "+ intf);
                Log.i(LOG_TAG, "USBcount = "+ count +" !!! "+ device +" |||| "+ intf);
                Log.i(LOG_TAG, "Printer");
                return intf; //发现USB总线上有1个打印机设备,把设备信息返回!
            }
        }
        return null;
    }


    // Sets the current USB device and interface
    //研究这个!!重点!
    //把USB和打印机设备接口相连,配对
    private boolean setPrinterInterface(UsbDevice device, UsbInterface intf) {
        //一些USB相关参数初始化前的清零
        if (mDeviceConnection != null) {
            if (mInterface != null) {							//UsbInterface
                mDeviceConnection.releaseInterface(mInterface); //UsbDeviceConnection
                mInterface = null;
            }
            mDeviceConnection.close();
            mDevice = null;
            mDeviceConnection = null;
        }
        openUsbDevice();
        // [5] 检查usb权限
        boolean hasPermission = mUsbManager.hasPermission(device);
        if (!hasPermission) {
            Log.i(LOG_TAG, "no usb permission");
            //[7] 申请usb权限
            PendingIntent mPermissionIntent = PendingIntent.getBroadcast(this, 9898, new Intent(ACTION_USB_PERMISSION), PendingIntent.FLAG_ONE_SHOT);
            IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
            registerReceiver(mUsbReceiver, filter);
            mUsbManager.requestPermission(device, mPermissionIntent);
            Log.i(LOG_TAG, "usb permission is " + mUsbManager.hasPermission(device));
        }

        if (device != null && intf != null) {
            //打开设备连接
            UsbDeviceConnection connection = mManager.openDevice(device);  //open
            //如果打开成功
            if (connection != null) {
                Log.i(LOG_TAG, "open succeeded");
                //claim提出要求连接
                if (connection.claimInterface(intf, true)) {
                    mDevice = device;     			  //以下全部传递到全局变量
                    mDeviceConnection = connection;
                    mInterface = intf;

                    //初始化USB设备为打印机设备
                    mPrinterDevice = new PrinterDevice(this, mDeviceConnection, intf);
                    LF_PrinterDevice lf_PrinterDevice = new LF_PrinterDevice(buf_commod, mDeviceConnection, intf);

                    //开始控制
                    lf_PrinterDevice.start(); //适合命令发送

					//mPrinterDevice.start(); //适合打印大段内容
                    return true;
                } else {
                    Log.i(LOG_TAG, "claim interface failed");  //这里出现过
                    connection.close();
                }
            } else {
                Log.i(LOG_TAG, "open failed");
            }
        }

        if (mDeviceConnection == null && mPrinterDevice != null) {
            //mPrinterDevice.stop();
            mPrinterDevice = null;
        }
        return false;
    }

    /**
     * 获得 usb 权限
     */
    private void openUsbDevice(){
        //before open usb device
        //should try to get usb permission
        tryGetUsbPermission();
    }
    UsbManager mUsbManager;
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

    private void tryGetUsbPermission(){
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        registerReceiver(mUsbPermissionActionReceiver, filter);

        PendingIntent mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);

        //here do emulation to ask all connected usb device for permission
        for (final UsbDevice usbDevice : mUsbManager.getDeviceList().values()) {
            //add some conditional check if necessary
            //if(isWeCaredUsbDevice(usbDevice)){
            if(mUsbManager.hasPermission(usbDevice)){
                //if has already got permission, just goto connect it
                //that means: user has choose yes for your previously popup window asking for grant perssion for this usb device
                //and also choose option: not ask again
                afterGetUsbPermission(usbDevice);
            }else{
                //this line will let android popup window, ask user whether to allow this app to have permission to operate this usb device
                mUsbManager.requestPermission(usbDevice, mPermissionIntent);
            }
            //}
        }
    }


    private void afterGetUsbPermission(UsbDevice usbDevice){
        //call method to set up device communication
        //Toast.makeText(this, String.valueOf("Got permission for usb device: " + usbDevice), Toast.LENGTH_LONG).show();
        //Toast.makeText(this, String.valueOf("Found USB device: VID=" + usbDevice.getVendorId() + " PID=" + usbDevice.getProductId()), Toast.LENGTH_LONG).show();

        doYourOpenUsbDevice(usbDevice);
    }

    private void doYourOpenUsbDevice(UsbDevice usbDevice){
        //now follow line will NOT show: User has not given permission to device UsbDevice
        UsbDeviceConnection connection = mUsbManager.openDevice(usbDevice);
        //add your operation code here
    }

    private final BroadcastReceiver mUsbPermissionActionReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice usbDevice = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        //user choose YES for your previously popup window asking for grant perssion for this usb device
                        if(null != usbDevice){
                            afterGetUsbPermission(usbDevice);
                        }
                    }
                    else {
                        //user choose NO for your previously popup window asking for grant perssion for this usb device
                        Toast.makeText(context, String.valueOf("Permission denied for device" + usbDevice), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    };

}
