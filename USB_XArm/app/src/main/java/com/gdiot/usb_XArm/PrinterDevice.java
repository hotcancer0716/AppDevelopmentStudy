package com.gdiot.usb_XArm;

import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class PrinterDevice {

    private static final String LOG_TAG = "PrinterDevice";

    private final MainActivity mActivity;
    private final UsbDeviceConnection mDeviceConnection;
    private final UsbEndpoint mEndpointOut;        //USB发送通道
    private final WaiterThread mWaiterThread = new WaiterThread();

    public PrinterDevice(MainActivity activity, UsbDeviceConnection connection,
                         UsbInterface intf) {
        mActivity = activity;
        mDeviceConnection = connection;
        connection.getSerial();

        UsbEndpoint epOut = null;
        UsbEndpoint epIn = null;
        // look for our bulk endpoints
        for (int i = 0; i < intf.getEndpointCount(); i++) {
            UsbEndpoint ep = intf.getEndpoint(i);
            if (ep.getType() == UsbConstants.USB_ENDPOINT_XFER_BULK) {  //Bulk USB的大容量传输模式
                if (ep.getDirection() == UsbConstants.USB_DIR_OUT) {  //OUT就是发送
                    epOut = ep;
                } else {
                    epIn = ep;
                }
            }
        }
        if (epOut == null || epIn == null) {
            throw new IllegalArgumentException("not all endpoints found");
        }
        mEndpointOut = epOut;
    }

    public void start() {
        mWaiterThread.start();
    }

    private class WaiterThread extends Thread {
        public void run() {
            BufferedInputStream bufferInputStream = null;

            try {
                final int MAX_USBFS_BUFFER_SIZE = 16384; //16KiB.
                byte[] bytes = new byte[MAX_USBFS_BUFFER_SIZE];
                //从这里获取了之前的文本信息
                bufferInputStream = new BufferedInputStream(mActivity.mInputStream);
                int bytesRead = 0;
                int bytesWrite = 0;

                //从文件中按字节读取内容，到文件尾部时read方法将返回-1
                while ((bytesRead = bufferInputStream.read(bytes)) != -1) {

                    //将读取的字节转为字符串对象
                    String chunk = new String(bytes, 0, bytesRead);
                    Log.i("LOG_TAG", chunk);

                    // mEndpointOut USB发送通道     OUT/IN（Host to Device用OUT，Device to Host 用IN）
                    // bytes 发送缓冲区   !将要发送/接收的指令或数据，当endpoint为OUT，buffer为你定义好的指令或数据，将下发给device，当endpoint为IN，buffer则是一个容器，用来存储device返回的应答指令或数据，此时一定要注意buffer的大小，以足够存储所有的数据
                    // bytesRead 发送字节数累计! 发送/接收指令或数据的大小
                    // 40 超时时间      !指令或数据的最长通讯时间，在通讯出现问题时，若超时还未通讯完成，视为通讯失败

                    //bulkTransfer 在USB通讯里可以用做发送也可以用做接收
                    bytesWrite = mDeviceConnection.bulkTransfer(mEndpointOut, bytes,
                            bytesRead, 40);

                    Log.i(LOG_TAG, "bytesRead:" + bytesRead + " bytesWrite:" + bytesWrite);

                    if(bytesWrite < 0) {

                        // 暂停此线程 排除故障 暂时做到这种程度，因为对Java多线程编程不是太熟悉。
                        // 打印被暂停时会进入到这个里面，可能引起的问题有：缺纸 卡纸等等。这个时候要有另外一个线程去获取Printer状态。
                        synchronized (this) {   //线程锁定
                            wait();
                            notifyAll();  //唤醒一个等待的线程
                        }
                    }

                    if(bytesWrite != bytesRead) {
                        Log.e(LOG_TAG, "bultTransfer error!");
                        break;
                    }

                    //         	Thread.sleep(1000);
                }

                //---------------------------------------------------------------------
                //尝试增加一个切纸
                byte[] buf_qiezhi = {0x1b ,0x6d};  //切纸
                InputStream qiezhi_InputStream = null;
                qiezhi_InputStream = new ByteArrayInputStream(buf_qiezhi);
                bufferInputStream = new BufferedInputStream(qiezhi_InputStream);
                bytesRead = 0;
                bytesWrite = 0;
                while ((bytesRead = bufferInputStream.read(bytes)) != -1) {
                    //将读取的字节转为字符串对象
                    String chunk = new String(bytes, 0, bytesRead);
                    Log.i("LOG_TAG", chunk);

                    // mEndpointOut USB发送通道     OUT/IN（Host to Device用OUT，Device to Host 用IN）
                    // bytes 发送缓冲区   !将要发送/接收的指令或数据，当endpoint为OUT，buffer为你定义好的指令或数据，将下发给device，当endpoint为IN，buffer则是一个容器，用来存储device返回的应答指令或数据，此时一定要注意buffer的大小，以足够存储所有的数据
                    // bytesRead 发送字节数累计! 发送/接收指令或数据的大小
                    // 40 超时时间      !指令或数据的最长通讯时间，在通讯出现问题时，若超时还未通讯完成，视为通讯失败

                    //bulkTransfer 在USB通讯里可以用做发送也可以用做接收
                    bytesWrite = mDeviceConnection.bulkTransfer(mEndpointOut, bytes,
                            bytesRead, 40);

                    Log.i(LOG_TAG, "bytesRead:" + bytesRead + " bytesWrite:" + bytesWrite);

                    if(bytesWrite < 0) {

                        // 暂停此线程 排除故障 暂时做到这种程度，因为对Java多线程编程不是太熟悉。
                        // 打印被暂停时会进入到这个里面，可能引起的问题有：缺纸 卡纸等等。这个时候要有另外一个线程去获取Printer状态。
                        synchronized (this) {   //线程锁定
                            wait();
                            notifyAll();  //唤醒一个等待的线程
                        }
                    }

                    if(bytesWrite != bytesRead) {
                        Log.e(LOG_TAG, "bultTransfer error!");
                        break;
                    }

                    Thread.sleep(1000);
                }

                //切纸结束


            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if(bufferInputStream != null)
                        bufferInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private byte[] make_bmp_byte(String bmp_path) throws IOException {
            // TODO Auto-generated method stub
            File the_bmp = new File(bmp_path);
            InputStream in = new FileInputStream(the_bmp);
            int data_buffer_length = (int)the_bmp.length();     //这就是文件大小
            System.out.println(" data_buffer_length = " + data_buffer_length );
            byte All_Data_Byte[]= new byte[data_buffer_length];     //创建合适文件大小的数组
            in.read(All_Data_Byte);    //读取文件中的内容到All_Data_Byte[]数组
            in.close();

            //开始整理有效图像到打印缓存Buffer里去
            int buffer_length = (int)the_bmp.length() - 58;
            byte Print_Data_Byte[]= new byte[buffer_length];	//颜色反转
            byte Print_Byte[]= new byte[buffer_length];

            for(int remix_time = 0; remix_time < buffer_length; remix_time ++ )
            {
                Print_Byte [remix_time] = All_Data_Byte[remix_time + 22];	//偏移量
            }

            boolean Color_reversal = false;	//颜色反转
            if (Color_reversal) {
                Print_Data_Byte = change(Print_Byte);
            }

            return Print_Byte;
            //	return Print_Data_Byte;
        }
    }

    private static byte[] zip_function(byte[] in_buffer) {
        // TODO Auto-generated method stub
        byte[] out_buffer = new byte[72]; //72是极端最大值
        /*
         * 压缩算法
         * 因为点阵图片发送数据时,可能图片会有很多空白或者纯黑的画面,
         * 这时可以通过打印机预留的协议,给打印机,
         * 即 重复数据 + 重复数据的长度
         * 这样有多个重复数据的时候,只需要发送一次就可以了.避免重复发送,从而减少发送中损耗的时间
         */
        // 开始遍历原始数据
        //有重复的分出来,没有重复的继续
        int in_buf_point = 0; //原始数据指针
        int out_buf_point = 0; //压缩后的数据指针
        int count = 0;   //重复数据计数器
        byte bLast = 0;		//代表重复值的数据变量
        while(in_buf_point < in_buffer.length)
        {
            if (in_buffer[in_buf_point] == bLast) {
                count = GetRepetitionCount(bLast , in_buf_point,in_buffer , in_buffer.length - in_buf_point );

                //^按位异或, 相同为0 不同为1
                out_buffer[out_buf_point] = (byte) (in_buffer[in_buf_point]^0x55);
                out_buf_point++;
                if(out_buf_point >= in_buffer.length) //全部都^0x55完了.
                    break;

                out_buffer[out_buf_point] = (byte) (count^0x55);
                out_buf_point++;
                if(out_buf_point >= in_buffer.length) //全部都^0x55完了.
                    break;

                in_buf_point = in_buf_point + count ;
            } else {
                out_buffer[out_buf_point] = (byte) (in_buffer[in_buf_point]^0x55);
                out_buf_point++;
                if(out_buf_point >= in_buffer.length)
                    break;

                bLast = (byte) in_buffer[in_buf_point];  //把新的值给变量bLast
            }
            in_buf_point ++;
        }

        byte[] F_out_buffer = new byte[out_buf_point+1]; //72是极端最大值
        while(out_buf_point >= 0)
        {
            F_out_buffer[out_buf_point] = out_buffer[out_buf_point];
            out_buf_point--;
        }

        return F_out_buffer;
    }

    private static int GetRepetitionCount(byte Repe_bLast, int in_buf_point,byte[] in_buffer, int length) {
        // TODO Auto-generated method stub
        int  count = 0;
        //判断字节数组中有几个连续的字节和参考字节的数值相同

        while(length > 0) //数据还没完
        {
            if(in_buffer[in_buf_point] != Repe_bLast)
                break;

            count++;
            if(count >= 255)
                break;

            length--;
            in_buf_point++;
        }

        return count-1;

    }

    private static byte[]change(byte[] buff)
    {
        for( int i=0;i<buff.length;i++)
        {
            int b=0;
            for (int j=0;j<8;j++){
                int bit = (buff[i]>>j&1)==0?1:0;
                b += (1<<j)*bit;
            }
            buff[i]=  (byte) b;
        }
        return buff;
    }

    private static byte Remix_Bit(byte start_byte) {
        // TODO Auto-generated method stub
        //单个字节内的位数依次变换
        String bit_String = RemixByteToBit( start_byte );

        return decodeBinaryString (bit_String);
    }


    /**
     * 把byte转为字符串的bit
     */
    public static String byteToBit(byte b) {
        return ""
                + (byte) ((b >> 7) & 0x1) + (byte) ((b >> 6) & 0x1)
                + (byte) ((b >> 5) & 0x1) + (byte) ((b >> 4) & 0x1)
                + (byte) ((b >> 3) & 0x1) + (byte) ((b >> 2) & 0x1)
                + (byte) ((b >> 1) & 0x1) + (byte) ((b >> 0) & 0x1);
    }

    /**
     * 把byte转为字符串的bit
     */
    public static String RemixByteToBit(byte b) {
        return ""
                + (byte) ((b >> 0) & 0x1) + (byte) ((b >> 1) & 0x1)
                + (byte) ((b >> 2) & 0x1) + (byte) ((b >> 3) & 0x1)
                + (byte) ((b >> 4) & 0x1) + (byte) ((b >> 5) & 0x1)
                + (byte) ((b >> 6) & 0x1) + (byte) ((b >> 7) & 0x1);
    }

    /**
     * 二进制字符串转byte
     */
    public static byte decodeBinaryString(String byteStr) {
        int re, len;
        if (null == byteStr) {
            return 0;
        }
        len = byteStr.length();
        if (len != 4 && len != 8) {
            return 0;
        }
        if (len == 8) {// 8 bit处理
            if (byteStr.charAt(0) == '0') {// 正数
                re = Integer.parseInt(byteStr, 2);
            } else {// 负数
                re = Integer.parseInt(byteStr, 2) - 256;
            }
        } else {// 4 bit处理
            re = Integer.parseInt(byteStr, 2);
        }
        return (byte) re;
    }

    //java 合并两个byte数组
    public static byte[] byteMerger(byte[] byte_1, byte[] byte_2){
        byte[] byte_3 = new byte[byte_1.length+byte_2.length];
        System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);
        System.arraycopy(byte_2, 0, byte_3, byte_1.length, byte_2.length);
        return byte_3;
    }
}

