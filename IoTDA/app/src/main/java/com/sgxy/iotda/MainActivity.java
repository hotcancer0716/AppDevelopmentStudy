package com.sgxy.iotda;
/*
* 华为IoTDA物联网云平台应用侧开发
* 基于API Token方式
* 没有使用JavaSDK
* */
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {

    String TAG = "############";
    String User ="hw18412179"; //华为帐号
    String IAM ="HOT";  //IAM帐号
    String IAMpw ="Hot071600";  //IAM帐号密码
    String StrToken = "StrToken";

    Switch SW_Btn_Light;

    boolean Light_On=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SW_Btn_Light = findViewById(R.id.switch_button_light);
        GetTokenThread getTokenThread = new GetTokenThread();
        getTokenThread.start();

        //GetShadowThread getShadowThread = new GetShadowThread();
        //getShadowThread.start();

        SW_Btn_Light.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean on) {
                if (on){
                    Light_On = false;
                    SendCMDThread sendCMDThread = new SendCMDThread();
                    sendCMDThread.start();
                }   else {
                    Light_On = true;
                    SendCMDThread sendCMDThread = new SendCMDThread();
                    sendCMDThread.start();
                }
            }
        });

    }

    private class SendCMDThread extends  Thread{
        @Override
        public void run() {
            String StrCMD = "OFF";
            String result = new SendCMD().SendCMD(StrToken,StrCMD,Light_On);
            Log.d(TAG, result);

            super.run();
        }
    }

    private  class GetTokenThread extends Thread{
        @Override
        public void run() {
            super.run();
            StrToken = new GetToken().getToken(User,IAM,IAMpw);
            Log.d(TAG, StrToken);

        }
    }

    private class GetShadowThread extends Thread{
        @Override
        public void run() {
            StrToken = new Get_Shadow().Get_Shadow("65fe6d0bfb8177243a50e16b_a0001","token");
            Log.d(TAG, StrToken);

            bearPi_Data bpd = new Gson().fromJson(StrToken,bearPi_Data.class);
            String temp = bpd.getShadow().get(0).getReported().getProperties().getTemperature().toString();
            String hump = bpd.getShadow().get(0).getReported().getProperties().getHumidity().toString();
            String lux = bpd.getShadow().get(0).getReported().getProperties().getLuminance().toString();
            String State_Light = bpd.getShadow().get(0).getReported().getProperties().getLightStatus().toString();
            String State_Motor = bpd.getShadow().get(0).getReported().getProperties().getMotorStatus().toString();

            Log.d(TAG,"当前传感器温度："+temp);
            Log.d(TAG,"当前传感器湿度："+hump);
            Log.d(TAG,"当前传感器光照度："+lux);
            Log.d(TAG,"当前照明状态："+State_Light);
            Log.d(TAG,"当前马达状态："+State_Motor);
            super.run();
        }
    }
}