package com.sgxy.iotda;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class bearPi_Data {

    /**
     * deviceId
     */
    @SerializedName("device_id")
    private String deviceId;
    /**
     * shadow
     */
    @SerializedName("shadow")
    private List<ShadowDTO> shadow;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public List<ShadowDTO> getShadow() {
        return shadow;
    }

    public void setShadow(List<ShadowDTO> shadow) {
        this.shadow = shadow;
    }

    public static class ShadowDTO {
        /**
         * serviceId
         */
        @SerializedName("service_id")
        private String serviceId;
        /**
         * desired
         */
        @SerializedName("desired")
        private DesiredDTO desired;
        /**
         * reported
         */
        @SerializedName("reported")
        private ReportedDTO reported;
        /**
         * version
         */
        @SerializedName("version")
        private Integer version;

        public String getServiceId() {
            return serviceId;
        }

        public void setServiceId(String serviceId) {
            this.serviceId = serviceId;
        }

        public DesiredDTO getDesired() {
            return desired;
        }

        public void setDesired(DesiredDTO desired) {
            this.desired = desired;
        }

        public ReportedDTO getReported() {
            return reported;
        }

        public void setReported(ReportedDTO reported) {
            this.reported = reported;
        }

        public Integer getVersion() {
            return version;
        }

        public void setVersion(Integer version) {
            this.version = version;
        }

        public static class DesiredDTO {
            /**
             * properties
             */
            @SerializedName("properties")
            private Object properties;
            /**
             * eventTime
             */
            @SerializedName("event_time")
            private Object eventTime;

            public Object getProperties() {
                return properties;
            }

            public void setProperties(Object properties) {
                this.properties = properties;
            }

            public Object getEventTime() {
                return eventTime;
            }

            public void setEventTime(Object eventTime) {
                this.eventTime = eventTime;
            }
        }

        public static class ReportedDTO {
            /**
             * properties
             */
            @SerializedName("properties")
            private PropertiesDTO properties;
            /**
             * eventTime
             */
            @SerializedName("event_time")
            private String eventTime;

            public PropertiesDTO getProperties() {
                return properties;
            }

            public void setProperties(PropertiesDTO properties) {
                this.properties = properties;
            }

            public String getEventTime() {
                return eventTime;
            }

            public void setEventTime(String eventTime) {
                this.eventTime = eventTime;
            }

            public static class PropertiesDTO {
                /**
                 * temperature
                 */
                @SerializedName("Temperature")
                private Integer temperature;
                /**
                 * humidity
                 */
                @SerializedName("Humidity")
                private Integer humidity;
                /**
                 * luminance
                 */
                @SerializedName("Luminance")
                private Integer luminance;
                /**
                 * lightStatus
                 */
                @SerializedName("LightStatus")
                private String lightStatus;
                /**
                 * motorStatus
                 */
                @SerializedName("MotorStatus")
                private String motorStatus;

                public Integer getTemperature() {
                    return temperature;
                }

                public void setTemperature(Integer temperature) {
                    this.temperature = temperature;
                }

                public Integer getHumidity() {
                    return humidity;
                }

                public void setHumidity(Integer humidity) {
                    this.humidity = humidity;
                }

                public Integer getLuminance() {
                    return luminance;
                }

                public void setLuminance(Integer luminance) {
                    this.luminance = luminance;
                }

                public String getLightStatus() {
                    return lightStatus;
                }

                public void setLightStatus(String lightStatus) {
                    this.lightStatus = lightStatus;
                }

                public String getMotorStatus() {
                    return motorStatus;
                }

                public void setMotorStatus(String motorStatus) {
                    this.motorStatus = motorStatus;
                }
            }
        }
    }
}
