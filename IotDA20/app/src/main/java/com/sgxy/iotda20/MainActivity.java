package com.sgxy.iotda20;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Switch;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    int[] icon = {R.drawable.bxp,R.drawable.bys,R.drawable.byt,R.drawable.byu,R.drawable.byv,
            R.drawable.byx,R.drawable.byy,R.drawable.byz,R.drawable.bzx,R.drawable.bzy };

    String[] Sensor_name = {"温度：","湿度：","光照：","照明：","马达："};
    String[] Sensor_value = {"25","68","460","开","关"};
    String[] Sensor_unit = {"℃","%","Lux","",""};
    ListView LV;
    SwipeRefreshLayout SR_Layput; //下拉刷新列表
    Switch SW_LED,SW_Motor;
    String str_token = "str_token";
    final String Device_id = "65fe6d0bfb8177243a50e16b_a0001";
    final String Project_id = "65fe6d0bfb8177243a50e16b";
    final String Service_id = "Agriculture";
    String CMD_Name,Paras,StrCMD, Str_token =null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LV = findViewById(R.id.lv_list);
        SW_LED = findViewById(R.id.sw_led);
        SW_Motor = findViewById(R.id.sw_motor);
        SR_Layput = findViewById(R.id.swiperefreshlayout);

        ThreadGetToken threadGetToken = new ThreadGetToken();
        threadGetToken.start();

        SW_LED.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                CMD_Name="Agriculture_Control_light";
                Paras ="Light";
                if (SW_LED.isChecked()){
                    //关闭
                    StrCMD="ON";
                    ThreadSendCMD threadSendCMD = new ThreadSendCMD();
                    threadSendCMD.start();
                }else {
                    //打开
                    StrCMD="OFF";
                    ThreadSendCMD threadSendCMD = new ThreadSendCMD();
                    threadSendCMD.start();
                }
            }
        });

        SW_Motor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                CMD_Name = "Agriculture_Control_Motor";
                Paras = "Motor";
                if (SW_Motor.isChecked()){
                    //关闭
                    StrCMD="ON";
                    ThreadSendCMD threadSendCMD = new ThreadSendCMD();
                    threadSendCMD.start();
                }else {
                    //打开
                    StrCMD="OFF";
                    ThreadSendCMD threadSendCMD = new ThreadSendCMD();
                    threadSendCMD.start();
                }
            }
        });

        SR_Layput.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ThreadGetShadow getShadow = new ThreadGetShadow();
                getShadow.start();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Show_Senser();
    }

    private class ThreadGetShadow extends Thread{
        @Override
        public void run() {
            Get_Shadow getShadow = new Get_Shadow();
            String json_Shadow = getShadow.Get_Shadow(Device_id,str_token);
            Message msg = new Message();
            msg.what = 222;
            Bundle bundle = new Bundle();
            bundle.putString("result",json_Shadow);
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
    }
    private  class  ThreadGetToken extends Thread{
        @Override
        public void run() {
            //获取Token
            String User ="hw18412179"; //华为帐号
            String IAM ="HOT";  //IAM帐号
            String IAMpw ="Hot071600";  //IAM帐号密码

            Get_Token get_token = new Get_Token();
            str_token = get_token.Get_Token(User,IAM,IAMpw);
            Log.d("TAG", str_token);

            //因为要等token获取成功才能显示影子数据，所以这里需要给handler发送数据
            Message msg = new Message();
            msg.what = 111; //通知其他线程Token获取到了
            handler.sendMessage(msg);
        }
    }

    private class ThreadSendCMD extends Thread{
        @Override
        public void run() {
            SendCMD sendCMD = new SendCMD();
            sendCMD.SendCMD(Project_id,Device_id,Service_id,CMD_Name,Paras,StrCMD,str_token);
        }
    }
    Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case 111:
                    //收到token消息，开始获取影子,
                    ThreadGetShadow getShadow = new ThreadGetShadow();
                    getShadow.start();
                    break;

                case 222:
                    String StrData = msg.getData().getString("result","get json fail");
                    Shadow_Data shadow_data = new Gson().fromJson(StrData,Shadow_Data.class);

                    String temp = shadow_data.getShadow().get(0).getReported().getProperties().getTemperature().toString();
                    String hump = shadow_data.getShadow().get(0).getReported().getProperties().getHumidity().toString();
                    String Lux = shadow_data.getShadow().get(0).getReported().getProperties().getLuminance().toString();
                    String light = shadow_data.getShadow().get(0).getReported().getProperties().getLightStatus().toString();
                    String motor = shadow_data.getShadow().get(0).getReported().getProperties().getMotorStatus().toString();
                    String[] new_value = {temp,hump,Lux,light,motor};
                    Sensor_value = new_value;

                    Show_Senser();
                    SR_Layput.setRefreshing(false); //让listviw停止刷新
                    break;
            }

        }
    };
    private void Show_Senser(){
        //初始化List
        List<HashMap<String,Object>> list = new ArrayList<HashMap<String,Object>>();
        for (int i = 0 ; i<Sensor_name.length ; i++){
            HashMap<String,Object> map = new HashMap<String,Object>();
            map.put("k_senser_name",Sensor_name[i]);
            map.put("k_sensor_value",Sensor_value[i]);
            map.put("k_sensor_unit",Sensor_unit[i]);
            map.put("k_sensor_icon",icon[i]);

            list.add(map);
        }
        //初始化适配器adapter
        SimpleAdapter adapter = new SimpleAdapter(
                MainActivity.this,
                list,
                R.layout.item_layout,
                new String[]{"k_sensor_icon","k_senser_name","k_sensor_value","k_sensor_unit"},
                new int[]{R.id.img_pic,R.id.tx_sensor_name,R.id.tx_sensor_value,R.id.tx_sensor_unit}
        );

        LV.setAdapter(adapter);
    }
}