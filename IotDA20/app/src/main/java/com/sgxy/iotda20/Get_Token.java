package com.sgxy.iotda20;

import android.util.Log;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Get_Token {
    public String Get_Token(String User ,String IAM ,String IAMpw){
        String  token = "get token fail";
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json;charset=UTF-8");
        RequestBody body = RequestBody.create(mediaType, "{\"auth\":{\"identity\":{\"methods\":[\"password\"],\"password\":{\"user\":{\"domain\":{\"name\":\""+User+"\"},\"name\":\""+IAM+"\",\"password\":\""+IAMpw+"\"}}}}}");
        Request request = new Request.Builder()
                .url("https://iam.cn-north-4.myhuaweicloud.com/v3/auth/tokens?0={&1=}")
                .method("POST", body)
                .addHeader("Content-Type", "application/json;charset=UTF-8")
                .build();
        try {
            Response response = client.newCall(request).execute();
            token = response.headers().get("X-Subject-Token");
            Log.d("#############", response.body().string());
        } catch (IOException e){
            Log.d("#############", "IOException!!!!");
        }

        return token;
    }

}
