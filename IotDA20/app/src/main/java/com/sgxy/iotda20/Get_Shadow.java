package com.sgxy.iotda20;

import android.util.Log;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Get_Shadow {
    public String  Get_Shadow(String device_id,String newToken){
        String result_json = "";
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/plain");
        Request request = new Request.Builder()
                .url("https://iotda.cn-north-4.myhuaweicloud.com/v5/iot/0d436836ae00f3112f4fc001f7bcf33d/devices/"+device_id+"/shadow")
                .addHeader("X-Sdk-Content-Sha256","UNSIGNED-PAYLOAD")
                .addHeader("X-Security-Token",newToken)
                .addHeader("x-auth-token",newToken)
                .addHeader("Host","a161169cbc.iotda.cn-north-4.myhuaweicloud.com:443")
                .addHeader("X-Sdk-Date","20240330T064601Z")
                .addHeader("X-Language","zh-cn")
                .addHeader("X-Project-Id","65fe6d0bfb8177243a50e16b")
                .addHeader("Content-Type","application/json")
                .build();
        try {
            Response response = client.newCall(request).execute();
            result_json = response.body().string();
            Log.d("#############json", result_json);
        } catch (IOException e){
            Log.d("#############json", e.getMessage());
        }
        return result_json;
    }
}
