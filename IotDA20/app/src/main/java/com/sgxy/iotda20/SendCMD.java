package com.sgxy.iotda20;

import android.util.Log;
import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SendCMD {
public String SendCMD(String project_id,String Device_id,String Service_id,String CMD_Name,String paras,String StrCMD, String Str_token){

    OkHttpClient client = new OkHttpClient().newBuilder().build();
    MediaType mediaType = MediaType.parse("text/plain");
    RequestBody body ;
    body = RequestBody.create(mediaType, "{\"service_id\":\""+Service_id+"\",\"command_name\":\""+CMD_Name+"\",\"paras\":{\""+paras+"\":\""+StrCMD+"\"}}");

    Request request = new Request.Builder()
                .url("https://iotda.cn-north-4.myhuaweicloud.com/v5/iot/0d436836ae00f3112f4fc001f7bcf33d/devices/"+Device_id+"/commands")
                .method("POST", body)
                .addHeader("0", "{")
                .addHeader("x-auth-token",Str_token)
                .addHeader("X-Security-Token",Str_token)
                .addHeader("Content-Type","application/json")
                .addHeader("X-Project-Id",project_id)
                .addHeader("X-Sdk-Content-Sha256","UNSIGNED-PAYLOAD")
                .addHeader("X-Sdk-Date","20240330T035323Z")
                .addHeader("Host","a161169cbc.iotda.cn-north-4.myhuaweicloud.com:443")
                .addHeader("1", "}")
                .build();
        try {
            Response response = client.newCall(request).execute();

            Log.d("#############", response.body().string());
        } catch (IOException e){
            Log.d("#############", e.getMessage());
        }
        return "ok";
    }
}
