package com.sgxy_ict.glide_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class MainActivity extends AppCompatActivity {

    ImageView IMG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        IMG = findViewById(R.id.pic);
        String url ="https://i0.hdslb.com/bfs/article/fff132acf1b683fc6fcbdcaace4fe31d1738aab3.gif";
        //IMG.setImageResource(R.mipmap.pic1);
        Glide.with(MainActivity.this).load(url).into(IMG);
    }
}