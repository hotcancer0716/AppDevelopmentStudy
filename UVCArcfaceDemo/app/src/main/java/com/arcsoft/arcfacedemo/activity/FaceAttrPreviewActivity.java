package com.arcsoft.arcfacedemo.activity;

import android.Manifest;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Toast;

import com.arcsoft.arcfacedemo.R;
import com.arcsoft.arcfacedemo.model.DrawInfo;
import com.arcsoft.arcfacedemo.util.ConfigUtil;
import com.arcsoft.arcfacedemo.util.DrawHelper;
import com.arcsoft.arcfacedemo.util.camera.CameraHelper;
import com.arcsoft.arcfacedemo.util.camera.CameraListener;
import com.arcsoft.arcfacedemo.util.face.RecognizeColor;
import com.arcsoft.arcfacedemo.widget.FaceRectView;
import com.arcsoft.face.AgeInfo;
import com.arcsoft.face.ErrorInfo;
import com.arcsoft.face.Face3DAngle;
import com.arcsoft.face.FaceEngine;
import com.arcsoft.face.FaceInfo;
import com.arcsoft.face.GenderInfo;
import com.arcsoft.face.LivenessInfo;
import com.arcsoft.face.VersionInfo;
import com.arcsoft.face.enums.DetectMode;
import com.arcsoft.face.model.ArcSoftImageInfo;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class FaceAttrPreviewActivity extends BaseActivity implements ViewTreeObserver.OnGlobalLayoutListener {
    private static final String TAG = "FaceAttrPreviewActivity";
    private CameraHelper cameraHelper;
    private DrawHelper drawHelper;
    private Camera.Size previewSize;
    private Integer rgbCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
    private FaceEngine faceEngine;
    private int afCode = -1;
    private int Xarm_times = 10;
    private int processMask = FaceEngine.ASF_AGE | FaceEngine.ASF_FACE3DANGLE | FaceEngine.ASF_GENDER | FaceEngine.ASF_LIVENESS;
    /**
     * 相机预览显示的控件，可为SurfaceView或TextureView
     */
    private View previewView;
    private FaceRectView faceRectView;

    private static final int ACTION_REQUEST_PERMISSIONS = 0x001;
    /**
     * 所需的所有权限信息
     */
    private static final String[] NEEDED_PERMISSIONS = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_PHONE_STATE
    };

    private UsbManager mManager;
    private UsbDevice mDevice;
    private UsbDeviceConnection mDeviceConnection;
    private UsbInterface mInterface;
    private static String LOG_TAG = "HOT<WLEI>";

    AssetManager mAssetManager = null;
    InputStream mInputStream = null;
    //	UsbDevice device;
    byte[] buf_commod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_attr_preview);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WindowManager.LayoutParams attributes = getWindow().getAttributes();
            attributes.systemUiVisibility = View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            getWindow().setAttributes(attributes);
        }

        // Activity启动后就锁定为启动时的方向
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        previewView = findViewById(R.id.texture_preview);
        faceRectView = findViewById(R.id.face_rect_view);
        //在布局结束后才做初始化操作
        previewView.getViewTreeObserver().addOnGlobalLayoutListener(this);
        //获得一个view的宽度和高度的方法

        init_usb();
    }

    private void initEngine() {
        faceEngine = new FaceEngine();
        afCode = faceEngine.init(this, DetectMode.ASF_DETECT_MODE_VIDEO, ConfigUtil.getFtOrient(this),
                16, 20, FaceEngine.ASF_FACE_DETECT | FaceEngine.ASF_AGE | FaceEngine.ASF_FACE3DANGLE | FaceEngine.ASF_GENDER | FaceEngine.ASF_LIVENESS);
        Log.i(TAG, "initEngine:  init: " + afCode);
        if (afCode != ErrorInfo.MOK) {
            showToast( getString(R.string.init_failed, afCode));
        }
    }

    private void unInitEngine() {

        if (afCode == 0) {
            afCode = faceEngine.unInit();
            Log.i(TAG, "unInitEngine: " + afCode);
        }
    }


    @Override
    protected void onDestroy() {
        if (cameraHelper != null) {
            cameraHelper.release();
            cameraHelper = null;
        }
        unInitEngine();
        super.onDestroy();
    }


    //initCamera是核心技术点，视频采集，显示，人脸识别，方框绘制与分析的结果显示
    private void initCamera() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        //CameraListener是虹软自己写的监听用的类
        CameraListener cameraListener = new CameraListener() {
            @Override
            public void onCameraOpened(Camera camera, int cameraId, int displayOrientation, boolean isMirror) {
                Log.i(TAG, "onCameraOpened: " + cameraId + "  " + displayOrientation + " " + isMirror);
                previewSize = camera.getParameters().getPreviewSize();
                //创建一个绘制辅助类对象，并且设置绘制相关的参数
                drawHelper = new DrawHelper(previewSize.width, previewSize.height, previewView.getWidth(), previewView.getHeight(), displayOrientation
                        , cameraId, isMirror, false, false);
            }


            @Override
            public void onPreview(byte[] nv21, Camera camera) {

                if (faceRectView != null) {
                    faceRectView.clearFaceInfo();
                }
                List<FaceInfo> faceInfoList = new ArrayList<>();
//                long start = System.currentTimeMillis();
                int code = faceEngine.detectFaces(nv21, previewSize.width, previewSize.height, FaceEngine.CP_PAF_NV21, faceInfoList);
                if (code == ErrorInfo.MOK && faceInfoList.size() > 0) {
                    code = faceEngine.process(nv21, previewSize.width, previewSize.height, FaceEngine.CP_PAF_NV21, faceInfoList, processMask);
                    if (code != ErrorInfo.MOK) {
                        return;
                    }
                } else {
                    return;
                }

                List<AgeInfo> ageInfoList = new ArrayList<>();
                List<GenderInfo> genderInfoList = new ArrayList<>();
                List<Face3DAngle> face3DAngleList = new ArrayList<>();
                List<LivenessInfo> faceLivenessInfoList = new ArrayList<>();
                int ageCode = faceEngine.getAge(ageInfoList);
                int genderCode = faceEngine.getGender(genderInfoList);
                int face3DAngleCode = faceEngine.getFace3DAngle(face3DAngleList);
                int livenessCode = faceEngine.getLiveness(faceLivenessInfoList);

                // 有其中一个的错误码不为ErrorInfo.MOK，return
                if ((ageCode | genderCode | face3DAngleCode | livenessCode) != ErrorInfo.MOK) {
                    return;
                }
                if (faceRectView != null && drawHelper != null) {
                    List<DrawInfo> drawInfoList = new ArrayList<>();
                    for (int i = 0; i < faceInfoList.size(); i++) {
                        drawInfoList.add(new DrawInfo(drawHelper.adjustRect(faceInfoList.get(i).getRect()), genderInfoList.get(i).getGender(), ageInfoList.get(i).getAge(), faceLivenessInfoList.get(i).getLiveness(), RecognizeColor.COLOR_UNKNOWN, null));
                    }
                    drawHelper.draw(faceRectView, drawInfoList);

                    Xarm_times --;
                    if (Xarm_times == 0){
                        //在这里增加机械臂控制，USB的初始化要放在Activity Create里
                        XArm_Move(drawInfoList.get(0).getRect().left + drawInfoList.get(0).getRect().width() / 2
                                , drawInfoList.get(0).getRect().top + drawInfoList.get(0).getRect().height()/2
                                , drawInfoList.get(0).getRect().width());
                        Xarm_times = 10;
                    }
                }
            }

            @Override
            public void onCameraClosed() {
                Log.i(TAG, "onCameraClosed: ");
            }

            @Override
            public void onCameraError(Exception e) {
                Log.i(TAG, "onCameraError: " + e.getMessage());
            }

            @Override
            public void onCameraConfigurationChanged(int cameraID, int displayOrientation) {
                if (drawHelper != null) {
                    drawHelper.setCameraDisplayOrientation(displayOrientation);
                }
                Log.i(TAG, "onCameraConfigurationChanged: " + cameraID + "  " + displayOrientation);
            }
        };
        cameraHelper = new CameraHelper.Builder()
                .previewViewSize(new Point(previewView.getMeasuredWidth(), previewView.getMeasuredHeight()))
                .rotation(getWindowManager().getDefaultDisplay().getRotation())
                .specificCameraId(rgbCameraId != null ? rgbCameraId : Camera.CameraInfo.CAMERA_FACING_FRONT)
                .isMirror(true)//水平镜像显示（若相机是镜像显示的，设为true，用于纠正）
                .previewOn(previewView)  //这里是那个显示视频的控件，View previewView，布局里面是TextureView。
                .cameraListener(cameraListener)
                .build();
        cameraHelper.init();
        cameraHelper.start();
    }

    @Override
    void afterRequestPermission(int requestCode, boolean isAllGranted) {
        if (requestCode == ACTION_REQUEST_PERMISSIONS) {
            if (isAllGranted) {
                initEngine();
                initCamera();
            } else {
                showToast(getString( R.string.permission_denied));
            }
        }
    }

    /**
     * 在{@link #previewView}第一次布局完成后，去除该监听，并且进行引擎和相机的初始化
     */
    @Override
    public void onGlobalLayout() {
        previewView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        if (!checkPermissions(NEEDED_PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, NEEDED_PERMISSIONS, ACTION_REQUEST_PERMISSIONS);
        } else {
            initEngine();
            initCamera();
        }
    }


    /**
     * 切换相机。注意：若切换相机发现检测不到人脸，则极有可能是检测角度导致的，需要销毁引擎重新创建或者在设置界面修改配置的检测角度
     *
     * @param view
     */
    public void switchCamera(View view) {
        XArm_Stop();
        if (cameraHelper != null) {
            boolean success = cameraHelper.switchCamera();
            if (!success) {
                showToast(getString(R.string.switch_camera_failed));
            } else {
                showLongToast(getString(R.string.notice_change_detect_degree));
            }
        }
    }

    private void init_usb() {
        // TODO Auto-generated method stub
        mManager = (UsbManager) getSystemService(Context.USB_SERVICE);

        // listen for new devices
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);	//开始监控USB激活
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);	//开始监控USB拔出
        registerReceiver(mUsbReceiver, filter);   //启动事先声明的系统广播!

        //增加机械臂复位动作
        XArm_Reset();
        XArm_Reset();
    }


    private int XArm_Move(int x , int y , int z){
        Log.d(LOG_TAG , "x = :"+x +"     y=:"+y +"     z:"+z);

        byte[] buf_move = {0x55 ,0x55 ,0x18 ,0x03 ,0x06 ,(byte)0xf4 ,0x01 ,
                0x01 ,0x14 ,0x05 ,
                0x02 ,0x14 ,0x05 ,
                0x03 ,0x4c ,0x04 ,
                0x04 ,0x4c ,0x04 ,
                0x05 ,0x14 ,0x05 ,
                0x06 ,0x14 ,0x05};  //控制机械臂完成1个动作

        x=2*x;
        byte[] buf_id6 = toLH(x);
        buf_move[23] = buf_id6[0];
        buf_move[24] = buf_id6[1];

        int z_id3 = 1600-y;
        byte[] buf_id3 = toLH(z_id3);
        buf_move[14] = buf_id3[0];
        buf_move[15] = buf_id3[1];

        int z_id5 = 1600-z;
        byte[] buf_id5 = toLH(z_id5);
        buf_move[20] = buf_id5[0];
        buf_move[21] = buf_id5[1];

        z=3*z;
        byte[] buf_id4 = toLH(z);
        buf_move[17] = buf_id4[0];
        buf_move[18] = buf_id4[1];

        buf_commod = buf_move;

        //初始化USB设备为打印机设备
        XArmDevice xArmDevice = new XArmDevice(buf_commod, mDeviceConnection, mInterface);
        //开始控制
        xArmDevice.start(); //适合命令发送
        return 0;
    }

    private int XArm_Stop(){

        byte[] buf_stop = {0x55 ,0x55 ,0x18 ,0x03 ,0x06 ,(byte)0xe8 ,0x03
                ,0x01 ,0xc ,0x05 ,0x02 ,(byte)0xdc ,0x05 ,0x03 ,(byte)0xdc ,0x05
                ,0x04 ,(byte)0xdc ,0x05 ,0x05 ,(byte)0xdc ,0x05 ,0x06 ,(byte)0xdc ,0x05};
        buf_commod = buf_stop;

        //给USB发送控制命令到机械臂控制器
        XArmDevice xArmDevice = new XArmDevice(buf_commod, mDeviceConnection, mInterface);
        //开始控制
        xArmDevice.start(); //适合命令发送

        return 0;
    }

    private int XArm_Reset(){

        byte[] buf_reset = {0x55 ,0x55 ,0x18 ,0x03 ,0x06 ,(byte)0xe8 ,0x03
                ,0x01 ,0xc ,0x05 ,0x02 ,(byte)0xdc ,0x05 ,0x03 ,(byte)0xdc ,0x05
                ,0x04 ,(byte)0xdc ,0x05 ,0x05 ,(byte)0xdc ,0x05 ,0x06 ,(byte)0xdc ,0x05};
        buf_commod = buf_reset;

        for(UsbDevice device_new : mManager.getDeviceList().values()) {
            //从device信息里提炼出UsbInterface接口信息
            UsbInterface intf = findPrinterInterface(device_new);
            //如果设置USB总线上设备为打印机类型成功,就退出循环,(上一条函数返回值为intf,如果不是打印机类型intf值为null)
            if (setPrinterInterface(device_new, intf)) {
                break;
            }
        }
        return 0;
    }

    public static byte[] toLH(int n) {
        byte[] b = new byte[4];
        b[0] = (byte) (n & 0xff);
        b[1] = (byte) (n >> 8 & 0xff);
        b[2] = (byte) (n >> 16 & 0xff);
        b[3] = (byte) (n >> 24 & 0xff);
        return b;
    }

    BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            //获取到USB有设备插入,初始化
            if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                UsbDevice device = intent
                        .getParcelableExtra(UsbManager.EXTRA_DEVICE);
                String deviceName = device.getDeviceName();
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                //USB设备拔出
                UsbDevice device = intent
                        .getParcelableExtra(UsbManager.EXTRA_DEVICE);
                String deviceName = device.getDeviceName();
                if (mDevice != null && mDevice.equals(deviceName)) {
                    Log.i(LOG_TAG, "printer interface removed");     //拔出USB接口弹出!!
                    setPrinterInterface(null, null);
                }
            }
        }
    };


    // searches for an adb interface on the given USB device
    // 从一大堆USB设备里面筛选出打印机设备
    static private UsbInterface findPrinterInterface(UsbDevice device) {
        Log.d(LOG_TAG, "WL findPrinterInterface " + device);
        int count = device.getInterfaceCount(); //遍历USB总线上有几个设备
        for (int i = 0; i < count; i++) {
            UsbInterface intf = device.getInterface(i);
            Log.i("HOT Forever", "USB_CLASS is"+intf.getInterfaceClass());

            //经测试得到该USB机械臂设备是USB_CLASS_VENDOR_SPEC ，编号255 0xff
            //20220812 可以在下面进行判断，再进行串口通信。
            //USB_CLASS_VENDOR_SPEC判断intf里USB设备class是不是对应的类型! class=255 是该机械臂设备
            if (intf.getInterfaceClass() == UsbConstants.USB_CLASS_VENDOR_SPEC)
            {
             //   TX_DEBUG.setText("USBcount = "+ count +" !!! "+ device +" |||| "+ intf);
                Log.i(LOG_TAG, "USBcount = "+ count +" !!! "+ device +" |||| "+ intf);
                Log.i(LOG_TAG, "Printer");
                return intf; //发现USB总线上有1个打印机设备,把设备信息返回!
            }
        }
        return null;
    }


    // Sets the current USB device and interface
    //研究这个!!重点!
    //把USB和打印机设备接口相连,配对
    private boolean setPrinterInterface(UsbDevice device, UsbInterface intf) {
        //一些USB相关参数初始化前的清零
        if (mDeviceConnection != null) {
            if (mInterface != null) {							//UsbInterface
                mDeviceConnection.releaseInterface(mInterface); //UsbDeviceConnection
                mInterface = null;
            }
            mDeviceConnection.close();
            mDevice = null;
            mDeviceConnection = null;
        }
        openUsbDevice();
        // [5] 检查usb权限
        boolean hasPermission = mUsbManager.hasPermission(device);
        if (!hasPermission) {
            Log.i(LOG_TAG, "no usb permission");
            //[7] 申请usb权限
            PendingIntent mPermissionIntent = PendingIntent.getBroadcast(this, 9898, new Intent(ACTION_USB_PERMISSION), PendingIntent.FLAG_ONE_SHOT);
            IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
            registerReceiver(mUsbReceiver, filter);
            mUsbManager.requestPermission(device, mPermissionIntent);
            Log.i(LOG_TAG, "usb permission is " + mUsbManager.hasPermission(device));
        }

        if (device != null && intf != null) {
            //打开设备连接
            UsbDeviceConnection connection = mManager.openDevice(device);  //open
            //如果打开成功
            if (connection != null) {
                Log.i(LOG_TAG, "open succeeded");
                //claim提出要求连接
                if (connection.claimInterface(intf, true)) {
                    mDevice = device;     			  //以下全部传递到全局变量
                    mDeviceConnection = connection;
                    mInterface = intf;

                    //初始化USB设备为打印机设备
                    XArmDevice xArmDevice = new XArmDevice(buf_commod, mDeviceConnection, mInterface);
                    //开始控制
                    xArmDevice.start(); //适合命令发送
                    return true;
                } else {
                    Log.i(LOG_TAG, "claim interface failed");  //这里出现过
                    connection.close();
                }
            } else {
                Log.i(LOG_TAG, "open failed");
            }
        }

        return false;
    }

    /**
     * 获得 usb 权限
     */
    private void openUsbDevice(){
        //before open usb device
        //should try to get usb permission
        tryGetUsbPermission();
    }
    UsbManager mUsbManager;
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

    private void tryGetUsbPermission(){
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        registerReceiver(mUsbPermissionActionReceiver, filter);

        PendingIntent mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);

        //here do emulation to ask all connected usb device for permission
        for (final UsbDevice usbDevice : mUsbManager.getDeviceList().values()) {
            //add some conditional check if necessary
            //if(isWeCaredUsbDevice(usbDevice)){
            if(mUsbManager.hasPermission(usbDevice)){
                //if has already got permission, just goto connect it
                //that means: user has choose yes for your previously popup window asking for grant perssion for this usb device
                //and also choose option: not ask again
                afterGetUsbPermission(usbDevice);
            }else{
                //this line will let android popup window, ask user whether to allow this app to have permission to operate this usb device
                mUsbManager.requestPermission(usbDevice, mPermissionIntent);
            }
            //}
        }
    }


    private void afterGetUsbPermission(UsbDevice usbDevice){
        //call method to set up device communication
        //Toast.makeText(this, String.valueOf("Got permission for usb device: " + usbDevice), Toast.LENGTH_LONG).show();
        //Toast.makeText(this, String.valueOf("Found USB device: VID=" + usbDevice.getVendorId() + " PID=" + usbDevice.getProductId()), Toast.LENGTH_LONG).show();

        doYourOpenUsbDevice(usbDevice);
    }

    private void doYourOpenUsbDevice(UsbDevice usbDevice){
        //now follow line will NOT show: User has not given permission to device UsbDevice
        UsbDeviceConnection connection = mUsbManager.openDevice(usbDevice);
        //add your operation code here
    }

    private final BroadcastReceiver mUsbPermissionActionReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice usbDevice = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        //user choose YES for your previously popup window asking for grant perssion for this usb device
                        if(null != usbDevice){
                            afterGetUsbPermission(usbDevice);
                        }
                    }
                    else {
                        //user choose NO for your previously popup window asking for grant perssion for this usb device
                        Toast.makeText(context, String.valueOf("Permission denied for device" + usbDevice), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    };
}
