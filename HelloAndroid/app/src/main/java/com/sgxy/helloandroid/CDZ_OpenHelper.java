package com.sgxy.helloandroid;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class CDZ_OpenHelper extends SQLiteOpenHelper {

    public  CDZ_OpenHelper(Context context,String name , SQLiteDatabase.CursorFactory factory , int version){
        super(context, name, factory, version);
    }
    //创建数据库，创建表
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String create_table = "create table stage(_id integer primary key,st_name text,distance text,price text,state integer )";
        sqLiteDatabase.execSQL(create_table);
        Log.d("TAG", "成功创建数据库: ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}


/*
* Adapter的作用
* listview和Gridview有何不同
* Android存储方式有哪些
* Shareprefence有什么特点，轻量化，键值对，本质是xml文件，用法简单，通常用来存储用户名，密码，等配置信息
* Shareprefence写入数据时的步骤及注意事项
* SQLite的特点，功能强，轻量化，体积小，效率高，跨平台，文件型，通用
* SQLite数据库文件的存储路径 /data/data/<package>/databases
* SQLiteOpenHelper类的方法 2个 onCreate()   onUpgrade()
*/