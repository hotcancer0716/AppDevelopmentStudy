package com.sgxy.helloandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class SecondActivity extends AppCompatActivity {
    String TAG ="----->";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_weather);

        Log.d(TAG, "2  onCreate: ");
        //接收Intent传过来的Bundle
        Intent second_intent = getIntent();
        Bundle second_bundle = second_intent.getExtras();
        //解析Bundle
        String name = second_bundle.getString("key_msg","空数据").toString();

        Log.d(TAG, "接收到的用户名是：     "+ name);
    }


    @Override
    protected void onStart() {
        Log.d(TAG, "2  onStart: ");
        super.onStart();
    }

    @Override
    protected void onRestart() {
        Log.d(TAG, "2  onRestart: ");
        super.onRestart();
    }


    @Override
    protected void onResume() {
        Log.d(TAG, "2  onResume: ");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "2  onPause: ");
        super.onPause();
    }


    @Override
    protected void onStop() {
        Log.d(TAG, "2  onStop: ");
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        Log.d(TAG, "2  onDestroy: ");
        super.onDestroy();
    }
}