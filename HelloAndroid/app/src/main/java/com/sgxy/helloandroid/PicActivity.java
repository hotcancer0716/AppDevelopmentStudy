package com.sgxy.helloandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;

public class PicActivity extends AppCompatActivity {

    private int[] imgs ={R.drawable.pic01,R.drawable.pic02,
            R.drawable.pic03,R.drawable.pic04,
            R.drawable.pic05,R.drawable.pic06};

    int index = 0;
    String TAG ="---->";
    ImageView Img_PIC;
    SeekBar Sbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pic);

        Img_PIC = findViewById(R.id.pic);
        Button Btb_Left = findViewById(R.id.btn_left);
        Button Btb_Right = findViewById(R.id.btn_right);
        Sbar =findViewById(R.id.sb_bar);

        Img_PIC.setImageResource(imgs[index]);
        //按键监听
        Btb_Left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                index--;
                if(index <=0)
                    index =imgs.length-1;
                Log.d(TAG, "index: "+index);
                Img_PIC.setImageResource(imgs[index]);

            }
        });

        Btb_Right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                index++;
                if (index>=imgs.length)
                    index=0;
                Log.d(TAG, "index: "+index);
                Img_PIC.setImageResource(imgs[index]);
            }
        });

        Sbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                Log.d(TAG, "onProgressChanged: "+progress);
                Img_PIC.setImageResource(imgs[progress]);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x0=0;
        float x1=0;
        //监听触屏事件
        if(event.getAction() == MotionEvent.ACTION_DOWN){
            x0 = event.getX();
            //Log.d(TAG, "onTouchEvent: 触屏按下  "+x0);
        }

        if(event.getAction() == MotionEvent.ACTION_UP){
            x1 = event.getX();

            if(x0 - x1 > 50){
                Log.d(TAG, "左划 ");
                index++;
                if (index>=imgs.length)
                    index=0;
                Log.d(TAG, "index: "+index);
                Img_PIC.setImageResource(imgs[index]);
            }
            if(x0 - x1 < 50){
                Log.d(TAG, "右划 ");
                index--;
                if(index <=0)
                    index =imgs.length-1;
                Log.d(TAG, "index: "+index);
                Img_PIC.setImageResource(imgs[index]);
            }
        }

        return super.onTouchEvent(event);
    }
}