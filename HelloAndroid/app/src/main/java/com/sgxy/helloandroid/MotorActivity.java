package com.sgxy.helloandroid;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cursoradapter.widget.SimpleCursorAdapter;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;

public class MotorActivity extends AppCompatActivity {

    ListView LV;
    ImageView Img;
    EditText Et_name,Et_distance,Et_price;
    Button Btn_Ok;
    Switch SWB;

    SQLiteDatabase sqldb;
    Cursor cursor;

    int updataID = 0;
    boolean state_insert = false;  //true是更新数据,false是插入新数据
    String id_key = "";

    SharedPreferences SPF;
    String TAG = "--->>>";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motor);

        LV = findViewById(R.id.lv_stage);
        Et_name = findViewById(R.id.ed_name);
        Et_distance = findViewById(R.id.ed_distance);
        Et_price = findViewById(R.id.ed_price);
        Btn_Ok = findViewById(R.id.btn_save);
        Img = findViewById(R.id.pic);
        SWB = findViewById(R.id.sw_b);

        //1.创建SharedPreferences对象
        SPF = getSharedPreferences("my_spf", Context.MODE_PRIVATE);

        //创建（连接）SQLite数据库(opneHelper)
        CDZ_OpenHelper cdz_openHelper = new CDZ_OpenHelper(MotorActivity.this,"mysql.db",null,1);
        SQLiteDatabase mysqlite = cdz_openHelper.getWritableDatabase();

        //mysqlite.
        Log.d(TAG, "onCreate: ");

        SWB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                state_insert = b;
                if (b){
                    Log.d(TAG, "onCheckedChanged: "+b);
                    //更新状态
                    SWB.setText("更新");
                }else{
                    Log.d(TAG, "onCheckedChanged: "+b);
                    //新增数据状态
                    SWB.setText("新增");

                }

            }
        });
        Btn_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //声明
                ContentValues values = new ContentValues();
                values.put("st_name",Et_name.getText().toString());
                values.put("distance",Et_distance.getText().toString());
                values.put("price",Et_price.getText().toString());
                values.put("state",list_head_pic[index]);
                if (state_insert){
                    //更新
                    mysqlite.update("stage",values,"_id=?",new String[]{""+updataID});

                }else {
                    //往数据库插入数据
                    mysqlite.insert("stage",null,values);
                }

                //刷新界面
                Show_SQL(mysqlite);
            }
        });


        //查询数据，并显示
        //刷新界面
        Show_SQL(mysqlite);

        //lietview的监听
        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                cursor.moveToPosition(position); //控制cursor到指定的行
                int myid = cursor.getInt(0);
                String myname = cursor.getString(1);
                String mydistance = cursor.getString(2);
                String myprice = cursor.getString(3);

                Et_name.setText(myname);
                Et_distance.setText(mydistance);
                Et_price.setText(myprice);

                updataID = myid;
                Log.d(TAG, ""+myid);
            }
        });

        //对长按事件的监听
        LV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                //长按事件
               //启动对话框
                //1. 构造器builder 2.show()
                AlertDialog.Builder builder = new AlertDialog.Builder(MotorActivity.this);
                builder.setTitle("警告！！！").setMessage("确认删除吗？").setNegativeButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //删除指定的行和数据
                        //获取该行对应的_id值
                        cursor.moveToPosition(position); //控制cursor到指定的行
                        int myid = cursor.getInt(0);
                        String myname = cursor.getString(1);

                        //通过API delete删除数据
                        mysqlite.delete("stage","_id=?",new String[]{""+myid});

                        Log.d(TAG, "onItemLongClick: "+"触发长按事件");

                        //刷新界面
                        Show_SQL(mysqlite);
                    }
                }).setPositiveButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).show();



                return false;
            }
        });
    }


    private void Show_SQL(SQLiteDatabase sqlite){
        //查询数据，并显示
        //Cursor游标，查询出来的数据库结果在内存中的临时表格
        cursor =  sqlite.query("stage",
                new String[]{"_id","st_name","distance","price","state"},
                null,null,null,null,"_id DESC",null);

        //SimpleCursorAdapter的特点，支持Cursor游标类型的数据，专门用在数据库Sqlite显示
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(
                MotorActivity.this,
                R.layout.item_layout,
                cursor,
                new String[]{"st_name","distance","price"},
                new int[]{R.id.tx_stage_name,R.id.tx_stage_distance,R.id.tx_stage_price},
                0 );
        //绑定适配器
        LV.setAdapter(cursorAdapter);

    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");
        //写入数据
        //2.获取editor对象
        SharedPreferences.Editor editor = SPF.edit();
        //3.存储数据
        editor.putString("key_name",Et_name.getText().toString());
        editor.putString("key_dis",Et_distance.getText().toString());
        editor.putString("key_price",Et_price.getText().toString());
        //4. 提交数据
        editor.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //读取
        Et_name.setText(SPF.getString("key_name"," "));
        Et_distance.setText(SPF.getString("key_dis"," "));
        Et_price.setText(SPF.getString("key_price"," "));
    }

    float x1,x2;
    int[] list_head_pic = {R.drawable.ayc,R.drawable.ayb,
            R.drawable.aye};
    int index = 0;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //获取按下时的x轴坐标
        if(MotionEvent.ACTION_DOWN == event.getAction()){
            x1 = event.getX();
        }

        //获取抬起时的x轴坐标
        if(MotionEvent.ACTION_UP == event.getAction()) {
            x2 = event.getX();

            if (x2 - x1 > 50) {
                //右划,显示上一张
                index--;
                if (index < 0 )
                    index = list_head_pic.length - 1;
                Img.setImageResource(list_head_pic[index]);
            }
            if (x1 - x2 > 50) {
                index++;
                if (index == list_head_pic.length)
                    index = 0;
                Img.setImageResource(list_head_pic[index]);
            }
        }
        return super.onTouchEvent(event);
    }
}