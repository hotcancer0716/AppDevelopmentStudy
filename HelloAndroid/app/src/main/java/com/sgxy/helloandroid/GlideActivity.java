package com.sgxy.helloandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class GlideActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glide);

        EditText ET_PIC = findViewById(R.id.pic_edit);
        ImageView IMG_PIC = findViewById(R.id.pic_result);
        Button BTN_PIC = findViewById(R.id.pic_ok);

        BTN_PIC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pic_url = ET_PIC.getText().toString();

                //通过glide框架，从网络获取图片，并显示
                Glide.with(GlideActivity.this)
                        .load(pic_url)
                        .into(IMG_PIC);
            }
        });
    }
}