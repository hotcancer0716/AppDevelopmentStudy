package com.sgxy.helloandroid;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherData {

    /**
     * reason
     */
    @SerializedName("reason")
    private String reason;
    /**
     * result
     */
    @SerializedName("result")
    private ResultDTO result;
    /**
     * errorCode
     */
    @SerializedName("error_code")
    private Integer errorCode;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ResultDTO getResult() {
        return result;
    }

    public void setResult(ResultDTO result) {
        this.result = result;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public static class ResultDTO {
        /**
         * city
         */
        @SerializedName("city")
        private String city;
        /**
         * realtime
         */
        @SerializedName("realtime")
        private RealtimeDTO realtime;
        /**
         * future
         */
        @SerializedName("future")
        private List<FutureDTO> future;

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public RealtimeDTO getRealtime() {
            return realtime;
        }

        public void setRealtime(RealtimeDTO realtime) {
            this.realtime = realtime;
        }

        public List<FutureDTO> getFuture() {
            return future;
        }

        public void setFuture(List<FutureDTO> future) {
            this.future = future;
        }

        public static class RealtimeDTO {
            /**
             * temperature
             */
            @SerializedName("temperature")
            private String temperature;
            /**
             * humidity
             */
            @SerializedName("humidity")
            private String humidity;
            /**
             * info
             */
            @SerializedName("info")
            private String info;
            /**
             * wid
             */
            @SerializedName("wid")
            private String wid;
            /**
             * direct
             */
            @SerializedName("direct")
            private String direct;
            /**
             * power
             */
            @SerializedName("power")
            private String power;
            /**
             * aqi
             */
            @SerializedName("aqi")
            private String aqi;

            public String getTemperature() {
                return temperature;
            }

            public void setTemperature(String temperature) {
                this.temperature = temperature;
            }

            public String getHumidity() {
                return humidity;
            }

            public void setHumidity(String humidity) {
                this.humidity = humidity;
            }

            public String getInfo() {
                return info;
            }

            public void setInfo(String info) {
                this.info = info;
            }

            public String getWid() {
                return wid;
            }

            public void setWid(String wid) {
                this.wid = wid;
            }

            public String getDirect() {
                return direct;
            }

            public void setDirect(String direct) {
                this.direct = direct;
            }

            public String getPower() {
                return power;
            }

            public void setPower(String power) {
                this.power = power;
            }

            public String getAqi() {
                return aqi;
            }

            public void setAqi(String aqi) {
                this.aqi = aqi;
            }
        }

        public static class FutureDTO {
            /**
             * date
             */
            @SerializedName("date")
            private String date;
            /**
             * temperature
             */
            @SerializedName("temperature")
            private String temperature;
            /**
             * weather
             */
            @SerializedName("weather")
            private String weather;
            /**
             * wid
             */
            @SerializedName("wid")
            private WidDTO wid;
            /**
             * direct
             */
            @SerializedName("direct")
            private String direct;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public String getTemperature() {
                return temperature;
            }

            public void setTemperature(String temperature) {
                this.temperature = temperature;
            }

            public String getWeather() {
                return weather;
            }

            public void setWeather(String weather) {
                this.weather = weather;
            }

            public WidDTO getWid() {
                return wid;
            }

            public void setWid(WidDTO wid) {
                this.wid = wid;
            }

            public String getDirect() {
                return direct;
            }

            public void setDirect(String direct) {
                this.direct = direct;
            }

            public static class WidDTO {
                /**
                 * day
                 */
                @SerializedName("day")
                private String day;
                /**
                 * night
                 */
                @SerializedName("night")
                private String night;

                public String getDay() {
                    return day;
                }

                public void setDay(String day) {
                    this.day = day;
                }

                public String getNight() {
                    return night;
                }

                public void setNight(String night) {
                    this.night = night;
                }
            }
        }
    }
}
