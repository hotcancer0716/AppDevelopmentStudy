package com.sgxy.helloandroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NBAActivity extends AppCompatActivity {

    private  String[] name = {"神龟维斯布鲁克","戴维斯","库里30","加内特","莱昂纳德","约基奇",
            "kobe","泡椒","戴戴戴戴戴戴维斯","德鲁大叔","巴特勒","0号","24号","我不是投敌",
            "詹皇","德罗赞"};
    private  String[] team = {"雷霆","湖人","勇士","凯尔特人","快船","掘金","湖人","快船",
            "湖人","小牛","热火","雷霆","湖人","纽约尼克斯","骑士","猛龙"};
    private  int[] head = {R.drawable.nba01,R.drawable.nba02,R.drawable.nba03,R.drawable.nba04,
            R.drawable.nba05,R.drawable.nba06,R.drawable.nba07,R.drawable.nba08,
            R.drawable.nba09,R.drawable.nba010,R.drawable.nba011,R.drawable.nba012,
            R.drawable.nba013,R.drawable.nba014,R.drawable.nba015,R.drawable.nba016,};

    GridView Lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nba);

        //1. 在布局layout文件里，添加Listview
        //2. 设计item的布局界面
        //3. 组织数据,集合List
        List<Map<String,Object >> list = new ArrayList<Map<String,Object >>();
        //往list里面填充数据
        for (int i=0 ;i<name.length ; i++){
            HashMap< String , Object> map = new HashMap<String , Object>();
            map.put("key_head",head[i]);
            map.put("key_name",name[i]);
            map.put("key_team",team[i]);

            //把填充好的map，放入集合List
            list.add(map);
        }

        //4. 初始化控件。从layout到java
        Lv= findViewById(R.id.lv_nba_list);
        Lv.setNumColumns(2);

        //5. 配置，初始化Adapter
        SimpleAdapter simpleadapter = new SimpleAdapter(NBAActivity.this ,
                list,
                R.layout.nba_itemlayout,//item的布局
                new String[]{"key_head","key_name","key_team"},
                new int[]{R.id.img_item_head,R.id.tx_item_name,R.id.tx_item_detail}
        );
        //6. 绑定适配器,显示
        Lv.setAdapter(simpleadapter);

        //7. 监听点击事件
        Lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(NBAActivity.this, name[i], Toast.LENGTH_SHORT).show();
            }
        });
    }

    //1. 捕捉当前屏幕方向的配置信息Config
    //2. 给清单文件的Activity添加配置信息Config变化的权限 android:configChanges
    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        //判断是不是横屏
        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
            Lv.setNumColumns(2);

        //判断是不是竖屏
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
            Lv.setNumColumns(1);

    }
}