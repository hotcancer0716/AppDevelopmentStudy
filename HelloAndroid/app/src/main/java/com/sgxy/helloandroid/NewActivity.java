package com.sgxy.helloandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class NewActivity extends AppCompatActivity {
    String TAG = "========>>";
    //1.声明控件，创建控件对象
    AutoCompleteTextView AUTO_ET;
    Spinner SP;
    ListView LV;
    TextView TV;

    //3.组织数据
    String [] lesson={"Android开发","Android移动开发","Android移动应用开发"
            ,"Linux操作系统","Linux驱动开发","Linux应用程序设计","Linux",
            "C语言","C++编程","Java编程","Java程序设计","Java Web编程"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);

        //2.实例化
        AUTO_ET = findViewById(R.id.auto_text);
        SP = findViewById(R.id.sp);
        LV = findViewById(R.id.list);
        TV = findViewById(R.id.tx_list);

        //4.初始化适配器Adapter
        ArrayAdapter arrayAdapter  = new ArrayAdapter<>(NewActivity.this,
                                    android.R.layout.select_dialog_singlechoice,
                                    lesson);

        ArrayAdapter arrayAdapter_listview = new ArrayAdapter<>(NewActivity.this,
                android.R.layout.simple_list_item_1,lesson);

        //5.绑定适配器
        AUTO_ET.setAdapter(arrayAdapter);
        SP.setAdapter(arrayAdapter);
        LV.setAdapter(arrayAdapter_listview);


        //6.监听Spinner动作
        SP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int index, long l) {
                //有对Item的选中事件
                Toast.makeText(NewActivity.this, "你选中的课程是:  "+lesson[index], Toast.LENGTH_SHORT).show();
                Log.d("TAG", "你选中的课程是:  "+lesson[index]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        //对Listview添加监听
        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                //Toast.makeText(NewActivity.this, lesson[index], Toast.LENGTH_SHORT).show();
                TV.setText(lesson[index]);
            }
        });

        Log.d(TAG, "onCreate: ");

    }

}