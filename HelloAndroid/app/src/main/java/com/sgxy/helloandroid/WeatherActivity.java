package com.sgxy.helloandroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class WeatherActivity extends AppCompatActivity {

    // 1.开新的子线程Thread
    // 2.在子线程里面进行网络请求
    // 3.把服务器返回的结果，显示在界面TextView上

    String mykey = "60110cc0bb7993d48c4441f0b8059343";
    String WeatherURL = "http://apis.juhe.cn/simpleWeather/query";
    String MyURL = "";

    EditText ET_City;
    TextView TX_Result;
    Button BTN_OK;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        ET_City = findViewById(R.id.weather_edit);
        TX_Result = findViewById(R.id.weather_result);
        BTN_OK = findViewById(R.id.weather_ok);

        BTN_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //按钮按下，启动线程和网络连接
                String Mycity = ET_City.getText().toString();

                MyURL = WeatherURL+"?city="+Mycity+"&key="+mykey;

                //start启动线程（调用线程）
                Weather_Thread weatherThread = new Weather_Thread();
                weatherThread.start();
            }
        });
    }

    //Handler处理者  用来接受子线程发过来的消息
    Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case 999:
                    //Toast.makeText(WeatherActivity.this, "更新界面", Toast.LENGTH_SHORT).show();
                    //解析message的信息
                    Bundle bundle =  msg.getData();
                    String weather_Data = bundle.getString("weather_info","解析失败");

                    //利用Gson框架解析json数据
                    WeatherData weatherData = new Gson().fromJson(weather_Data,WeatherData.class);
                    String w_info = weatherData.getResult().getRealtime().getInfo().toString();
                    String w_temp = weatherData.getResult().getRealtime().getTemperature().toString();


                    TX_Result.setText("当前天气是"+w_info+"   温度"+w_temp+"度"); //在主线程的文本框里，显示解析的字符串数据
                    break;
            }

        }
    };


    //创建子线程 ，继承Thread类，添加run方法
    public class Weather_Thread extends Thread{
        @Override
        public void run() {
            Log.d("TAG", MyURL);

            //使用okhttp进行网络连接 ,注意！赋予应用程序网络权限
            Request request = new Request.Builder().url(MyURL).build();
            //接收返回
            Call call = new OkHttpClient().newCall(request);
            //接收队列
            call.enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    //有错误返回的情况
                    Log.d("TAG", "onFailure: ");
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    //正常运行时的结果，通过response传递过来
                    String MyResult =  response.body().string();
                    Log.d("TAG", MyResult);

                    //TX_Result.setText(MyResult); //不能在子线程操作控件
                    //通过handler，把子线程的消息发送到主线程去
                    //1.组织Message消息
                    Message msg = new Message();
                    Bundle bundle = new Bundle(); //Bundle数据包
                    bundle.putString("weather_info",MyResult);  //通过键值对把数据放入Bundle数据包
                    msg.setData(bundle); //把Bundle数据包放入Message
                    msg.what=999;
                    //发送Message消息
                    handler.sendMessage(msg);

                }
            });
        }
    }
}





