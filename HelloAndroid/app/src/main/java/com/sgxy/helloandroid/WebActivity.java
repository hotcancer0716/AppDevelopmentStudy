package com.sgxy.helloandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WebActivity extends AppCompatActivity {
    WebView WB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        EditText Web_Et = findViewById(R.id.web_edit);
        WB = findViewById(R.id.web_result);
        Button Web_btn = findViewById(R.id.web_ok);

        //1. 配置webview内核
        WB.setWebViewClient(new WebViewClient()); //设置当前的WebView为默认浏览器
        WB.getSettings().setJavaScriptEnabled(true); //使能js
        //打开html文件
        WB.loadUrl("file://android_asset/sgxy.html");


        Web_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String web_url = "http://"+ Web_Et.getText().toString();
                Toast.makeText(WebActivity.this, web_url, Toast.LENGTH_SHORT).show();
                //2. 打开网址
                WB.loadUrl(web_url);
            }
        });
    }


    //3. 捕获返回键
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (WB.canGoBack() && keyCode == KeyEvent.KEYCODE_BACK){
            WB.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}