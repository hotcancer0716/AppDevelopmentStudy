package com.sgxy.helloandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String TAG = "------------>";
    String name = "ikun";
    String pw = "ctrl001";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout); //指定layout布局文件

        Log.d(TAG, "1  onCreate: ");
        EditText ET_User  = findViewById(R.id.et_user);
        EditText ET_PW  = findViewById(R.id.et_password);
        Button BTN_Login = findViewById(R.id.btn_login);

        //添加按键的监听器
        BTN_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //按钮的事件
                String Str_name = ET_User.getText().toString();
                String Str_pw = ET_PW.getText().toString();

                if(Str_name.equals("") || Str_name.length()==0){
                    //吐司Toast
                    Toast.makeText(MainActivity.this, "请输入用户名", Toast.LENGTH_SHORT).show();
                } else if (Str_name.equals(name)) {
                    if (Str_pw.equals("") || Str_pw.length()==0){
                        Toast.makeText(MainActivity.this, "请输入密码", Toast.LENGTH_SHORT).show();
                    } else if (Str_pw.equals(pw)) {
                        Toast.makeText(MainActivity.this, "验证通过", Toast.LENGTH_SHORT).show();
                        //创建Intent ,并且初始化起点和终点
                        Intent intent = new Intent(MainActivity.this , SecondActivity.class);
                        //初始化Bundle
                        Bundle bundle = new Bundle();
                        bundle.putString("key_msg",Str_name);
                        intent.putExtras(bundle);
                        //执行跳转动作
                        startActivity(intent);

                    }else {
                        Toast.makeText(MainActivity.this, "密码不正确", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(MainActivity.this, "没有此用户", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    @Override
    protected void onStart() {
        Log.d(TAG, "1  onStart: ");
        super.onStart();
    }

    @Override
    protected void onRestart() {
        Log.d(TAG, "1  onRestart: ");
        super.onRestart();
    }


    @Override
    protected void onResume() {
        Log.d(TAG, "1  onResume: ");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "1  onPause: ");
        super.onPause();
    }


    @Override
    protected void onStop() {
        Log.d(TAG, "1  onStop: ");
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        Log.d(TAG, "1  onDestroy: ");
        super.onDestroy();
    }
}