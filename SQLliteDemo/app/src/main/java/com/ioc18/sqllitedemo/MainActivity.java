package com.ioc18.sqllitedemo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static android.content.SharedPreferences.*;

public class MainActivity extends AppCompatActivity {

    MySQLiteDB mySQLiteDB;
    SQLiteDatabase sqlDB;
    Cursor cur;
    int cur_position;
    SharedPreferences spf;

    EditText ET_Name,ET_Age;
    Button BTN_Insert;
    ListView LV;

    final  int AlertDialogID = 0x0001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ET_Age = findViewById(R.id.et_age);
        ET_Name = findViewById(R.id.et_name);
        BTN_Insert = findViewById(R.id.btn_insert);
        LV = findViewById(R.id.sqlite_listvie);

        mySQLiteDB = new MySQLiteDB(MainActivity.this,"mydb",null,1);
        sqlDB = mySQLiteDB.getWritableDatabase();
        spf = getSharedPreferences("myspf",MODE_PRIVATE);

        BTN_Insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Str_Name = ET_Name.getText().toString();
                String Str_Age = ET_Age.getText().toString();
                sqlDB.execSQL("insert into people (name,age) values (?,?)",new String[]{Str_Name,Str_Age});
                showSQL();
            }
        });

        LV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                cur_position = position;
                showDialog(AlertDialogID);
                return false;
            }
        });
        
        showSQL();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = spf.edit();
        editor.putString("spf_name",ET_Name.getText().toString());
        editor.putString("spf_age",ET_Age.getText().toString());
        editor.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        ET_Name.setText(spf.getString("spf_name",null));
        ET_Age.setText( spf.getString("spf_age",null));
    }

    private void showSQL() {
        cur = sqlDB.query("people",new String[]{"id","name","age"},"",null,null,null,null);

        ArrayList<HashMap<String,Object>> mylist = new ArrayList<HashMap<String, Object>>();

        while (cur.moveToNext()){
            HashMap<String,Object> hm = new HashMap<String, Object>();
            hm.put("K_id",cur.getInt(0));
            hm.put("K_name",cur.getString(1));
            hm.put("K_age",cur.getInt(2));
            mylist.add(hm);
        }

        SimpleAdapter sqladapter = new SimpleAdapter(
                MainActivity.this,
                mylist,
                R.layout.item_layout,
                new String[]{"K_id","K_name","K_age"},
                new int[]{R.id.item_num,R.id.item_name,R.id.item_age});

        LV.setAdapter(sqladapter);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id){
            case AlertDialogID:
                return bulidAlertDialog(MainActivity.this);
        }
        return super.onCreateDialog(id);
    }

    private AlertDialog bulidAlertDialog(final Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("注意!");
        builder.setMessage("是否删除联系人？");
        builder.setIcon(R.mipmap.ic_launcher_round);
        builder.setCancelable(false);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cur.moveToPosition(cur_position);
                String Str_where = cur.getString(1);
                sqlDB.delete("people","name = ?",new String[]{Str_where});
                Toast.makeText(context, "已删除", Toast.LENGTH_SHORT).show();
                showSQL();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(context, "已取消", Toast.LENGTH_SHORT).show();
            }
        });
        return builder.create();
    }
}
