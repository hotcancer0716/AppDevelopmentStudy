package com.sgxy_ict.view_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

public class ImgActivity extends AppCompatActivity {

    private int[] pic ={R.drawable.pic01,R.drawable.pic02,R.drawable.pic03,
            R.drawable.pic04,R.drawable.pic05,R.drawable.pic06};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img);

        Intent intent = getIntent();
        int pic_num = intent.getIntExtra("pic_num",0);

        ImageView Img_Head_big = findViewById(R.id.img_head_big);
        Img_Head_big.setImageResource(pic[pic_num]);
        Log.d("debug","测试一下");
    }
}