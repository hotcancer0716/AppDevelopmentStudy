package com.sgxy_ict.view_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class WebActivity extends AppCompatActivity {

    WebView WB;
    EditText Ed_WebUrl;
    Button Btn_Go;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        Ed_WebUrl = findViewById(R.id.edit_weburl);
        WB = findViewById(R.id.web_view);
        Btn_Go = findViewById(R.id.btn_webgo);

        WB.loadUrl("http://www.baidu.com");//默认网址
        WB.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl("http://www.baidu.com");
                return super.shouldOverrideUrlLoading(view, request);
            }
        });

        Btn_Go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WB.loadUrl("http://"+Ed_WebUrl.getText().toString());
            }
        });
    }
}