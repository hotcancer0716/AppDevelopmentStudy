package com.sgxy_ict.view_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ImageView Img_head;
    RadioButton Rad_Boy,Rad_Girl;
    CheckBox CBox1, CBox2, CBox3, CBox4, CBox5, CBox6;
    ProgressBar PB;
    SeekBar SB;
    private int[] pic ={R.drawable.pic01,R.drawable.pic02,R.drawable.pic03,
            R.drawable.pic04,R.drawable.pic05,R.drawable.pic06};
    private int pic_num = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button Btn_Pre = findViewById(R.id.btn_pre);
        Img_head = findViewById(R.id.img_head);
        Button Btn_Next = findViewById(R.id.btn_next);
        Button Btn_OK = findViewById(R.id.btn_ok);
        Button Btn_Web = findViewById(R.id.btn_web);
        Rad_Boy = findViewById(R.id.radio_boys);
        Rad_Girl = findViewById(R.id.radio_girls);
        CBox1 = findViewById(R.id.checkbox1);
        CBox2 = findViewById(R.id.checkbox2);
        CBox3 = findViewById(R.id.checkbox3);
        CBox4 = findViewById(R.id.checkbox4);
        CBox5 = findViewById(R.id.checkbox5);
        CBox6 = findViewById(R.id.checkbox6);
        PB = findViewById(R.id.progress_h);
        SB = findViewById(R.id.seekbar);


        Img_head.setImageResource(R.drawable.pic01);
        Btn_Pre.setOnClickListener(new Btn_Click());
        Btn_Next.setOnClickListener(new Btn_Click());
        Btn_OK.setOnClickListener(new Btn_Click());
        Btn_Web.setOnClickListener(new Btn_Click());

        SB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                PB.setProgress(i);
                if (i == 100)
                    PB.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                PB.setVisibility(View.VISIBLE);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Img_head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Activity跳转
                Intent intent = new Intent();
                intent.putExtra("pic_num",pic_num);
                intent.setClass(MainActivity.this , ImgActivity.class);
                startActivity(intent);
            }
        });
    }

    public class Btn_Click implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            switch (view.getId()){
                case R.id.btn_web:
                    Intent intent = new Intent();
                    intent.setClass(MainActivity.this , WebActivity.class);
                    startActivity(intent);
                    break;
                case R.id.btn_pre:
                    pic_num--;
                    if (pic_num < 0)
                        pic_num = pic.length - 1;
                    Img_head.setImageResource(pic[pic_num]);
                    break;
                case R.id.btn_next:
                    pic_num++;
                    if(pic_num > pic.length - 1) {
                        pic_num = 0;
                    }
                    Img_head.setImageResource(pic[pic_num]);
                    break;
                case R.id.btn_ok:
                    String Str_result = null;
                    if (Rad_Boy.isChecked())
                        Str_result = Rad_Boy.getText().toString();
                    if (Rad_Girl.isChecked())
                        Str_result = Rad_Girl.getText().toString();
                    if (CBox1.isChecked())
                        Str_result = Str_result +"\n"+ CBox1.getText().toString();
                    if (CBox2.isChecked())
                        Str_result = Str_result +"\n"+ CBox2.getText().toString();
                    if (CBox3.isChecked())
                        Str_result = Str_result +"\n"+ CBox3.getText().toString();
                    if (CBox4.isChecked())
                        Str_result = Str_result +"\n"+ CBox4.getText().toString();
                    if (CBox5.isChecked())
                        Str_result = Str_result +"\n"+ CBox5.getText().toString();
                    if (CBox6.isChecked())
                        Str_result = Str_result +"\n"+ CBox6.getText().toString();

                    Toast.makeText(MainActivity.this, Str_result, Toast.LENGTH_SHORT).show();

                    break;

                default:
                    throw new IllegalStateException("Unexpected value: " + view.getId());
            }
        }
    }
}