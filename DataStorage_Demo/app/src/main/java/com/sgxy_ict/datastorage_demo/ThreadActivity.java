package com.sgxy_ict.datastorage_demo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;

import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.HashMap;

public class ThreadActivity extends AppCompatActivity {


    EditText ET_Name,ET_Age,ET_Number,ET_Class;
    MaterialButton BTN_Save;
    ListView LV;
    StudentSQL studentSQL;  //数据库管理工具SQLiteOpenHelper
    SQLiteDatabase StuDB;   //数据库
    Cursor cursor;  //游标
    ContentValues values;
    ProgressDialog insertprogressDialog;
    int insert_max = 10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration cfg = getResources().getConfiguration();
        if (cfg.orientation == Configuration.ORIENTATION_PORTRAIT)
            setContentView(R.layout.activity_sqlite);
        else
            setContentView(R.layout.activity_sqlite_h);

        ET_Name = findViewById(R.id.et_name);
        ET_Age = findViewById(R.id.et_age);
        ET_Number = findViewById(R.id.et_id);
        ET_Class = findViewById(R.id.et_class);
        BTN_Save = findViewById(R.id.btn_save);
        LV = findViewById(R.id.lv_list);
        //1. 创建数据库文件.创建表。借助数据库管理工具SQLiteOpenHelper实现该功能
        studentSQL = new StudentSQL(ThreadActivity.this,"sqlstudy.db",null,1);
        //2. 打开数据库，打开表。
        StuDB = studentSQL.getWritableDatabase();
        //3.数据库数据显示
        show_SQL();

        //保存按键的监听
        BTN_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String myname = ET_Name.getText().toString();
                String mynumber = ET_Number.getText().toString();
                String myage = ET_Age.getText().toString();
                String myclass = ET_Class.getText().toString();

                //插入数据 第①种方法。调用execSQL
   //             StuDB.execSQL("insert into student (name,age,number,class_name ) values (?,?,?,?)",new String[]{myname,myage,mynumber,myclass});
                //插入数据 第②种方法。调用insert
                values = new ContentValues();
                values.put("name",myname);
                values.put("age",myage);
                values.put("number",mynumber);
                values.put("class_name",myclass);

                insertprogressDialog = new ProgressDialog(ThreadActivity.this);
                insertprogressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                insertprogressDialog.setTitle("数据库信息处理中...");
                insertprogressDialog.setMax(insert_max);
                insertprogressDialog.setProgress(0);
                insertprogressDialog.setMessage("模拟插入10000条数据");
                insertprogressDialog.create();
                insertprogressDialog.show();

                //insert
                Insert_Thread insert_thread = new Insert_Thread();
                insert_thread.start();
            }
        });

        //长按删除功能的按键监听
        LV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ThreadActivity.this) ;
                builder.setTitle("确定删除？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        cursor.moveToPosition(position);
                        StuDB.delete("student","id = ?",new String[]{cursor.getString(0)});
                        //显示数据
                        show_SQL();
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return false;
            }
        });
    }

    public class Insert_Thread extends Thread{
        @Override
        public void run() {
            //
            for(int i=0; i<insert_max ;i++){
                StuDB.insert("student",null,values);
                Message pg_msg = new Message();
                Bundle bundle = new Bundle();
                bundle.putInt("pb_value",i);
                pg_msg.setData(bundle);
                pg_msg.what = 0;
                myhandler.sendMessage(pg_msg);
            }

            Message msg = new Message();
            msg.what = 1;
            myhandler.sendMessage(msg);


            super.run();
        }
    }

    public Handler myhandler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case 1:
                    show_SQL();
                    insertprogressDialog.dismiss();
                    break;

                case 0:
                    insertprogressDialog.setProgress(msg.getData().getInt("pb_value"));
                    break;

            }

            super.handleMessage(msg);
        }
    };

    private void show_SQL( ){
        cursor = StuDB.query("student",new String[]{"id","name","age","number","class_name"},null,null,null,null,null);
        //创建ArrayList
        ArrayList<HashMap<String,Object>> stu_list = new ArrayList<HashMap<String,Object>>();
        while (cursor.moveToNext()){
            HashMap<String,Object> hm = new HashMap<String,Object>();
            hm.put("k_id",cursor.getString(0));
            hm.put("k_name",cursor.getString(1));
            hm.put("k_age",cursor.getInt(2));
            hm.put("k_number",cursor.getInt(3));
            hm.put("k_class",cursor.getString(4));
            stu_list.add(hm);
        }
        //创建AdApter
        SimpleAdapter simpleAdapter = new SimpleAdapter(
                ThreadActivity.this,
                stu_list,
                R.layout.item_studentlayout,
                new String[]{"k_id","k_name","k_age","k_number","k_class"},
                new int[]{R.id.item_num,R.id.item_name,R.id.item_age,R.id.item_number,R.id.item_class});

        //ListView绑定
        LV.setAdapter(simpleAdapter);
    }
}