package com.sgxy_ict.datastorage_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class SPFActivity extends AppCompatActivity {

    Button BTN_OK;
    EditText ET_Name,ET_passwd;
    CheckBox CB_SPF;
    SharedPreferences myspf;

    //在Activity出现暂停时调用，保存数据
    @Override
    protected void onPause() {
        //1.获取SPF
        myspf = PreferenceManager.getDefaultSharedPreferences(SPFActivity.this);
        //2.获取Editor
        SharedPreferences.Editor editor = myspf.edit();
        //3.写入
        editor.putString("K_name",ET_Name.getText().toString());
        editor.putString("K_passwd",ET_passwd.getText().toString());
        //4.提交
        editor.commit();
        super.onPause();
    }
    //在Activity恢复时调用，从SPF取出数据
    @Override
    protected void onStart() {
        myspf = PreferenceManager.getDefaultSharedPreferences(SPFActivity.this);
        ET_Name.setText(myspf.getString("K_name",""));
        ET_passwd.setText(myspf.getString("K_passwd",""));
        super.onStart();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spfactivity);

        BTN_OK = findViewById(R.id.spf_button);
        ET_Name = findViewById(R.id.spf_user);
        ET_passwd = findViewById(R.id.spf_passwd);
        CB_SPF = findViewById(R.id.checkbox_passwd);

        BTN_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //点击登录按钮，同时保存数据
                //1.获取SPF
                myspf =  PreferenceManager.getDefaultSharedPreferences(SPFActivity.this);
                //2.获取Editor
                SharedPreferences.Editor editor = myspf.edit();
                //3.写入
                editor.putString("K_name",ET_Name.getText().toString());
                editor.putString("K_passwd",ET_passwd.getText().toString());
                //4.提交
                editor.commit();
            }
        });


    }
}