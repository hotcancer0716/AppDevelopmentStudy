package com.sgxy_ict.datastorage_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class FileActivity extends AppCompatActivity {
    EditText ET_Input;
    Button BTN_Write,BTN_Read;
    TextView TX_Output;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sdfile);

        ET_Input = findViewById(R.id.sd_input);
        BTN_Write = findViewById(R.id.sd_save);
        BTN_Read = findViewById(R.id.sd_read);
        TX_Output = findViewById(R.id.sd_output);

        BTN_Write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File file = new File(getFilesDir(),"data.txt");
                try {
                    //内部存储,写入
                    if (!file.exists()){
                        file.createNewFile();
                    }

                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(ET_Input.getText().toString().getBytes());
                    fos.close();
                }
                catch (Exception e){
                    e.printStackTrace();
                }

            }
        });


        BTN_Read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File file = new File(getFilesDir(),"data.txt");
                try {
                    //内部存储,读取
                    FileInputStream fis = new FileInputStream(file);
                    byte[] mybyte = new byte[1024];
                    int n = fis.read(mybyte);
                    String myStr = new String(mybyte,0,n);
                    TX_Output.setText(myStr);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

}