package com.sgxy_ict.datastorage_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class SDFileActivity extends AppCompatActivity {

    EditText ET_Input;
    Button BTN_Write,BTN_Read;
    TextView TX_Output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sdfile);

        ET_Input = findViewById(R.id.sd_input);
        BTN_Write = findViewById(R.id.sd_save);
        BTN_Read = findViewById(R.id.sd_read);
        TX_Output = findViewById(R.id.sd_output);

        BTN_Write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //1. 判断外部存储，mount
                //【注意权限】
                if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
                    //2.在指定外部存储路径下创建文件
                    File path = Environment.getExternalStorageDirectory();
                    File myfile = new File(path , "data.txt");
                    String mydata = ET_Input.getText().toString();
                    //3.利用 FileOutputStream写入文件
                    FileOutputStream fos;
                    try {
                        fos = new FileOutputStream(myfile);
                        OutputStreamWriter osw = new OutputStreamWriter(fos);
                        osw.write(mydata);
                        osw.flush();
                        osw.close();
                        fos.close();
                        Toast.makeText(SDFileActivity.this, "Write success", Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e ){
                        Toast.makeText(SDFileActivity.this, "Write failed", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
            }
        });

        BTN_Read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //1.判断mount
                if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
                    //2.根据路径和文件名，找到文件
                    File mypath = Environment.getExternalStorageDirectory();
                    File myfile = new File(mypath , "data.txt");
                    //3.利用输入流取数据
                    FileInputStream fis;
                    try {
                        fis = new FileInputStream(myfile);
                        InputStreamReader isr = new InputStreamReader(fis,"utf-8");
                        char[] input = new char[fis.available()];
                        isr.read(input);
                        isr.close();
                        //4.显示
                        TX_Output.setText(new String(input));

                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }

            }
        });
    }
}