package com.sgxy.a4412cvt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import casio.serial.SerialPort;

public class MainActivity extends AppCompatActivity {

    final String  TTY_DEV = "/dev/ttySAC1";
    final int 	  bps = 115200;
    SerialPort mSerialPort = null;		        //串口设备描述
    protected OutputStream mOutputStream;		//串口输出描述
    private InputStream mInputStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            mSerialPort = new SerialPort(new File(TTY_DEV), bps , 0);
            mOutputStream = mSerialPort.getOutputStream();
            mInputStream = mSerialPort.getInputStream();
        }catch (IOException e){
            e.printStackTrace();
        }


        while (true){
            byte[] buf = new byte[1]; 			//组织接收
            int size = 0;
            int buf_num = 13;

            try {
                size = mInputStream.read(buf);
                byte[] receive_buf = new byte[20]; 			//组织接收
                receive_buf[buf_num] = (byte) (0xff&buf[0]);

                Log.d("TAG", "测试  ："+byte2HexStr(receive_buf));

            /*
            //循环读取每一个字节，当收到0x02时，开始接收后续的13个字节
            if( buf[0] == 0x02){
                byte[] receive_buf = new byte[20]; 			//组织接收
                //读取13个字节
            }
*/
            }catch (IOException e){
                e.printStackTrace();
            }




        }
    }

    /**
     * bytes转换成十六进制字符串
     * @return String 每个Byte值之间空格分隔
     */
    public static String byte2HexStr(byte[] b)
    {
        String stmp="";
        StringBuilder sb = new StringBuilder("");
        for (int n=0;n<b.length;n++)
        {
            stmp = Integer.toHexString(b[n] & 0xFF);
            sb.append((stmp.length()==1)? "0"+stmp : stmp);
            sb.append(" ");
        }
        return sb.toString().toUpperCase().trim();
    }

}