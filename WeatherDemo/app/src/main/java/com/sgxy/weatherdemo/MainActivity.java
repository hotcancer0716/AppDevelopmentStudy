package com.sgxy.weatherdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    String TAG = "Test:    ";
    TextView Tx_Temp,Tx_Info,Tx_Power,Tx_Direct;

    ImageView Img_Icon,Img_bg;
    Spinner SP_City_Name;
    String Str_weather;
    String city = "韶关";
    String[] City_Array={"韶关","拉萨","长春","乌鲁木齐","广州","海口","阳江","南京","北海","昆明"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SP_City_Name = findViewById(R.id.sp_city_name);
        Tx_Temp = findViewById(R.id.tx_temp);
        Tx_Info = findViewById(R.id.tx_info);
        Tx_Power = findViewById(R.id.tx_power);
        Tx_Direct = findViewById(R.id.tx_direct);
        Img_Icon = findViewById(R.id.img_info);
        Img_bg = findViewById(R.id.background);


        Glide.with(MainActivity.this)
                .load("https://pics5.baidu.com/feed/80cb39dbb6fd5266ca5b258a3ad5922cd507360b.jpeg@f_auto?token=33f641284a475b793299dbdb9c975e54")
                .into(Img_bg);

        ArrayAdapter arrayAdapter = new ArrayAdapter(
                this,
                android.R.layout.simple_dropdown_item_1line,
                City_Array);

        SP_City_Name.setAdapter(arrayAdapter);
        SP_City_Name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "你选中的城市是: "+ City_Array[i] );
                city = City_Array[i];

                Thread_Net thread_net = new Thread_Net();
                thread_net.start();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public  Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case 1186:
                    //更新界面
                    String Str_temp = msg.getData().getString("k_temp",null);
                    Tx_Temp.setText(Str_temp+"°");
                    String Str_info = msg.getData().getString("k_info",null);
                    Tx_Info.setText(Str_info);
                    String Str_pic = "https://upload-images.jianshu.io/upload_images/5433966-763b100c458a81e3.jpg";  //背景图片的网络地址
                    switch (Str_info){
                        case "晴":
                            Str_pic = "https://upload-images.jianshu.io/upload_images/5433966-763b100c458a81e3.jpg";  //晴天
                            Img_Icon.setImageResource(R.mipmap.ic_widget_sunny_shadow);
                            break;
                        case "小雨":
                            Str_pic ="https://c-ssl.dtstatic.com/uploads/item/201802/18/20180218012933_zYPcT.thumb.1000_0.jpeg";
                            Img_Icon.setImageResource(R.mipmap.ic_widget_lightrain_shadow);
                            break;
                        case "中雨":
                            Img_Icon.setImageResource(R.mipmap.ic_widget_rain_shadow);
                            Str_pic ="https://img.pconline.com.cn/images/photoblog/7/3/8/8/7388376/20089/23/1222159043114_mthumb.jpg";
                            break;
                        case "大暴雨":
                            Img_Icon.setImageResource(R.mipmap.ic_widget_heavyrain_shadow);
                            Str_pic ="https://img.pconline.com.cn/images/photoblog/7/3/8/8/7388376/20089/23/1222159043526_mthumb.jpg";
                            break;
                        case "多云":
                            Str_pic ="https://up.sc.enterdesk.com/edpic/d7/05/87/d70587786179a0119f3b953a51300cd4.jpg";
                            Img_Icon.setImageResource(R.mipmap.ic_widget_cloudy_shadow);
                            break;
                        default:
                            Img_Icon.setImageResource(R.mipmap.ic_widget_wind_shadow);
                            break;
                    }

                    Glide.with(MainActivity.this)
                            .load(Str_pic)
                            .into(Img_bg);

                    Tx_Power.setText(msg.getData().getString("k_power",null));
                    Tx_Direct.setText(msg.getData().getString("k_direct",null));
                    break;
            }
        }
    };

    public  class  Thread_Net extends Thread{
        @Override
        public void run() {
            String Url="http://apis.juhe.cn/simpleWeather/query";
            String key="60110cc0bb7993d48c4441f0b8059343";

            String WeatherURL = Url +"?"+"city="+city+"&"+"key="+key;
            Log.d(TAG, WeatherURL);

            Request request = new Request.Builder().url(WeatherURL).build();
            Call call = new OkHttpClient().newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    Log.d(TAG, "onFailure: ");
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    String Str_json = response.body().string();
                    Log.d(TAG, Str_json);
                    //解析json语句
                    Weather_info weather_info =
                            new Gson().fromJson(Str_json,Weather_info.class);
                    String temp = weather_info.getResult().getRealtime().getTemperature().toString();
                    Log.d(TAG, temp);
                    String info = weather_info.getResult().getRealtime().getInfo();
                    Log.d(TAG, info);

                    Str_weather = city+"的天气是："+info+"   温度是："+temp;
                    Log.d(TAG,Str_weather);

                    //发送消息到主线程
                    Message msg = new Message();
                    msg.what = 1186;
                    Bundle bundle = new Bundle();
                    bundle.putString("k_temp",temp);
                    bundle.putString("k_info",info);
                    bundle.putString("k_power",weather_info.getResult().getRealtime().getPower());
                    bundle.putString("k_direct",weather_info.getResult().getRealtime().getDirect());

                    msg.setData(bundle);
                    handler.sendMessage(msg);
                }
            });

        }
    }
}