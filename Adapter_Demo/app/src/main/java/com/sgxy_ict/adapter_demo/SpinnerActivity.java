package com.sgxy_ict.adapter_demo;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SpinnerActivity extends AppCompatActivity {

    String[] All_College ={"信息工程学院","物理工程学院","生物工程学院","食品工程学院","化学工程学院","土木工程学院","音乐学院",
            "体育学院","车辆工程学院","教育学院","美术学院","医学院"};
    String[] num ={"138342225","134675758","13834226457","138342342","1354642225","137665225",};
    String[] Lessons ={"138342225","134675758","13834226457","138342342","1354642225","137665225",};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        Spinner spin_College = findViewById(R.id.spin_college);
        AutoCompleteTextView AUTO_Text = findViewById(R.id.auto_text);
        ListView listView = findViewById(R.id.list_num);

        ArrayAdapterPlus<String> My_All_College = new ArrayAdapterPlus<String>(
                SpinnerActivity.this,
                android.R.layout.simple_dropdown_item_1line,
                All_College
        );
        AUTO_Text.setAdapter(My_All_College);

        ArrayAdapter<String> adapter_lesson = new ArrayAdapter<String>(
                SpinnerActivity.this,
                android.R.layout.simple_dropdown_item_1line,
                Lessons
        );

        listView.setAdapter(adapter_lesson);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(SpinnerActivity.this, "你点击的是："+Lessons[position], Toast.LENGTH_SHORT).show();
            }
        });



        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_dropdown_item_1line ,
                All_College);

        spin_College.setAdapter(adapter);

        spin_College.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(SpinnerActivity.this, "你选中的是："+All_College[position], Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
}