package com.sgxy_ict.adapter_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button BTN_1, BTN_2, BTN_3, BTN_4, BTN_5, BTN_6;
    Intent intent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BTN_1 = findViewById(R.id.btn_1);
        BTN_2 = findViewById(R.id.btn_2);
        BTN_3 = findViewById(R.id.btn_3);
        BTN_4 = findViewById(R.id.btn_4);
        BTN_5 = findViewById(R.id.btn_5);
        BTN_6 = findViewById(R.id.btn_6);

        BtnChick btnChick = new BtnChick();

        BTN_1.setOnClickListener(btnChick);
        BTN_2.setOnClickListener(btnChick);
        BTN_3.setOnClickListener(btnChick);
        BTN_4.setOnClickListener(btnChick);
        BTN_5.setOnClickListener(btnChick);
        BTN_6.setOnClickListener(btnChick);
    }

    public class BtnChick implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            switch (view.getId()){
                case R.id.btn_1:
                    intent.setClass(MainActivity.this,SpinnerActivity.class);
                    startActivity(intent);
                    break;
                case R.id.btn_2:
                    intent.setClass(MainActivity.this,ListviewActivity.class);
                    startActivity(intent);
                    break;
                case R.id.btn_3:
                    intent.setClass(MainActivity.this,GridViewActivity.class);
                    startActivity(intent);
                    break;
                case R.id.btn_4:
                    intent.setClass(MainActivity.this,ListviewMenuActivity.class);
                    startActivity(intent);
                    break;
                case R.id.btn_5:
                    intent.setClass(MainActivity.this,MenuActivity.class);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        }
    }
}