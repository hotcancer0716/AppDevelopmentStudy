package com.sgxy_ict.adapter_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListviewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);

        //2. 创建ListView对象实例，并绑定布局id
        ListView LV_Book = findViewById(R.id.listview_book);

        //3. 数据的创建
        String[] Lesson = {"物联网移动应用开发","移动应用开发","物联网系统应用开发",
                "物联网移动应用设计","物联网网络应用开发","传感器应用开发",
                "物联网感知层开发","物联网云数据开发","物联网综合应用开发",};
        int[] value ={45,39,45,76,34,77,12,43,56};
        int[] pic = {R.drawable.book_1,R.drawable.book_2,R.drawable.book_3,
                R.drawable.book_4,R.drawable.book_5,R.drawable.book_6,
                R.drawable.book_7,R.drawable.book_8,R.drawable.book_9,};
        //创建List数据集合
        List<Map<String, Object>> book = new ArrayList<Map<String,Object>>();
        for (int i=0 ; i < Lesson.length ; i++ ){
           Map<String , Object> itemMap = new HashMap<>();
            itemMap.put("K_Lesson",Lesson[i]);
            itemMap.put("K_value",value[i]);
            itemMap.put("K_pic",pic[i]);
            book.add(itemMap);
        }

        //4. 创建item的布局

        //5. simpleAdapter创建
        SimpleAdapter simpleAdapter = new SimpleAdapter(
                ListviewActivity.this ,
                book,
                R.layout.itemlayout,
                new String[]{"K_Lesson","K_value","K_pic"},
                new int[]{R.id.item_lesson,R.id.item_value,R.id.item_pic}
                );

        //6. SetAdapter
        LV_Book.setAdapter(simpleAdapter);

        //7.按键监听（监听Item）
        LV_Book.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(ListviewActivity.this, "你点击的是：" + Lesson[position], Toast.LENGTH_SHORT).show();
            }
        });
    }
}