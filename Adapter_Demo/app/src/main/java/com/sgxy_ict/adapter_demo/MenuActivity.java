package com.sgxy_ict.adapter_demo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MenuActivity extends AppCompatActivity {

    String data;
    EditText editText_1,editText_2;
    Button BTN_PopupMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        //1. 创建菜单布局menu.xml 在目录/res/menu下

        editText_1 = findViewById(R.id.edit_1);
        editText_2 = findViewById(R.id.edit_2);
        BTN_PopupMenu = findViewById(R.id.btn_popupMenu);

        //把控件和ContextMenu关联
        registerForContextMenu(editText_1);
        registerForContextMenu(editText_2);

        BTN_PopupMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(MenuActivity.this,view);
                MenuInflater menuInflater = popupMenu.getMenuInflater();
                menuInflater.inflate(R.menu.edit_menu,popupMenu.getMenu());
                popupMenu.show();
            }
        });
    }

    //2. 菜单创建    利用onCreateOptionsMenu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //3.创建 MenuInflater对象，用于从/res/menu获取显示布局
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.my_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    //4.菜单监听  onOptionsItemSelected
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Toast.makeText(MenuActivity.this, "你点击的是"+item.getTitle().toString(), Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.edit_menu,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_copy:
                data = editText_1.getText().toString();
                break;

            case R.id.menu_prase:
                editText_2.setText(data);
                break;
        }

        return super.onContextItemSelected(item);
    }
}