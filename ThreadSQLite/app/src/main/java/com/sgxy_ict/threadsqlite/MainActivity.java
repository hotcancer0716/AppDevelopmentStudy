package com.sgxy_ict.threadsqlite;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    EditText ET_Name,ET_Age,ET_Number,ET_Class;
    MaterialButton BTN_Save;
    ListView LV;
    ProgressBar progressBar;
    final  int AlertDialogID = 0x0001;
    final  int handler_progresss = 0x11;
    final  int handler_showSQL = 0x12;

    MySQLiteDB mySQLiteDB;
    SQLiteDatabase sqlDB;
    Cursor cur;
    int cur_position;
    String Str_Name,Str_Age,Str_Number,Str_Class;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        find_id();
        Key_things();
        mySQLiteDB = new MySQLiteDB(MainActivity.this,"mydb",null,1);
        sqlDB = mySQLiteDB.getWritableDatabase();

        showSQL();
    }

    private void Key_things() {
        LV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                //长按进行删除操作
                cur_position = position;
                showDialog(AlertDialogID);
                return false;
            }
        });

        BTN_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Str_Name = ET_Name.getText().toString();
                Str_Age = ET_Age.getText().toString();
                Str_Number = ET_Number.getText().toString();
                Str_Class = ET_Class.getText().toString();

                sqlDB.execSQL("insert into people (name,age,number,class_name) values (?,?,?,?)",
                        new String[]{Str_Name,Str_Age,Str_Number,Str_Class});
                Toast.makeText(MainActivity.this, "保存成功", Toast.LENGTH_SHORT).show();
                showSQL();
            }
        });
    }

    /*
    * Android线程开发步骤
    * 1.实现Thread，并重写run方法
    * 2.创建线程实例，调用start()方法运行
    * 3.为了实现线程间的数据传递，需要实现内部类Handler,并进行UI线程（主线程）
    * 的handleMessage消息接收
    * 4.在辅助线程里创建消息实例(Message)，利用Bundle组织数据。
    * 5.调用handler.sentMessage()将辅助线程的消息(Message)传递给主线程
    */

    private void find_id() {
        ET_Name = findViewById(R.id.et_name);
        ET_Age = findViewById(R.id.et_age);
        ET_Number = findViewById(R.id.et_id);
        ET_Class = findViewById(R.id.et_class);
        BTN_Save = findViewById(R.id.btn_save);
        LV = findViewById(R.id.lv_list);
        progressBar = findViewById(R.id.progress);
    }

    //创建选项菜单
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //选项菜单
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //刷新并显示数据库信息到Listview
    private void showSQL() {
        cur = sqlDB.query("people",new String[]{"id","name","age","number","class_name"},"",null,null,null,null);

        ArrayList<HashMap<String,Object>> mylist = new ArrayList<HashMap<String, Object>>();

        while (cur.moveToNext()){
            HashMap<String,Object> hm = new HashMap<String, Object>();
            hm.put("K_id",cur.getInt(0));
            hm.put("K_name",cur.getString(1));
            hm.put("K_age",cur.getInt(2));
            hm.put("K_number",cur.getInt(3));
            hm.put("K_class",cur.getString(4));
            mylist.add(hm);
        }

        Collections.reverse(mylist);

        SimpleAdapter sqladapter = new SimpleAdapter(
                MainActivity.this,
                mylist,
                R.layout.item_layout,
                new String[]{"K_id","K_name","K_age","K_number","K_class"},
                new int[]{R.id.item_num,R.id.item_name,R.id.item_age,R.id.item_number,R.id.item_class});

        LV.setAdapter(sqladapter);
    }


    //创建对话框
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id){
            case AlertDialogID:
                return bulidAlertDialog(MainActivity.this);
        }
        return super.onCreateDialog(id);
    }

    private AlertDialog bulidAlertDialog(final Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("注意!");
        builder.setMessage("是否删除联系人？");
        builder.setIcon(R.mipmap.ic_launcher_round);
        builder.setCancelable(false);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cur.moveToPosition(cur_position);
                String Str_where = cur.getString(0);
                sqlDB.delete("people","id = ?",new String[]{Str_where});
                Toast.makeText(context, "已删除", Toast.LENGTH_SHORT).show();
                showSQL();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(context, "已取消", Toast.LENGTH_SHORT).show();
            }
        });
        return builder.create();
    }
}
