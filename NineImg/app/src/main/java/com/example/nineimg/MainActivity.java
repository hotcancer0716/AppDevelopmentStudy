package com.example.nineimg;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ImageView IMG_1,IMG_2,IMG_3,IMG_4,IMG_5,IMG_6,IMG_7,IMG_8,IMG_9;
    TextView Text_Step;
    int status=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Text_Step = findViewById(R.id.text_num);
        IMG_1 = findViewById(R.id.pic_1);
        IMG_2 = findViewById(R.id.pic_2);
        IMG_3 = findViewById(R.id.pic_3);
        IMG_4 = findViewById(R.id.pic_4);
        IMG_5 = findViewById(R.id.pic_5);
        IMG_6 = findViewById(R.id.pic_6);
        IMG_7 = findViewById(R.id.pic_7);
        IMG_8 = findViewById(R.id.pic_8);
        IMG_9 = findViewById(R.id.pic_9);

        IMG_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Click_Img(IMG_1);
            }
        });

        IMG_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Click_Img(IMG_2);
            }
        });

    }

    public Click_Img(ImageView view){
        //status为0的时候，代表这时还只选中了第1张图片
        if (status == 0)
        {
            //记录按下的第一个图
            status = 1;
        }else{
            //status为1的时候，代表这时已经点了第2张图片，
            //需要判断，第1张和第2张的位置关系，只有相邻的才可以换照片。否则弹出提示要求重新操作。
            //如果判断没问题，就更换两张图片，并且计步器+1


            status = 0;
        }
    }
}