package com.sgxy.smarthome;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cursoradapter.widget.SimpleCursorAdapter;

import android.content.DialogInterface;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    //准备数据
    int[] pic = {R.drawable.c7k,R.drawable.c7m,R.drawable.c7o,R.drawable.c8b,
            R.drawable.c8d,R.drawable.c8f,R.drawable.c8i,R.drawable.c7k,
            R.drawable.c8k,R.drawable.c8m};

    String[] DevName = {"机顶盒","智能风扇","IPTV","智能开关","电视盒子","Ipad","遥控器","投影仪","风扇","智能开关","电视盒子","冰箱"};
    //1.初始化Listview列表控件
    GridView LV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("TAG", "图片id: "+R.drawable.c7k);

        //1.初始化Listview列表控件

        LV = findViewById(R.id.lv);

        MySQLIteDB mySQLIteDB = new MySQLIteDB(MainActivity.this,"IOT_DB",null,1);
        SQLiteDatabase mySQLite = mySQLIteDB.getWritableDatabase();

        Cursor cursor = mySQLite.query("iot_table",
                new String[]{"_id","pic","title","message"},
                null,null,null,null,null,null);
     //   cursor.moveToPosition(0);
     //   Log.d("TAG", "查询出的结果是: "+cursor.getInt(0));
     //   Log.d("TAG", "查询出的结果是: "+cursor.getString(1));
     //   Log.d("TAG", "查询出的结果是: "+cursor.getString(2));

        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(
                MainActivity.this,
                R.layout.item_layout,
                cursor,
                new String[]{"pic","title","message","_id"},
                new int[]{R.id.img_main,R.id.tx_main,R.id.tx_des,R.id.tx_num},
                0);

        /*
        //2. 实现List
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        for(int i=0;i<pic.length;i++){
            //初始化一个Map类型的数据
            HashMap<String,Object> map = new HashMap<String,Object>();
            map.put("k_pic",pic[i]);
            map.put("k_DevN",DevName[i]);
            list.add(map);
        }

        //3.初始化Adapter适配器

        SimpleAdapter adapter = new SimpleAdapter(
                MainActivity.this, //上下文，也就是当前Activity
                list,  //数据集合List
                R.layout.item_layout, //Item的布局
                new String[]{"k_pic","k_DevN"},  //键的列表
                new int[]{R.id.img_main,R.id.tx_main} //item布局下的控件id，这个id要跟key对应
                );
*/
        //4.Adapter适配器和Listview控件绑定

        LV.setAdapter(cursorAdapter);

        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               // Toast.makeText(MainActivity.this, DevName[i], Toast.LENGTH_SHORT).show();
            }
        });

        LV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                //长按，出发对话框
                //使用构造器builder方式创建对话框
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setIcon(R.drawable.c8d).
                        setCancelable(false).
                        setTitle("警告！！").setNeutralButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).setNegativeButton("Detail", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).
                        setMessage("你选中的是:"+DevName[i]);
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return false;
            }
        });


    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            LV.setNumColumns(1);
        }
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            LV.setNumColumns(2);
        }
    }
}


// SQLite 轻量化/嵌入式
// 增删改查
// 创建SQLite， SQLiteOpenHelper
// 调试方法      使用sdk adb进入安卓设备的Linux系统
//             进入的目录/data/data/xxxx/databases
//             连接SQLite数据库的命令  sqlite3 数据库文件名
//             数据库操作命令 查看表名称  .table
//                         查看字段    .schema
//                         历史操作记录 .dump
//             数据库查询语句      select * from tablename;
//             数据库插入数据语句   insert into table (字段) values (值);
//             insert into iot_table (pic,title,message) values (101,"iot" , "helloworld");
//             Cursor 游标，用于数据库查询后暂存查询结果
//
//
//

