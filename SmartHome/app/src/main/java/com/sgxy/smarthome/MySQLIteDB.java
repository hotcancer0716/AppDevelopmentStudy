package com.sgxy.smarthome;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLIteDB extends SQLiteOpenHelper {

    public MySQLIteDB(Context context ,String name ,
                      SQLiteDatabase.CursorFactory factory,int version){
        super(context,name,factory,version);
    }
    //快速创建数据库表
    //数据库版本的管理
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String Create_DB = "create table iot_table(_id integer primary key,pic integer,title text,message text)";
        sqLiteDatabase.execSQL(Create_DB);

        ContentValues value = new ContentValues();
        value.put("pic",R.drawable.c7k);
        value.put("title","iot");
        value.put("message","helloworld");
        for(int i = 0 ; i<20 ;i++) {
            sqLiteDatabase.insert("iot_table", null, value);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.d("TAG", "onUpgrade: ");
    }
}
