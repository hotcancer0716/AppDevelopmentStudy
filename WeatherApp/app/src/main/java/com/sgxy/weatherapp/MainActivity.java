package com.sgxy.weatherapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
//1.组织URL，
//2.建立线程Thread，网络请求
//3.通过okhttp网络请求
//4.得到json语句
//5.用gson框架，解析json
//6.组织message
//7.用handler把message发送到主线程
//8.主线程里接收并处理消息
//9.图片显示用Glide框架（with,load,into）

    String web_url="http://apis.juhe.cn/simpleWeather/query";
    String city_name = "韶关";
    String mykey = "60110cc0bb7993d48c4441f0b8059343";

    TextView Tx,TX_Temp,Tx_Info,Tx_Direct,Tx_Power;

    ImageView Img_Info,Img_bg;
    Spinner SP;
    String TAG = "%%%===>>";

    String[] City_Array = {"韶关","哈尔滨","三亚","西安","乌鲁木齐","拉萨","北京","南京","长沙","阳江"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Tx = findViewById(R.id.tx_json);
        SP = findViewById(R.id.sp_city);

        TX_Temp = findViewById(R.id.tx_temp);
        Tx_Direct = findViewById(R.id.tx_direct);
        Tx_Info = findViewById(R.id.tx_info);
        Tx_Power = findViewById(R.id.tx_power);

        Img_Info = findViewById(R.id.img_info);
        Img_bg = findViewById(R.id.img_bg);

        //Adapter适配器的初始化
        ArrayAdapter arrayAdapter = new ArrayAdapter(
                MainActivity.this,
                android.R.layout.simple_list_item_1,
                City_Array);
        //将初始化完成的适配器和Spinner控件进行绑定(setAdapter)
        SP.setAdapter(arrayAdapter);
        //对Spinner进行事件监听
        SP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "你选的城市是: " + City_Array[i]);
                city_name = City_Array[i];

                Thread_weather thread_weather = new Thread_weather();
                thread_weather.start();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    //Handler 线程间的通信机制（Message）

    Handler handler = new Handler(){
        @Override
        //接收消息（工作在主线程）
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case 1100:
                    //更新界面，更新UI（控件）
                    String Str_temp = msg.getData().getString("k_temp",null);
                    String Str_city = msg.getData().getString("k_city",null);
                    String Str_info = msg.getData().getString("k_info",null);
                    String Str_direct = msg.getData().getString("k_direct",null);
                    String Str_power = msg.getData().getString("k_power",null);

                    //更新到对应的控件去
                    TX_Temp.setText(Str_temp+"°");
                    Tx_Info.setText(Str_info);
                    Tx_Direct.setText(Str_direct);
                    Tx_Power.setText(Str_power);

                    switch (Str_info){
                        case "晴":
                            Img_Info.setImageResource(R.mipmap.ic_widget_sunny_shadow);
                            Glide.with(MainActivity.this).load("https://5b0988e595225.cdn.sohucs.com/c_fit,w_675,h_1200,q_70/images/20191123/5a946872e8054d5ab8db0ff7345ea703.jpeg").into(Img_bg);
                            break;
                        case "多云":
                            Img_Info.setImageResource(R.mipmap.ic_widget_mostlycloudy_shadow);
                            Glide.with(MainActivity.this)
                                    .load("https://5b0988e595225.cdn.sohucs.com/images/20191123/3cdc9e93da314767b8703e166a19f01c.jpeg")
                                    .into(Img_bg);
                            break;
                        case "阴":
                            Img_Info.setImageResource(R.mipmap.ic_widget_cloudy_shadow);
                            Glide.with(MainActivity.this).load("https://c-ssl.duitang.com/uploads/item/202007/06/20200706185031_dmgwc.jpeg").into(Img_bg);
                            break;
                        case "小雨":
                            Img_Info.setImageResource(R.mipmap.ic_widget_lightrain_shadow);
                            Glide.with(MainActivity.this).load("https://img.mp.itc.cn/upload/20161211/9b73276541d143c39895a4aba510b72c_th.jpeg").into(Img_bg);
                            break;
                    }

                    //Tx.setText(Str_city + "的天气是"+Str_info+" 温度"+Str_temp +"度");
                    break;
            }
        }
    };

    public class Thread_weather extends Thread{
        @Override
        public void run() {
            StringBuilder stringBuilder = new StringBuilder();

            String MyUrl = web_url+"?"+"city="+city_name+"&"+"key="+mykey;
            Log.d(TAG, MyUrl);
            try {
                URL url = new URL(MyUrl);
                HttpURLConnection huc = (HttpURLConnection) url.openConnection();
                huc.setRequestMethod("GET");
                huc.setConnectTimeout(3000);

                if (huc.getResponseCode() == 200){
                    BufferedReader bf = new BufferedReader(
                            new InputStreamReader(huc.getInputStream())
                    );

                    for ( String s=bf.readLine() ;s!=null;s=bf.readLine() ){
                        stringBuilder.append(s);
                    }

                    String Str_json = stringBuilder.toString();
                    Log.d(TAG, Str_json);

                    Weather_info weather_info =
                            new Gson().fromJson(Str_json , Weather_info.class);

                    String temp = weather_info.getResult().getRealtime().getTemperature();
                    String city = weather_info.getResult().getCity();
                    String info = weather_info.getResult().getRealtime().getInfo();
                    String direct = weather_info.getResult().getRealtime().getDirect();
                    String power = weather_info.getResult().getRealtime().getPower();

                    String Str_weather = city + "的天气是：  "+info+"   温度是："+temp+"摄氏度";
                    Log.d(TAG, Str_weather);

                    //子线程发送数据到主线程
                    Message msg = new Message();
                    msg.what = 1100;
                    Bundle bundle = new Bundle();
                    bundle.putString("k_temp",temp);
                    bundle.putString("k_city",city);
                    bundle.putString("k_info",info);
                    bundle.putString("k_direct",direct);
                    bundle.putString("k_power",power);
                    msg.setData(bundle);

                    handler.sendMessage(msg);
                }
            }catch (Exception e){
                Log.d(TAG,"Error!!!!");
            }

        }
    }
}