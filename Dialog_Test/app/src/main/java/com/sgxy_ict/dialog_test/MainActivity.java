package com.sgxy_ict.dialog_test;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    Button BTN_1,BTN_2,BTN_3,BTN_4,BTN_5,BTN_6;
    private int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BTN_1 = findViewById(R.id.btn_1);
        BTN_2 = findViewById(R.id.btn_2);
        BTN_3 = findViewById(R.id.btn_3);
        BTN_4 = findViewById(R.id.btn_4);
        BTN_5 = findViewById(R.id.btn_5);
        BTN_6 = findViewById(R.id.btn_6);

        BtnChick btnChick = new BtnChick();

        BTN_1.setOnClickListener(btnChick);
        BTN_2.setOnClickListener(btnChick);
        BTN_3.setOnClickListener(btnChick);
        BTN_4.setOnClickListener(btnChick);
        BTN_5.setOnClickListener(btnChick);
        BTN_6.setOnClickListener(btnChick);
    }


    public class BtnChick implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.btn_1:
                    //1.创建builder对象
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                    //2.初始化builer
                    builder.setTitle("警告！！")
                            .setCancelable(false)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Toast.makeText(MainActivity.this, "你点击的是"+getString(R.string.ok), Toast.LENGTH_SHORT).show();
                                    dialogInterface.dismiss();
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Toast.makeText(MainActivity.this, "你点击的是取消", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setIcon(R.mipmap.ic_launcher)
                            .setMessage("确定删除吗？");

                    //3.Create AlertDialog
                    AlertDialog alertDialog = builder.create();

                    //4.show
                    alertDialog.show();
                    break;

                case R.id.btn_2:
                    ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
                    progressDialog.setTitle("正在连接服务器... ");
                    progressDialog.setMessage("请等待");
                    progressDialog.create();
                    progressDialog.show();

                    break;

                case R.id.btn_3:
                    ProgressDialog h_progressDialog = new ProgressDialog(MainActivity.this);
                    h_progressDialog.setTitle("正在复制... ");
                    h_progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    h_progressDialog.setMax(100);
                    h_progressDialog.setProgress(67);
                    h_progressDialog.setMessage("请等待");
                    h_progressDialog.create();
                    h_progressDialog.show();
                    break;

                case R.id.btn_4:
                    String[] glass = {"大一","大二","大三","大四",};
                    //1.创建builder
                    AlertDialog.Builder singlebuilder = new AlertDialog.Builder(MainActivity.this);
                    //2.初始化builder
                    singlebuilder.setTitle("请选择你的年级")
                            .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Toast.makeText(MainActivity.this, "你点击的是"+glass[index], Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setSingleChoiceItems(glass, index, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int position) {
                            index = position;
                        }
                    });
                    //3.创建AlertDialog对象
                    AlertDialog singleAlertDialog = singlebuilder.create();
                    //4.show
                    singleAlertDialog.show();
                    break;

                case R.id.btn_5:
                    Calendar ca = Calendar.getInstance();
                    int  mYear = ca.get(Calendar.YEAR);
                    int  mMonth = ca.get(Calendar.MONTH);
                    int  mDay = ca.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                            Toast.makeText(MainActivity.this, "你选择的是"+year+"年"+month+"月"+day+"日", Toast.LENGTH_SHORT).show();
                        }
                    },mYear,mMonth,mDay);
                    datePickerDialog.create();
                    datePickerDialog.show();
                    break;

                case R.id.btn_6:
                    TimePickerDialog timePickerDialog = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int i, int i1) {

                        }
                    },11,32,true);
                    timePickerDialog.create();
                    timePickerDialog.show();
                    break;
                default:
                    break;
            }
        }
    }
}