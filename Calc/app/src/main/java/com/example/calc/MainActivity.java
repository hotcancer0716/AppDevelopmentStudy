package com.example.calc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView Tx_result;
    Button Btn_0,Btn_1,Btn_2,Btn_3,Btn_4,Btn_5,Btn_6,Btn_7,Btn_8,Btn_9;
    Button Btn_jian,Btn_add,Btn_cheng,Btn_clear,Btn_dengyu;

    int Calc_Status[]={0,1,2,3};
    int State = Calc_Status[1];
    //假设计算A+B
    //状态1为一开始的初始状态,即还没有按下运算符，此时按下任何数字都会在显示栏增加数字，这个数字就是A，但是0不能作为第一个按下的数字。
    //状态2为有按下运算符的状态，如“+，-，x”，当有运算符按下，程序进入状态2，此时再按下数字将作为数字B，同理，按下0也不会累计。
    //状态3为按下等于号的状态，显示运算结果在显示栏，此时按下运算符无效，按下数字键将强制变为状态1.
    //点击clear键，强制清除数字，并恢复到状态1。

    int yunsuan_Status[]={0,1,2,3};
    //三种运算符，1是加法，2是减法，3是乘法
    int yunsuan = 0;

    String Str_A,Str_B,Str_C;
    int Int_A,Int_B,Int_C;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Tx_result = findViewById(R.id.tx);
        Btn_0 = findViewById(R.id.btn_0);
        Btn_1 = findViewById(R.id.btn_1);
        Btn_2 = findViewById(R.id.btn_2);
        Btn_3 = findViewById(R.id.btn_3);
        Btn_4 = findViewById(R.id.btn_4);
        Btn_5 = findViewById(R.id.btn_5);
        Btn_6 = findViewById(R.id.btn_6);
        Btn_7 = findViewById(R.id.btn_7);
        Btn_8 = findViewById(R.id.btn_8);
        Btn_9 = findViewById(R.id.btn_9);
        Btn_jian = findViewById(R.id.btn_jian);
        Btn_add = findViewById(R.id.btn_add);
        Btn_cheng = findViewById(R.id.btn_x);
        Btn_clear = findViewById(R.id.btn_clear);
        Btn_dengyu = findViewById(R.id.btn_dengyu);

        Btn_0.setOnClickListener(new calc_click());
        Btn_1.setOnClickListener(new calc_click());
        Btn_2.setOnClickListener(new calc_click());
        Btn_3.setOnClickListener(new calc_click());
        Btn_4.setOnClickListener(new calc_click());
        Btn_5.setOnClickListener(new calc_click());
        Btn_6.setOnClickListener(new calc_click());
        Btn_7.setOnClickListener(new calc_click());
        Btn_8.setOnClickListener(new calc_click());
        Btn_9.setOnClickListener(new calc_click());
        Btn_add.setOnClickListener(new calc_click());
        Btn_jian.setOnClickListener(new calc_click());
        Btn_cheng.setOnClickListener(new calc_click());
        Btn_clear.setOnClickListener(new calc_click());
        Btn_dengyu.setOnClickListener(new calc_click());

    }

    public class calc_click implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btn_1:
                    add_num(1);
                    break;
                case R.id.btn_2:
                    add_num(2);
                    break;
                case R.id.btn_3:
                    add_num(3);
                    break;
                case R.id.btn_4:
                    add_num(4);
                    break;
                case R.id.btn_5:
                    add_num(5);
                    break;
                case R.id.btn_6:
                    add_num(6);
                    break;
                case R.id.btn_7:
                    add_num(7);
                    break;
                case R.id.btn_8:
                    add_num(8);
                    break;
                case R.id.btn_9:
                    add_num(9);
                    break;
                case R.id.btn_0:
                    add_num(0);
                    break;

                case R.id.btn_add:
                    add_yunsuan(1);
                    break;
                case R.id.btn_jian:
                    add_yunsuan(2);
                    break;
                case R.id.btn_x:
                    add_yunsuan(3);
                    break;

                case R.id.btn_dengyu:
                    dengyu();
                    break;

                case R.id.btn_clear:
                    Tx_result.setText("");
                    Str_A = Str_B = Str_C = null;
                    yunsuan = 0;
                    State = 1;
                    break;
            }

        }
    }

    public void add_num(int num){

        if (State == Calc_Status[1]){
            Str_A = Tx_result.getText().toString();
            if (num == 0 ){
                if (Str_A == null || Str_A.length()==0)
                   return;
            }
            Str_A = Str_A+num;
            Tx_result.setText(Str_A);
        }
        if (State == Calc_Status[2]){
            Str_B = Tx_result.getText().toString();
            if (num == 0 ){
                if (Str_B == null || Str_B.length()==0)
                    return;
            }
            Str_B = Str_B+num;
            Tx_result.setText(Str_B);
        }
        if (State == Calc_Status[3]){
            Toast.makeText(this, "请点击清除键", Toast.LENGTH_SHORT).show();
        }
    }

    public void add_yunsuan(int now_state){
        if (State == Calc_Status[1]){
            State = Calc_Status[2];
            yunsuan = yunsuan_Status[now_state];
            Tx_result.setText("");
        }
        if (State == Calc_Status[2]){
            State = Calc_Status[2];
            yunsuan = yunsuan_Status[now_state];
            Tx_result.setText("");
        }
        if (State == Calc_Status[3]){
            Toast.makeText(this, "请点击清除键", Toast.LENGTH_SHORT).show();
        }
    }

    public void dengyu(){
        if (State == Calc_Status[3]){
            Toast.makeText(this, "请点击清除键", Toast.LENGTH_SHORT).show();
        }

        if (State == Calc_Status[1]){

        }
        if (State == Calc_Status[2]){
            State = Calc_Status[3];
            Int_A = Integer.parseInt(Str_A);
            Int_B = Integer.parseInt(Tx_result.getText().toString());
            if (yunsuan == yunsuan_Status[1]){
                Int_C = Int_A + Int_B;
            }
            if (yunsuan == yunsuan_Status[2]){
                Int_C = Int_A - Int_B;
            }
            if (yunsuan == yunsuan_Status[3]){
                Int_C = Int_A * Int_B;
            }
            Tx_result.setText(""+Int_C);
        }

    }
}