package com.sgxy_ict.connetonenet;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Toolbar;

import com.chinamobile.iot.onenet.edp.CmdMsg;
import com.chinamobile.iot.onenet.edp.CmdRespMsg;
import com.chinamobile.iot.onenet.edp.Common;
import com.chinamobile.iot.onenet.edp.ConnectRespMsg;
import com.chinamobile.iot.onenet.edp.EdpMsg;
import com.chinamobile.iot.onenet.edp.PingRespMsg;
import com.chinamobile.iot.onenet.edp.SaveDataMsg;
import com.chinamobile.iot.onenet.edp.SaveRespMsg;
import com.chinamobile.iot.onenet.edp.toolbox.EdpClient;
import com.chinamobile.iot.onenet.edp.toolbox.Listener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private int connetcType = 1;
    private String Deviceid = "740795533";
    private String api_key = "8OHfzUm4fQH=sOBPfwKk=qcfNaQ=";
    private int encryptType = -1;

    EditText InputData,InputData2;
    Button SaveData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InputData = findViewById(R.id.input_data);
        InputData2 = findViewById(R.id.input_data2);
        SaveData = findViewById(R.id.savedata);


        // 1、初始化SDK
        EdpClient.initialize(this, connetcType, Deviceid, api_key);

        // 2、设置接收响应的回调
        EdpClient.getInstance().setListener(mEdpListener);

        // 3、设置自动发送心跳的周期（默认4min）
        EdpClient.getInstance().setPingInterval(3 * 60 * 1000);

        // 4、建立TCP连接
        EdpClient.getInstance().connect();

        if (Common.Algorithm.NO_ALGORITHM == encryptType) {
            // 5、如果使用明文通信，则建立连接后直接发送连接请求
            EdpClient.getInstance().sendConnectReq();
        } else if (Common.Algorithm.ALGORITHM_AES == encryptType) {
            // 6、如果使用加密通信，则先发送加密请求，然后在加密响应回调中发送连接请求
            EdpClient.getInstance().requestEncrypt(Common.Algorithm.ALGORITHM_AES);
        }

        SaveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*                try {
                    JSONObject data = new JSONObject();
                    data.put("temp" , InputData.getText().toString() );
                    data.put("humi" , InputData2.getText().toString() );
                    data.put("CO2",650);
                    EdpClient.getInstance().saveData(Deviceid , 3 , api_key ,data.toString().getBytes());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
*/

            //GPS
                String Str_lon = InputData.getText().toString() ;
                String Str_lat = InputData2.getText().toString();
                JSONObject data = new JSONObject();
                JSONObject gps = new JSONObject();
                try {
                    data.put("lon", Float.parseFloat(Str_lon) );
                    data.put("lat", Float.parseFloat(Str_lat) );
                    SaveDataMsg.packSaveData1Msg(gps,
                            null,"GPS",null,data);
                    EdpClient.getInstance().saveData(Deviceid,
                            1,api_key,gps.toString().getBytes());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private Listener mEdpListener = new Listener() {

        @Override
        public void onReceive(List<EdpMsg> msgList) {
            if (null == msgList) {
                return;
            }
            for (EdpMsg msg : msgList) {
                if (null == msg) {
                    continue;
                }
                switch (msg.getMsgType()) {

                    // 连接响应
                    case Common.MsgType.CONNRESP:
                      ConnectRespMsg connectRespMsg = (ConnectRespMsg) msg;
                      if (connectRespMsg.getResCode() == Common.ConnResp.ACCEPTED){
                          Toast.makeText(MainActivity.this, "连接成功 ResCode:" + connectRespMsg.getResCode(), Toast.LENGTH_SHORT).show();
                          Log.d("debug","连接成功 ResCode:" + connectRespMsg.getResCode());
                      }
                      else{
                          Toast.makeText(MainActivity.this, "连接失败 ResCode:" + connectRespMsg.getResCode(), Toast.LENGTH_SHORT).show();
                          Log.d("debug","连接失败 ResCode:" + connectRespMsg.getResCode());
                      }

                      break;

                    // 心跳响应
                    case Common.MsgType.PINGRESP:
                        PingRespMsg pingRespMsg = (PingRespMsg) msg;
                        Log.d("debug", "心跳响应 pingRespMsg: ");
                        Toast.makeText(MainActivity.this, "心跳响应", Toast.LENGTH_SHORT).show();
                       break;

                    // 存储确认
                    case Common.MsgType.SAVERESP:
                        SaveRespMsg saveRespMsg = (SaveRespMsg) msg;
                        Log.d("debug","存储确认: "+new String(saveRespMsg.getData()));
                        Toast.makeText(MainActivity.this, "存储确认: "+new String(saveRespMsg.getData()), Toast.LENGTH_SHORT).show();
                        break;

                    // 转发（透传）
                    case Common.MsgType.PUSHDATA:
                        break;

                    // 存储（转发）
                    case Common.MsgType.SAVEDATA:
                        Log.d("debug","存储确认: ");
                        Toast.makeText(MainActivity.this, "存储确认: ", Toast.LENGTH_SHORT).show();
                        break;

                    // 命令请求
                    case Common.MsgType.CMDREQ:
                        CmdMsg cmdMsg = (CmdMsg) msg;
                        String cmdID = ((CmdMsg) msg).getCmdId();
                        String cmdData = new String ( ((CmdMsg) msg).getData());
                        Toast.makeText(MainActivity.this,
                                "cmdID:"+cmdID + "  cmdData:"+cmdData, Toast.LENGTH_SHORT).show();
                        Log.d("debug","cmdID:"+cmdID + "  cmdData:"+cmdData);
                        switch (cmdData){
                            case "open water":
                                Toast.makeText(MainActivity.this, "灌溉打开", Toast.LENGTH_SHORT).show();
                                break;
                            case "close water":
                                Toast.makeText(MainActivity.this, "灌溉关闭", Toast.LENGTH_SHORT).show();
                                break;
                            case "open light":
                                Toast.makeText(MainActivity.this, "照明打开", Toast.LENGTH_SHORT).show();
                                break;


                        }

                        break;

                    // 加密响应
                    case Common.MsgType.ENCRYPTRESP:
                        break;
                }
            }
        }

        @Override
        public void onFailed(Exception e) {
            e.printStackTrace();
        }

        @Override
        public void onDisconnect() {

        }
    };
}
