package com.example.net_test;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyHttpActivity extends AppCompatActivity {
    Button Btn_GetPic;
    TextView PIC;
    EditText ET_Input;
    //星座
 //   String myURL = "http://web.juhe.cn/constellation/getAll";
 //   String myKey = "bb1b2eb92a05907cf93884c4c216e6b5";
    String consName = "";
    String str=null;

    //天气
    String myURL = "https://apis.juhe.cn/simpleWeather/query";
    String myKey = "60110cc0bb7993d48c4441f0b8059343";

    private final int shuaxin = 7788;
    private final int UPDATE = 5566;

    Bitmap bmp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_url);

        Btn_GetPic = findViewById(R.id.btn_getpic);
        PIC = findViewById(R.id.text);
        ET_Input = findViewById(R.id.edit_input);

        Btn_GetPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                consName = ET_Input.getText().toString();
                GetPic_Thread getPicThread = new GetPic_Thread();
                getPicThread.start();
            }
        });

        NewTimeThread newTimeThread = new NewTimeThread();
        newTimeThread.start();
    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case shuaxin:
                    //显示文本
                    PIC.setText(str);

                    break;

                case UPDATE:
                    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:MM:SS");
                    Date date = new Date(System.currentTimeMillis());
                    Btn_GetPic.setText(dateFormat.format(date));
                    handler.sendEmptyMessageDelayed(UPDATE,1000);
                    break;
            }
        }
    };

    public class GetPic_Thread extends Thread{
        @Override
        public void run() {
            super.run();
            //网络连接，并显示
            try {
                //把String转化为URL格式（类型）
               // String uri= myURL+"?consName="+consName+"&key="+myKey+"&type=today";
                String uri= myURL+"?city="+consName+"&key="+myKey;
                URL url = new URL(uri);
                Log.d("log", uri);
                //连接网页
                HttpURLConnection huc = (HttpURLConnection) url.openConnection();
                huc.setConnectTimeout(2000);
 //               huc.setRequestMethod("get");
                int ResponseCode = huc.getResponseCode();
                if (ResponseCode != 404){
                    Log.d("TAG", "返回码是: "+ResponseCode);
                    //输入流
                    InputStream is = huc.getInputStream();
                    //解析is
                    StringBuffer stringBuffer = new StringBuffer();
                    byte[] bs = new byte[1024];
                    int len =0;
                    while ((len = is.read(bs))!=-1){
                        str = new String(bs,0,len);
                        stringBuffer.append(str);
                    }
                    Log.d("TAG", str);

                    String cityname,temp,humi,widy;

                    Weatherdata wdata = new Gson().fromJson(str,Weatherdata.class);
                    cityname = wdata.getResult().getCity().toString();
                    temp = wdata.getResult().getRealtime().getTemperature().toString();
                    humi = wdata.getResult().getRealtime().getHumidity().toString();
                    widy = wdata.getResult().getRealtime().getWid().toString();

                    str = cityname +"温度是："+ temp +"湿度是："+ humi +"风力是："+widy;


                    handler.sendEmptyMessage(shuaxin);

                }


            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }
    }

    public class NewTimeThread extends Thread{
        @Override
        public void run() {
            super.run();
            handler.sendEmptyMessage(UPDATE);
        }
    }

}