package com.example.net_test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    Button Btn_GetPic;
    ImageView PIC;
    String myURL = "http://www.xmsouhu.com/d/file/tupian/bizhi/2021-10-18/4b9ecab6c2bb54ec59d41eff3ecb2c7d.jpg";
    private final int shuaxin = 7788;
    private final int UPDATE = 5566;

    Bitmap bmp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Btn_GetPic = findViewById(R.id.btn_getpic);
        PIC = findViewById(R.id.pic);

        Btn_GetPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetPic_Thread getPicThread = new GetPic_Thread();
                getPicThread.start();
            }
        });

        NewTimeThread newTimeThread = new NewTimeThread();
        newTimeThread.start();
    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case shuaxin:
                    //显示图片
                    PIC.setImageBitmap(bmp);
                    break;

                case UPDATE:
                    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:MM:SS");
                    Date date = new Date(System.currentTimeMillis());
                    Btn_GetPic.setText(dateFormat.format(date));
                    handler.sendEmptyMessageDelayed(UPDATE,1000);
                    break;
            }
        }
    };

    public class GetPic_Thread extends Thread{
        @Override
        public void run() {
            super.run();
            //网络连接，并显示
            try {
                //把String转化为URL格式（类型）
                URL url = new URL(myURL);
                //建立连接,得到输入流对象
                InputStream is = url.openStream();
                //把InputStream转化成图片格式
                bmp = BitmapFactory.decodeStream(is);
                //通知主线程（UI线程）可以更新界面了
                handler.sendEmptyMessage(shuaxin);

            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }
    }

    public class NewTimeThread extends Thread{
        @Override
        public void run() {
            super.run();
            handler.sendEmptyMessage(UPDATE);
        }
    }

}