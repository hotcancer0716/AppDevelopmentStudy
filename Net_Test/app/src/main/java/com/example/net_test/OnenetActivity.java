package com.example.net_test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttp;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class OnenetActivity extends AppCompatActivity {
    TextView Tx;
    final int msg_ref = 999;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onenet);

        Tx = findViewById(R.id.result);

        Connect_Onenet_Thread onenet_thread = new Connect_Onenet_Thread();
        onenet_thread.start();
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case msg_ref:
                    Tx.setText(msg.getData().getString("speed").toString());
                    break;
            }
        }
    };

    public class Connect_Onenet_Thread extends Thread{
        @Override
        public void run() {
            super.run();
            //1。组织网络请求
            Request request = new Request.Builder()
                    .get()
                    .url("http://api.heclouds.com/devices/740795533/datastreams/gas")
                    .addHeader("api-key","8OHfzUm4fQH=sOBPfwKk=qcfNaQ=")
                    .build();
            //2。连接网络，连接服务器
            Call call = new OkHttpClient().newCall(request);
            //3。处理返回事件
            call.enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    Log.d("TAG", "onFailure: ");
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    String Str_json = response.body().string();

                    Onenet_data one_data = new Gson().fromJson(Str_json,Onenet_data.class);
                    String Values = one_data.getData().getCurrentValue().toString();
                    Log.d("TAG", Values);

                    //子线程发送多个数据到UI线程
                    Message msg = new Message();
                    msg.what = msg_ref;
                    Bundle bundle = new Bundle();
                    bundle.putString("speed",Values);
                    msg.setData(bundle);
                    handler.sendMessage(msg);

                }
            });


        }
    }
}