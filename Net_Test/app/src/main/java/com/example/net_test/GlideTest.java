package com.example.net_test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class GlideTest extends AppCompatActivity {

    ImageView Img;
    Button Btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Img = findViewById(R.id.pic);
        Btn = findViewById(R.id.btn_getpic);

        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://img.alicdn.com/imgextra/i2/723011868/O1CN01jlXAEK1PfaTmOQOHc_!!723011868.jpg";
                Glide.with(GlideTest.this).load(url).into(Img);
            }
        });

    }
}