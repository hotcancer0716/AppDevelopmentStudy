package com.example.net_test;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UrlActivity extends AppCompatActivity {
    Button Btn_GetPic;
    TextView PIC;
    EditText ET_Input;
    String myURL = "www.baidu.com";
    String str=null;

    private final int shuaxin = 7788;
    private final int UPDATE = 5566;

    Bitmap bmp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_url);

        Btn_GetPic = findViewById(R.id.btn_getpic);
        PIC = findViewById(R.id.text);
        ET_Input = findViewById(R.id.edit_input);

        Btn_GetPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myURL = ET_Input.getText().toString();
                GetPic_Thread getPicThread = new GetPic_Thread();
                getPicThread.start();
            }
        });

        NewTimeThread newTimeThread = new NewTimeThread();
        newTimeThread.start();
    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case shuaxin:
                    //显示文本
                    PIC.setText(str);

                    break;

                case UPDATE:
                    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:MM:SS");
                    Date date = new Date(System.currentTimeMillis());
                    Btn_GetPic.setText(dateFormat.format(date));
                    handler.sendEmptyMessageDelayed(UPDATE,1000);
                    break;
            }
        }
    };

    public class GetPic_Thread extends Thread{
        @Override
        public void run() {
            super.run();
            //网络连接，并显示
            try {
                //把String转化为URL格式（类型）
                URL url = new URL("http://"+myURL);
                //连接网页
                URLConnection urlConnection = url.openConnection();
                //输入流
                InputStream is = urlConnection.getInputStream();
                //解析is
                StringBuffer stringBuffer = new StringBuffer();
                byte[] bs = new byte[1024];
                int len =0;
                while ((len = is.read(bs))!=-1){
                    str = new String(bs,0,len);
                    stringBuffer.append(str);
                }
                Log.d("TAG", str);
                handler.sendEmptyMessage(shuaxin);

            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }
    }

    public class NewTimeThread extends Thread{
        @Override
        public void run() {
            super.run();
            handler.sendEmptyMessage(UPDATE);
        }
    }

}