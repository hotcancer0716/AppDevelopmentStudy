package com.sgxy.mt_motor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttp;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class WeatherActivity extends AppCompatActivity {
    String TAG = "test++++++++++>";
    String url = "韶关";

    TextView Tx_w;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        Tx_w = findViewById(R.id.tx_weather);

        String city_name = "韶关";
        String base_url  = "http://apis.juhe.cn/simpleWeather/query";
        String api_key = "60110cc0bb7993d48c4441f0b8059343";

        url = base_url+"?city="+city_name+"&key="+api_key;

        //当Android APP进入Activity时，当前状态是在主线程中，主线程的另一个叫法（名字）UI线层。
        //Android线程原则，1.在主线程禁止做耗时的操作。 2.在其他线程里，不能操作UI
        Log.d(TAG, url);

        //1.创建线程对象。 2.启动线程
        ThreadWeather weather = new ThreadWeather();
        weather.start();  //启动线程用start，不能用run
    }

    //Handler（处理者）实现线程间通信
    Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            //处理消息
            switch (msg.what){
                case 111:
                    Log.d(TAG, "接收到消息： "+msg.what);
                    Tx_w.setText(msg.getData().getString("k_result"));
                    break;
            }
        }
    };


    //1.创建网络通信线程
    public class ThreadWeather extends Thread{
        @Override
        public void run() {
            //网络通信的实现
            //okhttp的使用步骤。1.建立请求， 2. 接收数据（队列） 3.解析json数据
            Request request = new Request.Builder().get().url(url).build();
            //2. 接收数据（队列）
            Call call = new OkHttpClient().newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    //当通信失败时，执行这里
                    Log.d(TAG, "onFailure");
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    //当通信成功时，执行这里
                    if(response.code()==200){
                        String result = response.body().string();
                        Log.d(TAG, result);

                        //Json数据用Gson模块解析
                        Weather_Data wd= new Gson().fromJson(result , Weather_Data.class);
                        String cityname = wd.getResult().getCity().toString();
                        String temp = wd.getResult().getRealtime().getTemperature().toString();
                        String future_info = wd.getResult().getFuture().get(0).getWeather().toString();
                        String weather_msg = "当前城市："+cityname+"  温度是："+temp+"度";
                        weather_msg = "当前城市："+cityname+"  明天的天气是："+future_info;

                        //线程间通信，把子线程的内容，发送到主线程
                        //msg 快递员，Bundle快递盒 msg是物流公司
                        Message msg = new Message();
                        msg.what = 111;
                        Bundle bundle = new Bundle();
                        bundle.putString("k_result",weather_msg);
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                    }
                }
            });
       super.run();
        }
    }
}