package com.sgxy.mt_motor;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MT_OpenHelper extends SQLiteOpenHelper {

    public MT_OpenHelper(Context context ,String name ,
                         SQLiteDatabase.CursorFactory factory,int version){
        super(context,name,factory,version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CreateTable = "create table stage(_id integer primary key,st_name text,distance text,price text,state integer)";
        sqLiteDatabase.execSQL(CreateTable);

        ContentValues values = new ContentValues();
        values.put("st_name","大学路充电站");
        values.put("distance","2.5km");
        values.put("price","3元/时");
        values.put("state",R.drawable.ayc);

        sqLiteDatabase.insert("stage",null,values);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
