package com.sgxy.mt_motor;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cursoradapter.widget.SimpleCursorAdapter;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView LV;
    ImageView Img;
    EditText Et_name,Et_distance,Et_price;
    Button Btn_Ok;
    Switch SWB;

    SQLiteDatabase sqldb;
    Cursor cursor;

    boolean state_insert = true;
    String id_key = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LV = findViewById(R.id.lv_stage);
        Et_name = findViewById(R.id.ed_name);
        Et_distance = findViewById(R.id.ed_distance);
        Et_price = findViewById(R.id.ed_price);
        Btn_Ok = findViewById(R.id.btn_save);
        Img = findViewById(R.id.pic);
        SWB = findViewById(R.id.sw_b);

        //1. 创建/连接 SQLite数据库 （OpenHelper）
        MT_OpenHelper mt_openHelper = new MT_OpenHelper(
                MainActivity.this,"MT.db",null,1);
        //2. SQLite数据库实例化，创建SQLite对象
        sqldb = mt_openHelper.getWritableDatabase();

        sql_show();

        SWB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    SWB.setText("更新");
                    state_insert = false;
                }else {
                    SWB.setText("新增");
                    state_insert = true;

                }
            }
        });
        Btn_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContentValues values = new ContentValues();
                values.put("st_name",Et_name.getText().toString());
                values.put("distance",Et_distance.getText().toString());
                values.put("price",Et_price.getText().toString());
                values.put("state",list_head_pic[index]);

                if (state_insert){
                    //数据插入
                    sqldb.insert("stage",null,values);
                }else {
                    //数据更新
                    sqldb.update("stage",values,"_id=?",new String[]{id_key});
                }
                sql_show();
            }
        });

        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                cursor.moveToPosition(position);
                id_key = cursor.getString(0);
                Et_name.setText(cursor.getString(1));
                Et_distance.setText(cursor.getString(2));
                Et_price.setText(cursor.getString(3));
            }
        });
        LV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                //对话框
                //长按，出发对话框
                //使用构造器builder方式创建对话框
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setIcon(R.drawable.a7).
                        setCancelable(false).
                        setTitle("警告！！").setNeutralButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int j) {
                                //删除指定内容
                                cursor.moveToPosition(i);
                                String Str_find = cursor.getString(1);
                                sqldb.delete("stage","st_name=?",new String[]{Str_find});
                                sql_show();
                            }
                        }).setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).setNegativeButton("Detail", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).
                        setMessage("你确定删除吗？"  );
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

                return false;
            }
        });
    }

    private void sql_show(){

        //3. 查询数据到游标Cursor
        cursor = sqldb.query("stage",new String[]{"_id","st_name","distance","price","state"},null,null,null,null,"_id DESC", null);
        //4. 适配器初始化
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(
                MainActivity.this,
                R.layout.item_layout,
                cursor,
                new String[]{"st_name","distance","price","state"},
                new int[]{R.id.tx_stage_name,R.id.tx_stage_distance,R.id.tx_stage_price,R.id.img_stage
                });
        //5. 适配器和Listview绑定/适配
        LV.setAdapter(adapter);
    }

    float x1,x2;
    int[] list_head_pic = {R.drawable.ayc,R.drawable.ayb,
            R.drawable.aye};
    int index = 0;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //获取按下时的x轴坐标
        if(MotionEvent.ACTION_DOWN == event.getAction()){
            x1 = event.getX();
        }

        //获取抬起时的x轴坐标
        if(MotionEvent.ACTION_UP == event.getAction()) {
            x2 = event.getX();

            if (x2 - x1 > 50) {
                //右划,显示上一张
                index--;
                if (index < 0 )
                    index = list_head_pic.length - 1;
                Img.setImageResource(list_head_pic[index]);
            }
            if (x1 - x2 > 50) {
                index++;
                if (index == list_head_pic.length)
                    index = 0;
                Img.setImageResource(list_head_pic[index]);
            }
        }
        return super.onTouchEvent(event);
    }
}