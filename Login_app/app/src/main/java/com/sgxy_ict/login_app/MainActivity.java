package com.sgxy_ict.login_app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText ED_Name;
    EditText ET_Password;
    TextView Tx_Title,Tx_Debug;
    ImageView Img_Head;
    CheckBox CB_Basket,CB_Foot,CB_Yumao,CB_Movie;
    ProgressBar PG_Yuan;

    String CodePassword = "123456";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CB_Basket = findViewById(R.id.cb_basketball);
        CB_Foot = findViewById(R.id.cb_football);
        CB_Yumao = findViewById(R.id.cb_yumao);
        CB_Movie = findViewById(R.id.cb_movie);
        Img_Head = findViewById(R.id.img_head);
        Tx_Title = findViewById(R.id.tx_title);
        Tx_Title.setText("信息工程学院学籍管理系统");
        Tx_Debug = findViewById(R.id.tx_debug);
        ED_Name = findViewById(R.id.ed_username);
        PG_Yuan = findViewById(R.id.pg_yuan);

        ET_Password = findViewById(R.id.et_password);

        Button BTN_Login = findViewById(R.id.btn_login);

        BTN_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Str_name = ED_Name.getText().toString();
                String mima = ET_Password.getText().toString();

                if( Str_name == null || Str_name.length() == 0 ){
                    Tx_Debug.setText("用户名不能为空！！");
                }else{
                    if (mima == null || mima.length() == 0){
                        Tx_Debug.setText("请输入密码！！");
                    }else {
                        String Checkbox_String = "";
                        if (mima.equals(CodePassword)){
                            if (CB_Basket.isChecked())
                                Checkbox_String = CB_Basket.getText().toString();
                            if (CB_Foot.isChecked())
                                Checkbox_String = Checkbox_String + CB_Foot.getText().toString();

                            //VISIBLE   显示
                            //INVISIBLE 隐藏，但是仍占据位置
                            //GONE      隐藏，位置不保留了

                            PG_Yuan.setVisibility(View.INVISIBLE);

                            Tx_Debug.setText(Checkbox_String+"被选中");
                       //     Img_Head.setImageResource(R.drawable.zyl);
                        }
                        else
                            Tx_Debug.setText("密码错误！！！");
                    }
                }
            }
        });



    }
}
