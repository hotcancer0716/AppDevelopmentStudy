package com.sgxy_ict.calcbaseonbutterknife;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.bumptech.glide.Glide;
import com.sgxy_ict.calcbaseonbutterknife.databinding.ActivityMainBinding;

/*
* google官方建议的新模式，viewBinding用于取代findviewbyid
* 同时原有的butterknife也不能再用了，
* viewBinding使用方法如下：
* 1.build.gradle(app)中，增加
     viewBinding{
        enabled = true;
    }
* 2.正常编写布局文件
* 3.在Activity中实例化     ActivityMainBinding this_;
* 4.this_ = ActivityMainBinding.inflate(getLayoutInflater());
* 5.setContentView(this_.getRoot());
* 6.使用控件即可 this_.tx.setText("");
* 7.或按键监听 this_.btn0.setOnClickListener(btn_click);
*
* 优点在于，简化 View控件的声明 和 频繁的findviewbyid操作。
* 但是缺点是失去了butterknife快速实例化按键监听的效果。按键监听需要自己重新写
* */
public class MainActivity extends AppCompatActivity {

    ActivityMainBinding this_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this_ = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(this_.getRoot());

        Btn_Click btn_click = new Btn_Click();
        this_.btn0.setOnClickListener(btn_click);
        this_.btn1.setOnClickListener(btn_click);
        this_.btn2.setOnClickListener(btn_click);
        this_.btn3.setOnClickListener(btn_click);
        this_.btn4.setOnClickListener(btn_click);
        this_.btn5.setOnClickListener(btn_click);
        this_.btn6.setOnClickListener(btn_click);
        this_.btn7.setOnClickListener(btn_click);
        this_.img.setImageResource(R.drawable.aaa);
    }

    public class Btn_Click implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btn_0:
                    this_.tx.setText(""+view.getId());
                    break;
                case R.id.btn_1:
                    this_.tx.setText(""+view.getId());
                    break;
                case R.id.btn_2:
                    this_.tx.setText(""+view.getId());
                    break;
                case R.id.btn_3:
                    this_.tx.setText(""+view.getId());
                    break;
                case R.id.btn_4:
                    this_.tx.setText(""+view.getId());
                    break;
                case R.id.btn_5:
                    this_.tx.setText(""+view.getId());
                    break;
                case R.id.btn_6:
                    String url="https://img1.baidu.com/it/u=4216741981,2520985741&fm=253&fmt=auto&app=138&f=GIF?w=296&h=282";
                    Glide.with(MainActivity.this).load(url).error(R.drawable.ic_launcher_background).placeholder(R.drawable.ic_launcher_background).into(this_.img);
                    break;
                case R.id.btn_7:
                    String url_7="https://c-ssl.dtstatic.com/uploads/item/202007/22/20200722010405_343ur.thumb.1000_0.jpeg";
                    Glide.with(MainActivity.this).load(url_7).error(R.drawable.ic_launcher_background).placeholder(R.drawable.ic_launcher_background).into(this_.img);
                    break;
            }
        }
    }
}