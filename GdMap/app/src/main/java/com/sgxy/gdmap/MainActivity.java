package com.sgxy.gdmap;


import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.amap.api.location.AMapLocationClient;
import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.amap.api.maps.MapsInitializer;

public class MainActivity extends AppCompatActivity {
    private MapView mMapView  = null;//地图容器
    private AMap aMap;//地图对象AMap
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initMap(savedInstanceState);
    }

    /**
     * 初始化地图
     * @param savedInstanceState
     */
    private void initMap(Bundle savedInstanceState) {
        MapsInitializer.updatePrivacyShow(this, true, true);//隐私合规接口
        MapsInitializer.updatePrivacyAgree(this, true);//隐私合规接口
        AMapLocationClient.updatePrivacyAgree(this, true);
        AMapLocationClient.updatePrivacyShow(this, true, true);
        mMapView = findViewById(R.id.map); //获取地图视图对象
        mMapView.onCreate(savedInstanceState);//创建地图
        if (aMap == null) {
            aMap = mMapView.getMap();
        }
    }
    /**
     * 生命周期-onDestroy
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy(); //销毁地图
    }
    /**
     * 生命周期-onResume
     */
    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume(); //重新绘制加载地图
    }
    /**
     * 生命周期-onPause
     */
    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();//暂停地图的绘制
    }
    /**
     * 生命周期-onSaveInstanceState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);//保存地图当前的状态
    }
}