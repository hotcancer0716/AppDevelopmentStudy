package com.sgxy_ict.uvc_demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.zqx.chart.anim.Anim;
import com.zqx.chart.data.HistogramData;
import com.zqx.chart.view.Histogram;

import java.util.List;

public class ResultActivity extends AppCompatActivity {

    Histogram histogramChart;
    TextView tv_result;
    TextView tv_weight;
    TextView tv_result_prefix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        tv_result = findViewById(R.id.tv_result);
        tv_result_prefix = findViewById(R.id.tv_result_prefix);
        tv_weight = findViewById(R.id.tv_weight);

        Intent intent = getIntent();
        String rec_json = intent.getExtras().getString("send_json");
        String rec_weight = intent.getExtras().getString("weight");

        IngredientData ingredientData = new Gson().fromJson(rec_json, IngredientData.class);
        try{
            int resultNum = ingredientData.getResultNum();
            List<IngredientData.ResultDTO> list = ingredientData.getResult();

            String[] xdata = new String[5];
            float[] ydata = new float[5];
            for(int i = 0; i < resultNum; i++){
                IngredientData.ResultDTO result = list.get(i);
                xdata[i] = result.getName();
                ydata[i] = result.getScore().floatValue()*100;
            }

            if(!xdata[0].equals("非果蔬食材")) {
                tv_result.setText(xdata[0]);
                tv_result.setTextColor(getResources().getColor(R.color.colorPrimary));
                tv_result_prefix.setText("该果蔬很有可能是");
                tv_result_prefix.setVisibility(View.VISIBLE);
            }
            else if(ydata[1] > 15) {
                tv_result.setText(xdata[1]);
                tv_result.setTextColor(getResources().getColor(R.color.colorPrimary));
                tv_result_prefix.setText("该果蔬有可能是");
                tv_result_prefix.setVisibility(View.VISIBLE);
            }
            else {
                tv_result.setText("未识别到果蔬");
                tv_result.setTextColor(getResources().getColor(R.color.colorAccent));
                tv_result_prefix.setVisibility(View.INVISIBLE);
            }

            histogramChart = (Histogram)findViewById(R.id.histoGram);
            HistogramData histogramData = HistogramData.builder()
                    .setXdata(xdata)
                    .setYdata(ydata)
                    .setAnimType(Anim.ANIM_ALPHA)
                    .setXpCount(5)
                    .setYpCount(5)
                    .build();
            histogramChart.setChartData(histogramData);
        }catch (Exception e){

        }

        tv_weight.setText(rec_weight);
    }
}