package com.sgxy_ict.uvc_demo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IngredientData {

    /**
     * log_id : 4906069875050455782
     * result_num : 5
     * result : [{"score":0.9973669648170471,"name":"非果蔬食材"},{"score":0.001601499505341053,"name":"葡萄"},{"score":2.866595605155453E-5,"name":"甜瓜"},{"score":2.7075879188487306E-5,"name":"白菜"},{"score":2.47490042966092E-5,"name":"西瓜"}]
     */

    @SerializedName("log_id")
    private Long logId;
    @SerializedName("result_num")
    private Integer resultNum;
    @SerializedName("result")
    private List<ResultDTO> result;

    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public Integer getResultNum() {
        return resultNum;
    }

    public void setResultNum(Integer resultNum) {
        this.resultNum = resultNum;
    }

    public List<ResultDTO> getResult() {
        return result;
    }

    public void setResult(List<ResultDTO> result) {
        this.result = result;
    }

    public static class ResultDTO {
        /**
         * score : 0.9973669648170471
         * name : 非果蔬食材
         */

        @SerializedName("score")
        private Double score;
        @SerializedName("name")
        private String name;

        public Double getScore() {
            return score;
        }

        public void setScore(Double score) {
            this.score = score;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
