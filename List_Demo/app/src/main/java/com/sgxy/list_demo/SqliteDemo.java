package com.sgxy.list_demo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SqliteDemo extends SQLiteOpenHelper {

    String TAG = "!!!";

    public SqliteDemo(Context context){
        super(context,"sql.bd",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //onCreate只执行一次
        //创建数据库文件db
        Log.d(TAG, "SQLiteOpenHelper onCreate: ");
        sqLiteDatabase.execSQL("create table sensor(_id integer primary key autoincrement ,name text,value text,class text,icon integer,position integer);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //更新表的版本
    }
}
