package com.sgxy.list_demo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cursoradapter.widget.SimpleCursorAdapter;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SQLActivity extends AppCompatActivity {

    ListView LV;
    EditText Et_name,Et_value,Et_class;
    Switch Sw;
    Button Btn_save;

    Spinner SP_Icon;

    int updata_id=0;
    int icon_drawable = R.drawable.bxp;
    int[] icon_Array = {R.drawable.bxp,R.drawable.bys,R.drawable.byt,R.drawable.byu,R.drawable.byv,R.drawable.byx,R.drawable.byy,R.drawable.byz,R.drawable.c0p,R.drawable.c0t,R.drawable.c0v};
    String  TAG = "gogogo!";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sqlactivity_layout_h);
        Log.d(TAG, "onCreate: ");
        View_init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");
    }
    
    

    private void sql_show(SQLiteDatabase sql){
        //查询数据
        Cursor cursor = sql.query("sensor",null,null,null,null,null,"_id desc",null);

        //将结果显示到Listview,   初始化Adapter是前提
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(
                SQLActivity.this,
                R.layout.item_layout,
                cursor,
                new String[]{"name","value","class","icon"},
                new int[]{R.id.item_text_sen,R.id.item_text_value,R.id.item_text_danwei,R.id.item_img}
        );
        LV.setAdapter(adapter);
    }

    private void View_init(){
        LV = findViewById(R.id.sql_listview);
        Et_name= findViewById(R.id.sql_et_name);
        Et_value= findViewById(R.id.sql_et_value);
        Et_class= findViewById(R.id.sql_et_style);
        Btn_save = findViewById(R.id.sql_btn_save);
        Sw = findViewById(R.id.sql_sw);
        SP_Icon = findViewById(R.id.sql_sp);

        SqliteDemo sqliteDemo = new SqliteDemo(SQLActivity.this);
        SQLiteDatabase sql = sqliteDemo.getWritableDatabase(); //连接数据库

        sql_show(sql);

        //点击按键进行数据库插入
        Btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //插入数据的方法
                ContentValues values = new ContentValues();
                values.put("name",Et_name.getText().toString());
                values.put("value",Et_value.getText().toString());
                values.put("class",Et_class.getText().toString());
                values.put("icon",icon_drawable);
                if (Sw.isChecked()){
                    //更新数据
                    sql.update("sensor",values,"_id=?",new String[]{updata_id+""});
                }else {
                    //插入一条新数据
                    sql.insert("sensor",null,values);
                }

                sql_show(sql);
            }
        });

        //对item进行点击事件的监听
        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d("TAG", "onItemClick: "+ i);
                Cursor cursor = sql.query("sensor",null,null,null,null,null,"_id desc",null);
                cursor.moveToPosition(i); //引导cursor到指定的行
                Et_name.setText(cursor.getString(1)); //取出第一列
                Et_value.setText(cursor.getString(2));
                Et_class.setText(cursor.getString(3));
                updata_id = cursor.getInt(0);
            }
        });

        //长按item，删除一条数据
        LV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                //1. bulider（对话框的设计工具）
                AlertDialog.Builder builder = new AlertDialog.Builder(SQLActivity.this);

                builder.setTitle("警告！！！！")
                        .setMessage("确定删除吗？")
                        .setCancelable(false)
                        .setIcon(R.mipmap.ic_launcher)
                        .setNeutralButton("点击查看详细信息", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .setNegativeButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Cursor cursor = sql.query("sensor",null,null,null,null,null,"_id desc",null);
                                cursor.moveToPosition(position); //引导cursor到指定的行
                                int id = cursor.getInt(0);  //从数据库取出id号
                                sql.delete("sensor","_id=?",new String[]{""+id}); //根据id号删除数据
                                sql_show(sql);
                            }
                        }).setPositiveButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                //2. 将对话框对象和builder绑定
                AlertDialog alertDialog = builder.create();
                //3. 弹出对话框
                alertDialog.show();

                return false;
            }
        });


        //用Spinner选择图标
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        for (int i=0;i<icon_Array.length;i++){
            HashMap<String,Object> map = new HashMap<String,Object>();
            map.put("icon",icon_Array[i]);
            list.add(map);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(
                SQLActivity.this,
                list,
                R.layout.icon_layout,
                new String[]{"icon"},
                new int[]{R.id.item_icon}
        );
        SP_Icon.setAdapter(simpleAdapter);
        //监听图标的选中事件
        SP_Icon.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                icon_drawable = icon_Array[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.sqlactivity_layout_h);
            View_init();
        }
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.sqlactivity_layout);
            View_init();
        }
    }
}



// SQLite 嵌入式系统专用 .db文件
//数据库的连接 SqliteOpenHelper，调试方法，增删改查

// 查看数据库文件里有没有表  .tables
// 查看数据库表的字段和类型  .schema
// 退出数据库文件          .quit
// 创建表         create table student(name text,age text,class text);
// 插入一条数据    insert into student values("Tom","16","CT1");
// 查询           select * from student;