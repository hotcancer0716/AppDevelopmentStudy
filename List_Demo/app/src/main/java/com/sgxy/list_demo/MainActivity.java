package com.sgxy.list_demo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    GridView LV;
    String TAG = "TAGTAGTAG";
    String[] Values = {"空气温度","空气湿度","大气压力","CO2浓度","风力","风向","土壤温度","土壤湿度","土壤酸碱度","噪音分贝","PM2.5","可燃气体浓度"};
    Object[] Data = {25,70,200,79.8,3.9,"西北风",25.2,70.2,2000.2,79.8,3.5,0.3,};
    String[] Danwei = {"℃","%","ug/m","ppm","abs","uuv","&","#C","℃","%","ug/m","ppm","abs","uuv","&","#C",};

    int[] icon = {R.drawable.bxp,R.drawable.bys,R.drawable.byt,R.drawable.byu,R.drawable.byv,R.drawable.byx,R.drawable.byy,R.drawable.byz,R.drawable.c0p,R.drawable.bxp,R.drawable.bys,R.drawable.byt,R.drawable.byu,R.drawable.byv,R.drawable.byx,R.drawable.byy,R.drawable.byz,R.drawable.c0p};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LV = findViewById(R.id.mylist);

        //初始化集合List
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        //往集合里面填入数据
        for (int i =0 ; i<Values.length ; i++){
            HashMap<String,Object> map = new HashMap<String,Object>();

            map.put("k_Values",Values[i]);
            map.put("k_Data",Data[i]);
            map.put("k_Danwei",Danwei[i]);
            map.put("k_icon",icon[i]); //图标资源

            list.add(map);//不要漏掉这一行，用来往集合List里面添加数据
        }

        SimpleAdapter adapter = new SimpleAdapter(
                MainActivity.this, //要显示Activity或载体，当前Activity
                list,   //数据源集合，List
                R.layout.item_layout,  //Item的布局
                new String[]{"k_Values","k_Data","k_Danwei","k_icon"}, //数据源List的key
                new int[]{R.id.item_text_sen,R.id.item_text_value,R.id.item_text_danwei,R.id.item_img} //Item布局里的控件id，和key对应
                );
        LV.setNumColumns(2);

        LV.setAdapter(adapter);

        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "onItemClick: "+ Values[i]);
            }
        });

        LV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                //长按事件的内容
                Toast.makeText(MainActivity.this, "onItemClick: "+ Values[i], Toast.LENGTH_LONG).show();
                return false;
            }
        });
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
            LV.setNumColumns(1);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
            LV.setNumColumns(2);
    }
}



