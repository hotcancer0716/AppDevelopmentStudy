package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

public class ThirdActivity extends AppCompatActivity {
    Spinner myspinner;
    AutoCompleteTextView AutoText;
    ListView MyList;
    GridView MyGrid;
    
    String[] mylanguage = {"中国","美国","法国","英国","韩国","朝鲜","越南","俄罗斯","古巴","伊拉克","澳大利亚"};
    String[] tel = {"13354536747","137654536747","13745757567747","1389789676u5",
            "13757536768977","13745756747","1374536747","13575536747","1374536747",
            "137575736747","139789536747","1374536747","1374536747",};
   @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_third);
       
       myspinner = findViewById(R.id.spinner_language);
       AutoText = findViewById(R.id.autotext);
       MyList = findViewById(R.id.mylist);
       MyGrid = findViewById(R.id.mygrid);

       ArrayAdapter<String> myadapter = new ArrayAdapter<String>(
               ThirdActivity.this,
               android.R.layout.simple_spinner_dropdown_item,
               mylanguage
       );

       ArrayAdapter<String> Autoadapter = new ArrayAdapter<String>(
               ThirdActivity.this,
               android.R.layout.simple_spinner_dropdown_item,
               tel
       );

       myspinner.setAdapter(myadapter);

       myspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               Toast.makeText(ThirdActivity.this, ""+mylanguage[i], Toast.LENGTH_SHORT).show();
           }

           @Override
           public void onNothingSelected(AdapterView<?> adapterView) {

           }
       });

       AutoText.setAdapter(Autoadapter);

       MyList.setAdapter(myadapter);
       MyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               Toast.makeText(ThirdActivity.this, ""+mylanguage[i], Toast.LENGTH_SHORT).show();
           }
       });

       MyGrid.setAdapter(myadapter);
   }
}