package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class Edit_SQL extends AppCompatActivity {

    ImageView Edit_IMG,Pic_Left,Pic_Right,Edit_State;
    EditText Edit_Title,Edit_Message;
    Button Btn_Save;

    private int[] pic={R.drawable.c8b,R.drawable.c8d,R.drawable.c8f,R.drawable.c8i,
            R.drawable.c8k,R.drawable.c8m,R.drawable.c8n,R.drawable.c8z};
    private int[] pic_state={R.drawable.a7,R.drawable.a1,R.drawable.a8_,
            R.drawable.ayb,R.drawable.ayc,R.drawable.aye,R.drawable.b80};
    private int pic_index = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_sql);

        Edit_IMG = findViewById(R.id.item_img);
        Pic_Left = findViewById(R.id.item_left);
        Pic_Right = findViewById(R.id.item_right);
        Edit_Title = findViewById(R.id.item_text1);
        Edit_Message = findViewById(R.id.item_text2);
        Btn_Save = findViewById(R.id.item_save);

        Edit_IMG.setImageResource(pic[pic_index]);
        Pic_Right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pic_index ++;
                if (pic_index >= pic.length)
                    pic_index = 0;
                Edit_IMG.setImageResource(pic[pic_index]);
            }
        });
        Pic_Left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pic_index --;
                if (pic_index < 0)
                    pic_index = pic.length-1;
                Edit_IMG.setImageResource(pic[pic_index]);
            }
        });
        Btn_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
}