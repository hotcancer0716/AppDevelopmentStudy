package com.example.helloworld;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListActivity extends AppCompatActivity {
    GridView NewList;
    int[] pic={R.drawable.c8b,R.drawable.c8d,R.drawable.c8f,R.drawable.c8i,
            R.drawable.c8k,R.drawable.c8m,R.drawable.c8n,R.drawable.c8z};
    int[] pic2={R.drawable.a89,R.drawable.a89,R.drawable.a8_,R.drawable.a89,
            R.drawable.ayb,R.drawable.ayc,R.drawable.a89,R.drawable.a89};
    String[] Str_name= {"智能音箱","智能冰箱","智能电视","智能空调",
            "智能wifi","智能手表","智能平板","智能电脑"};
    String[] Str_year= {"一年","两年半","三年","四年"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        NewList = findViewById(R.id.newlist);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            NewList.setNumColumns(2);

        registerForContextMenu(NewList);

        //List，里面存放的HashMap
        List<Map<String,Object>> mylist = new ArrayList<Map<String,Object>>();
        for (int i=0 ;i<pic.length ; i++){
            Map<String,Object> mapItem = new HashMap<String,Object>();
            mapItem.put("pic",pic[i]);
            mapItem.put("name",Str_name[i]);
            mapItem.put("name2",Str_name[i]);
            mapItem.put("pic2",pic2[i]);

            mylist.add(mapItem);
        }

        //1.当前的Activity
        //2.数据来源，List，
        //3.Item的显示布局
        //4.key
        //5.控件id（Item布局）
        SimpleAdapter simpleAdapter = new SimpleAdapter(
                ListActivity.this,
                mylist,
                R.layout.item_layout,
                new String[]{"pic","name","name2","pic2"},
                new int[]{R.id.item_img,R.id.item_text1,R.id.item_text2,R.id.item_img2}
        );

        NewList.setAdapter(simpleAdapter);

        NewList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(ListActivity.this, ""+Str_name[i], Toast.LENGTH_SHORT).show();
            }
        });

        NewList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(ListActivity.this, "长按事件！！"+Str_name[i], Toast.LENGTH_SHORT).show();

                return false;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //把R.menu.文件加载到当前Activity
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.new_menu , menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Toast.makeText(this, "你选择的是："+item.getTitle().toString(), Toast.LENGTH_SHORT).show();
        switch (item.getItemId()){
            case R.id.menu_1:
                DatePickerDialog datePickerDialog = new DatePickerDialog(ListActivity.this);
                datePickerDialog.setTitle("请选择你的出生日期：");
                datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        Toast.makeText(ListActivity.this, "请选择的日期是"+year+"年"+(month+1)+"月"+day+"日", Toast.LENGTH_SHORT).show();
                    }
                });
                datePickerDialog.show();
                break;
            case R.id.menu_2:
                TimePickerDialog timePickerDialog = new TimePickerDialog(ListActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                        Toast.makeText(ListActivity.this, "你的出发时间为："+hour+"点"+minute+"分", Toast.LENGTH_SHORT).show();
                    }
                },6,30,true);
                timePickerDialog.setTitle("选择你的出发时间：");
                timePickerDialog.show();
                break;
            case R.id.menu_3:
                //警告的初始化
                AlertDialog.Builder builder = new AlertDialog.Builder(ListActivity.this);
                builder.setTitle("警告！！！");
                builder.setMessage("确定退出吗？");
                builder.setIcon(R.drawable.c8n);
                builder.setCancelable(false);
                builder.setNeutralButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
                builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.setPositiveButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                break;

            case R.id.menu_4:
                ProgressDialog progressDialog = new ProgressDialog(ListActivity.this);
                progressDialog.setTitle("正在加载");
                progressDialog.setMessage("请耐心等待...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressDialog.setMax(100);
                progressDialog.setIcon(R.drawable.c8i);
                progressDialog.show();
                progressDialog.setProgress(30);
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.new_menu,menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        Toast.makeText(this, "你选择的是："+item.getTitle().toString(), Toast.LENGTH_SHORT).show();
        switch (item.getItemId()){
            case R.id.menu_1:
                AlertDialog.Builder builder = new AlertDialog.Builder(ListActivity.this);
                builder.setTitle("你的练习时长是");
                builder.setSingleChoiceItems(Str_year, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(ListActivity.this, ""+Str_year[i], Toast.LENGTH_SHORT).show();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        AlertDialog.Builder builder = new AlertDialog.Builder(ListActivity.this);
        builder.setTitle("!!");
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}