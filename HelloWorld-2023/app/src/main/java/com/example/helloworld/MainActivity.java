package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText Edt_name,Edt_pw;
    Button Btn;
    String password = "123456";
    String str_name,str_pw;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        Log.d("IOT2", "=========》》onCreate: ");

        Edt_name=findViewById(R.id.textname);
        Edt_pw = findViewById(R.id.textpw);
        Btn = findViewById(R.id.mybutton);

        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //判断用户名是否为空，如果是空，弹出toast，提示输入用户名。
                //判断密码是否为空，如果是空，弹出toast，提示密码不能为空。
                //判断密码是否正确,如果不正确，弹出toast，提示密码不正确。
                str_name = Edt_name.getText().toString();
                if (str_name == null || str_name.length()==0)
                    Toast.makeText(MainActivity.this, "请输入用户名", Toast.LENGTH_SHORT).show();
                else {
                    str_pw = Edt_pw.getText().toString();
                    if (str_pw == null || str_pw.length()==0)
                        Toast.makeText(MainActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
                    else {
                        if(str_pw.equals(password)){
                            //1.获取对象，使用PreferenceManager的getDefaultSharedPreferences方法获取默认的SharedPreferences
                            SharedPreferences spf = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                            //2.创建编辑器
                            SharedPreferences.Editor editor = spf.edit();
                            //3.用键值对的形式写入到剪辑器
                            editor.putString("name",str_name);
                            editor.putString("passwd",str_pw);
                            //4.提交并保存
                            editor.commit();

                            Toast.makeText(MainActivity.this, "验证成功", Toast.LENGTH_SHORT).show();

                            //1创建Intent,并初始化
                            Intent intent = new Intent(MainActivity.this , SecondActivity.class);
                            //2创建空白的数据包Bundle
                            Bundle bundle = new Bundle();
                            //3往包里放入数据，注意格式是key-value形式，键值对
                            bundle.putString("uesr_name",str_name);
                            bundle.putString("user_pw",str_pw);
                            //4把数据包和intent绑定
                            intent.putExtras(bundle);
                            //5启动Activity
                            startActivity(intent);
                        }
                        else
                            Toast.makeText(MainActivity.this, "密码不正确", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }


    @Override
    protected void onStart() {
        Log.d("IOT2", "=======>>onStart: ");
        super.onStart();

        //1.创建对象
        SharedPreferences spf = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        //2.通过key，获取值，并显示在控件里
        Edt_name.setText(spf.getString("name",""));
        Edt_pw.setText(spf.getString("passwd",""));
   }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("IOT2", "=======>>onResume: ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("IOT2", "=======>>onPause: ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("IOT2", "=======>>onStop: ");

        str_name = Edt_name.getText().toString();
        str_pw = Edt_pw.getText().toString();
        //1.获取对象，使用PreferenceManager的getDefaultSharedPreferences方法获取默认的SharedPreferences
        SharedPreferences spf = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        //2.创建编辑器
        SharedPreferences.Editor editor = spf.edit();
        //3.用键值对的形式写入到剪辑器
        editor.putString("name",str_name);
        editor.putString("passwd",str_pw);
        //4.提交并保存
        editor.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("IOT2", "=======>>onDestroy: ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("IOT2", "=======>>onRestart: ");

    }
}