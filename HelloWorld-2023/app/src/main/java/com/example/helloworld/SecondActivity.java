package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class SecondActivity extends AppCompatActivity {

    Button btn,btnPre,btnNext;
    TextView Tx;
    ImageView Img;
    RadioButton Radman,Radwoman;

    RadioGroup Radgroup;
    ProgressBar Pgb;

    SeekBar seek;
    CheckBox Chx_dance,Chx_swim,Chx_climb,Chx_basketball;
    int []pic={R.drawable.pic01,R.drawable.pic02,R.drawable.pic03,
               R.drawable.pic04,R.drawable.pic05,R.drawable.pic06
    };

    int index = 0;
    int progree_num = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        btn = findViewById(R.id.btn_start);
        Tx = findViewById(R.id.mytx);
        btnPre = findViewById(R.id.btn_pre);
        btnNext = findViewById(R.id.btn_next);
        Img = findViewById(R.id.pic);
        Radman = findViewById(R.id.rad_man);
        Radwoman = findViewById(R.id.rad_woman);
        Radgroup = findViewById(R.id.radgroup);
        Chx_dance = findViewById(R.id.chb_dance);
        Chx_basketball = findViewById(R.id.chb_backetball);
        Chx_climb = findViewById(R.id.chb_climb);
        Chx_swim = findViewById(R.id.chb_swim);
        Pgb = findViewById(R.id.pgb);
        seek = findViewById(R.id.seek);

        //getProgress获取进度条的进度值
        progree_num = Pgb.getProgress();

        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                btn.setText(""+i);
                Pgb.setVisibility(View.VISIBLE);
                Pgb.setProgress(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String Str_like = "你的爱好是：";
                //判断单选框是否被选中
                if (Chx_dance.isChecked())
                    Str_like = Str_like + Chx_dance.getText().toString();
                if (Chx_swim.isChecked())
                    Str_like = Str_like + Chx_swim.getText().toString();
                if (Chx_climb.isChecked())
                    Str_like = Str_like + Chx_climb.getText().toString();
                if (Chx_basketball.isChecked())
                    Str_like = Str_like + Chx_basketball.getText().toString();

                Toast.makeText(SecondActivity.this, Str_like, Toast.LENGTH_SHORT).show();


            }
        });

        btnPre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                index --;
                if (index < 0 )
                    index = pic.length -1;
                Img.setImageResource(pic[index]);

                Pgb.setVisibility(View.VISIBLE);
                progree_num = progree_num - 5;
                //setProgress是设置进度表的进度
                Pgb.setProgress(progree_num);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                index ++;
                if (index >= pic.length)
                    index =0;

                Img.setImageResource(pic[index]);

                Pgb.setVisibility(View.VISIBLE);
                progree_num = progree_num + 5;
                Pgb.setProgress(progree_num);
                if (progree_num>=100)
                    Pgb.setVisibility(View.GONE);
            }
        });

        Radgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                //判断单选框是否被选中
                if (Radman.isChecked())
                    Toast.makeText(SecondActivity.this, "你选择的是"+Radman.getText().toString(), Toast.LENGTH_SHORT).show();
                if (Radwoman.isChecked())
                    Toast.makeText(SecondActivity.this, "你选择的是"+Radwoman.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
/*
        //1获取Intent，用getIntent方法
        Intent intent = getIntent();
        //2从Intent里获取数据包
        Bundle bundle =  intent.getExtras();
        //3从包里取数据
        String uesrname = bundle.getString("uesr_name","none");
        String userpw = bundle.getString("user_pw","none");

        Log.d("IOT", "用户名是: "+uesrname);
        Log.d("IOT", "用户密码: "+userpw);
        Tx.setText("用户名是: "+uesrname+"用户密码: "+userpw);
*/
    }
}