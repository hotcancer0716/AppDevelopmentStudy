package com.sgxy.ai_pic;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    TextView Tx;
    ImageView Img;

    float x1,x2;

    String TAG = "Wlei--->>";

    int index = 0;

    String[] Pic_Array = {"https://ask.zhifure.com/upload/images/2019/1/5205519769.jpg",
            "https://y.zdmimg.com/202309/30/651774ec6edf1109.jpg_a200.jpg",
            "https://pic.rmb.bdstatic.com/bjh/5562a6319fe39867be3c75b04bcdc237.png",
            "https://imgsa.baidu.com/exp/pic/item/6f4703950a7b020838dfca5d63d9f2d3572cc83d.jpg",
            "https://y.zdmimg.com/202309/24/650f8a16e31cd7530.jpg_a200.jpg",
            "https://ss2.baidu.com/-vo3dSag_xI4khGko9WTAnF6hhy/exp/w=500/sign=e32cacd4b0014a90813e46bd99763971/a8ec8a13632762d00a38b7d9a9ec08fa513dc656.jpg",
            "http://img.duoziwang.com/2019/05/08091649816304.jpg",
            "https://a.zdmimg.com/202309/28/648d3cb5e92d75970.jpg_a200.jpg"
    };

    String Pic_Url = Pic_Array[index];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Tx = findViewById(R.id.tx_message);
        Img = findViewById(R.id.img);

        Glide.with(MainActivity.this).load(Pic_Array[index]).into(Img);
    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case 1111:
                    Tx.setText(msg.getData().getString("k_name",null));
                    break;
                default:
                    break;

            }

        }
    };

    public class Thread_Net extends Thread{
        @Override
        public void run() {
            String AI_Url = "https://aip.baidubce.com/rest/2.0/image-classify/v1/classify/ingredient";
            Pic_Url = Pic_Array[index];
            String access_token = "替换你的token"; //去app.apifox.com查我的记录

            String Myurl = AI_Url+"?"+"url="+Pic_Url+"&"+"access_token="+access_token;
            Log.d(TAG, Myurl);

            Request request = new Request.Builder().url(Myurl).build();
            Call call = new OkHttpClient().newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    Log.d(TAG, "onFailure");
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    String Str_json = response.body().string();
                    Log.d(TAG, Str_json);
                    if (response.code() == 200){
                        AI_Result ai_result = new Gson().fromJson(Str_json , AI_Result.class);
                        String Str_name = ai_result.getResult().get(0).getName();

                        Log.d(TAG, "Str_name:"+Str_name );

                        //给主线程传递消息
                        Message msg = new Message();
                        msg.what = 1111;
                        Bundle bundle = new Bundle();
                        bundle.putString("k_name",Str_name);
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                    }

                }
            });
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN){
            x1 = event.getX();
        }

        if (event.getAction() == MotionEvent.ACTION_UP){
            x2 = event.getX();

            if (x2 - x1 > 50){
                //手指向右滑动了，图片应显示前一张
                index--;
                if (index<0){
                    index = Pic_Array.length-1;
                }
                Glide.with(MainActivity.this).load(Pic_Array[index]).into(Img);
            }

            if (x1 - x2 > 50){
                //手指向左滑动了，图片应显示下一张
                index++;
                if (index >= Pic_Array.length){
                    index = 0;
                }
                Glide.with(MainActivity.this).load(Pic_Array[index]).into(Img);
            }

            Thread_Net thread_net = new Thread_Net();
            thread_net.start();
        }



        return super.onTouchEvent(event);
    }
}