package com.sgxy.ai_pic;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AI_Result {
    @SerializedName("result_num")
    private Integer resultNum;
    @SerializedName("result")
    private List<ResultDTO> result;
    @SerializedName("log_id")
    private Long logId;

    public Integer getResultNum() {
        return resultNum;
    }

    public void setResultNum(Integer resultNum) {
        this.resultNum = resultNum;
    }

    public List<ResultDTO> getResult() {
        return result;
    }

    public void setResult(List<ResultDTO> result) {
        this.result = result;
    }

    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public static class ResultDTO {
        @SerializedName("score")
        private Double score;
        @SerializedName("name")
        private String name;

        public Double getScore() {
            return score;
        }

        public void setScore(Double score) {
            this.score = score;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
