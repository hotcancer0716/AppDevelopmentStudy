package com.sgxy_ict.onenet_test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttp;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    TextView textView,textView2;
    Button button;
    Switch aSwitch;
    int message_from_getgas = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);
        button = findViewById(R.id.button);
        aSwitch = findViewById(R.id.switch1);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // textView.setText("测试按键点击！");

               Thread_Onenet thread_onenet = new Thread_Onenet();
               thread_onenet.start();
            }
        });

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Thread_Onenet_switch onenet_switch = new Thread_Onenet_switch();
                onenet_switch.start();
            }
        });
    }

    class  Thread_Onenet_switch extends  Thread{
        @Override
        public void run() {
            String URL = "http://api.heclouds.com/cmds?device_id=707984602";
            String api_key = "k8SZBe2xWkCero87zV1JLiLDeAs=";
            String Command_ON = "ON";
            String Command_OFF = "OFF";

            Request request = new Request.Builder().
                    get().
                    url(URL).
                    addHeader("api-key",api_key).
                    build();

            Call call = new OkHttpClient().newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    Log.d("debug","网络访问失败");
                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    String debug = response.body().string();

                    Log.d("debug",debug);

                    Message msg = new Message();
                    msg.what = 101;
                    Bundle bundle = new Bundle();
                    bundle.putString("gas",debug);
                    msg.setData(bundle);
                    handler.sendMessage(msg);
                }
            });


        }
    }




    class Thread_Onenet extends Thread{
        @Override
        public void run() {
            //http 连接 Onenet
            String URL ="http://api.heclouds.com/devices/707984602/datastreams/temp";
            String api_key ="k8SZBe2xWkCero87zV1JLiLDeAs=";

            Request request = new Request.Builder().
                    get().
                    url(URL).
                    addHeader("api-key",api_key).
                    build();

            Call call = new OkHttpClient().newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    Log.d("debug","网络访问失败");
                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    String debug = response.body().string();
                    Log.d("HOT", "onResponse: "+debug);
                    Onenet_gas onenet_gas = new Gson().fromJson(debug,Onenet_gas.class);
                    String value = onenet_gas.getData().getCurrent_value();
                    Log.d("debug",value);

                    Message msg = new Message();
                    msg.what = message_from_getgas;
                    Bundle bundle = new Bundle();
                    bundle.putString("gas",value);
                    msg.setData(bundle);
                    handler.sendMessage(msg);
                }
            });

        }
    }


    Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case 100:
                    String vaule = msg.getData().getString("gas").toString();
                    textView.setText(vaule);
                    break;
                case 101:
                    String value = msg.getData().getString("gas").toString();
                    textView2.setText(value);
                    break;
            }
        }
    };
}
