package com.sgxy_ict.onenet_test;

public class Onenet_gas {
    /**
     * errno : 0
     * data : {"update_at":"2021-07-01 09:25:27","unit":"","id":"gas","unit_symbol":"","current_value":"40"}
     * error : succ
     */

    private int errno;
    private DataBean data;
    private String error;

    public int getErrno() {
        return errno;
    }

    public void setErrno(int errno) {
        this.errno = errno;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public static class DataBean {
        /**
         * update_at : 2021-07-01 09:25:27
         * unit :
         * id : gas
         * unit_symbol :
         * current_value : 40
         */

        private String update_at;
        private String unit;
        private String id;
        private String unit_symbol;
        private String current_value;

        public String getUpdate_at() {
            return update_at;
        }

        public void setUpdate_at(String update_at) {
            this.update_at = update_at;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUnit_symbol() {
            return unit_symbol;
        }

        public void setUnit_symbol(String unit_symbol) {
            this.unit_symbol = unit_symbol;
        }

        public String getCurrent_value() {
            return current_value;
        }

        public void setCurrent_value(String current_value) {
            this.current_value = current_value;
        }
    }
}
