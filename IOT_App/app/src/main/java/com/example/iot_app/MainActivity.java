package com.example.iot_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class MainActivity extends AppCompatActivity {
    final int get_Dev_Detail = 20001;

    TextView Debug;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Debug = findViewById(R.id.debug);

        OneNet_Thread oneNet_thread = new OneNet_Thread();
        oneNet_thread.start();
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case get_Dev_Detail:
                    Debug.setText(msg.getData().getCharSequence("data_key","null"));
                    break;

            }
        }
    };

    public class OneNet_Thread extends  Thread{
        @Override
        public void run() {
            super.run();

            //组织URL
            String url = "http://iot-api.heclouds.com/datapoint/current-datapoints";
            String Token = "version=2022-05-01&res=userid%2F120405&et=1990682368&method=md5&sign=kcevonk9s23Ab7XE0IK40A%3D%3D";
            String product_id="5Rc8K1vWml";
            String device_name="xuan001";
            String OneNet_URL = url+"?product_id="+product_id+"&device_name="+device_name;
            Log.d("TAG", OneNet_URL);

            //组织网络请求
            Request request = new Request.Builder().url(OneNet_URL).get().addHeader("Authorization",Token).build();
            //连接网络
            Call call = new OkHttpClient().newCall(request);
            //网络回调，处理Response
            call.enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    Log.d("TAG", "onFailure: ");
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    Log.d("TAG", "onResponse");
                    //得到json
                    String MyJson = response.body().string();
                    Log.d("TAG", MyJson);
                    //json解析
                    Current_datapoints cur_data = new Gson().fromJson(MyJson,Current_datapoints.class);

                    List<Current_datapoints.DataDTO.DevicesDTO.DatastreamsDTO> list =  cur_data.getData().getDevices().get(0).getDatastreams();
                    Log.d("TAG", "list 的长度为: "+ list.size());
                    for (int i =0 ;i<list.size();i++){
                        Log.d("TAG", list.get(i).getId()+" :"+list.get(i).getValue());
                    }

                    String data_Msg = cur_data.getMsg();
                    String data_code = ""+cur_data.getCode().toString();
                    String data_request_id = cur_data.getRequestId();

                    String data_key = data_Msg+"  "+data_code+"  "+data_request_id;
                    Log.d("TAG", data_key);
                    Message msg = new Message();
                    Bundle bundle = new Bundle();
                    bundle.putString("data_key",MyJson);
                    msg.setData(bundle);
                    msg.what = get_Dev_Detail;
                    handler.sendMessage(msg);
                }
            });

        }
    }
}