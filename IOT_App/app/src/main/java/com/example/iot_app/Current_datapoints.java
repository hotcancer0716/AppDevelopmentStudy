package com.example.iot_app;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Current_datapoints {
    /**
     * code
     */
    @SerializedName("code")
    private Integer code;
    /**
     * data
     */
    @SerializedName("data")
    private DataDTO data;
    /**
     * msg
     */
    @SerializedName("msg")
    private String msg;
    /**
     * requestId
     */
    @SerializedName("request_id")
    private String requestId;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public static class DataDTO {
        /**
         * devices
         */
        @SerializedName("devices")
        private List<DevicesDTO> devices;

        public List<DevicesDTO> getDevices() {
            return devices;
        }

        public void setDevices(List<DevicesDTO> devices) {
            this.devices = devices;
        }

        public static class DevicesDTO {
            /**
             * datastreams
             */
            @SerializedName("datastreams")
            private List<DatastreamsDTO> datastreams;
            /**
             * id
             */
            @SerializedName("id")
            private String id;
            /**
             * title
             */
            @SerializedName("title")
            private String title;

            public List<DatastreamsDTO> getDatastreams() {
                return datastreams;
            }

            public void setDatastreams(List<DatastreamsDTO> datastreams) {
                this.datastreams = datastreams;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public static class DatastreamsDTO {
                /**
                 * at
                 */
                @SerializedName("at")
                private String at;
                /**
                 * id
                 */
                @SerializedName("id")
                private String id;
                /**
                 * value
                 */
                @SerializedName("value")
                private Integer value;

                public String getAt() {
                    return at;
                }

                public void setAt(String at) {
                    this.at = at;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public Integer getValue() {
                    return value;
                }

                public void setValue(Integer value) {
                    this.value = value;
                }
            }
        }
    }
}
