package com.example.iot_app;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Onenet_dev_data {
    @SerializedName("code")
    private Integer code;
    @SerializedName("data")
    private DataDTO data;
    @SerializedName("msg")
    private String msg;
    @SerializedName("request_id")
    private String requestId;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public static class DataDTO {
        @SerializedName("did")
        private Integer did;
        @SerializedName("pid")
        private String pid;
        @SerializedName("access_pt")
        private Integer accessPt;
        @SerializedName("auth_info")
        private String authInfo;
        @SerializedName("data_pt")
        private Integer dataPt;
        @SerializedName("name")
        private String name;
        @SerializedName("desc")
        private String desc;
        @SerializedName("status")
        private Integer status;
        @SerializedName("create_time")
        private String createTime;
        @SerializedName("activate_time")
        private String activateTime;
        @SerializedName("last_time")
        private String lastTime;
        @SerializedName("imsi")
        private String imsi;
        @SerializedName("imei")
        private String imei;
        @SerializedName("psk")
        private String psk;
        @SerializedName("group_id")
        private String groupId;
        @SerializedName("enable_status")
        private Boolean enableStatus;
        @SerializedName("tags")
        private List<?> tags;
        @SerializedName("lat")
        private String lat;
        @SerializedName("lon")
        private String lon;
        @SerializedName("auth_code")
        private String authCode;
        @SerializedName("sec_key")
        private String secKey;
        @SerializedName("chip")
        private Integer chip;
        @SerializedName("obsv")
        private Boolean obsv;
        @SerializedName("obsv_st")
        private Boolean obsvSt;
        @SerializedName("private")
        private Boolean privateX;
        @SerializedName("imsi_old")
        private List<?> imsiOld;
        @SerializedName("imsi_mt")
        private String imsiMt;
        @SerializedName("intelligent_way")
        private Integer intelligentWay;

        public Integer getDid() {
            return did;
        }

        public void setDid(Integer did) {
            this.did = did;
        }

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public Integer getAccessPt() {
            return accessPt;
        }

        public void setAccessPt(Integer accessPt) {
            this.accessPt = accessPt;
        }

        public String getAuthInfo() {
            return authInfo;
        }

        public void setAuthInfo(String authInfo) {
            this.authInfo = authInfo;
        }

        public Integer getDataPt() {
            return dataPt;
        }

        public void setDataPt(Integer dataPt) {
            this.dataPt = dataPt;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getActivateTime() {
            return activateTime;
        }

        public void setActivateTime(String activateTime) {
            this.activateTime = activateTime;
        }

        public String getLastTime() {
            return lastTime;
        }

        public void setLastTime(String lastTime) {
            this.lastTime = lastTime;
        }

        public String getImsi() {
            return imsi;
        }

        public void setImsi(String imsi) {
            this.imsi = imsi;
        }

        public String getImei() {
            return imei;
        }

        public void setImei(String imei) {
            this.imei = imei;
        }

        public String getPsk() {
            return psk;
        }

        public void setPsk(String psk) {
            this.psk = psk;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public Boolean getEnableStatus() {
            return enableStatus;
        }

        public void setEnableStatus(Boolean enableStatus) {
            this.enableStatus = enableStatus;
        }

        public List<?> getTags() {
            return tags;
        }

        public void setTags(List<?> tags) {
            this.tags = tags;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLon() {
            return lon;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }

        public String getAuthCode() {
            return authCode;
        }

        public void setAuthCode(String authCode) {
            this.authCode = authCode;
        }

        public String getSecKey() {
            return secKey;
        }

        public void setSecKey(String secKey) {
            this.secKey = secKey;
        }

        public Integer getChip() {
            return chip;
        }

        public void setChip(Integer chip) {
            this.chip = chip;
        }

        public Boolean getObsv() {
            return obsv;
        }

        public void setObsv(Boolean obsv) {
            this.obsv = obsv;
        }

        public Boolean getObsvSt() {
            return obsvSt;
        }

        public void setObsvSt(Boolean obsvSt) {
            this.obsvSt = obsvSt;
        }

        public Boolean getPrivateX() {
            return privateX;
        }

        public void setPrivateX(Boolean privateX) {
            this.privateX = privateX;
        }

        public List<?> getImsiOld() {
            return imsiOld;
        }

        public void setImsiOld(List<?> imsiOld) {
            this.imsiOld = imsiOld;
        }

        public String getImsiMt() {
            return imsiMt;
        }

        public void setImsiMt(String imsiMt) {
            this.imsiMt = imsiMt;
        }

        public Integer getIntelligentWay() {
            return intelligentWay;
        }

        public void setIntelligentWay(Integer intelligentWay) {
            this.intelligentWay = intelligentWay;
        }
    }
}
