package com.example.iot_app;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class onenet_history_datapoit {
    @SerializedName("code")
    private Integer code;
    @SerializedName("data")
    private DataDTO data;
    @SerializedName("msg")
    private String msg;
    @SerializedName("request_id")
    private String requestId;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public static class DataDTO {
        @SerializedName("count")
        private Integer count;
        @SerializedName("datastreams")
        private List<DatastreamsDTO> datastreams;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public List<DatastreamsDTO> getDatastreams() {
            return datastreams;
        }

        public void setDatastreams(List<DatastreamsDTO> datastreams) {
            this.datastreams = datastreams;
        }

        public static class DatastreamsDTO {
            @SerializedName("datapoints")
            private List<DatapointsDTO> datapoints;
            @SerializedName("id")
            private String id;

            public List<DatapointsDTO> getDatapoints() {
                return datapoints;
            }

            public void setDatapoints(List<DatapointsDTO> datapoints) {
                this.datapoints = datapoints;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public static class DatapointsDTO {
                @SerializedName("at")
                private String at;
                @SerializedName("value")
                private Integer value;

                public String getAt() {
                    return at;
                }

                public void setAt(String at) {
                    this.at = at;
                }

                public Integer getValue() {
                    return value;
                }

                public void setValue(Integer value) {
                    this.value = value;
                }
            }
        }
    }
}
