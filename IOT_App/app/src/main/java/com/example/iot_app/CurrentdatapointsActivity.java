package com.example.iot_app;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

//current-datapoints 现在实时数据
public class CurrentdatapointsActivity extends AppCompatActivity {
    final int get_Currentdata = 20002;

    TextView Debug;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Debug = findViewById(R.id.debug);

        OneNet_Thread oneNet_thread = new OneNet_Thread();
        oneNet_thread.start();
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case get_Currentdata:
                    String cdata_id1 = msg.getData().getString("cdata_id1","null");
                    String cdata_value1 = msg.getData().getString("cdata_valude1","null");
                    String cdata_id2 = msg.getData().getString("cdata_id2","null");
                    String cdata_value2 = msg.getData().getString("cdata_valude2","null");

                    Debug.setText(cdata_id1+" : "+ cdata_value1 +"\n"+cdata_id2+" : "+ cdata_value2);
                    break;

            }
        }
    };

    public class OneNet_Thread extends  Thread{
        @Override
        public void run() {
            super.run();

            //组织URL
            String url = "https://iot-api.heclouds.com/datapoint/history-datapoints";
            String Token = "version=2022-05-01&res=userid%2F120405&et=1990682368&method=md5&sign=kcevonk9s23Ab7XE0IK40A%3D%3D";
            String product_id="5Rc8K1vWml";
            String device_name="xuan001";
            String OneNet_URL = url+"?product_id="+product_id+"&device_name="+device_name;
            //组织网络请求
            Request request = new Request.Builder().url(OneNet_URL).get().addHeader("Authorization",Token).build();
            //连接网络
            Call call = new OkHttpClient().newCall(request);
            //网络回调，处理Response
            call.enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    Log.d("TAG", "onFailure: ");
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    //得到json
                    String MyJson = response.body().string();
                    Log.d("TAG", MyJson);
                    //json解析

                   /*
                    Currentdata currentdata = new Gson().fromJson(MyJson,Currentdata.class);

                    String cdata_id1 = currentdata.getData().getDatastreams().get(0).getId().toString();
                    String cdata_valude1 = currentdata.getData().getDatastreams().get(0).getDatapoints().get(0).getValue().toString();
                    Log.d("TAG", cdata_valude1);
                    String cdata_id2 = currentdata.getData().getDatastreams().get(1).getId().toString();
                    String cdata_valude2 = currentdata.getData().getDatastreams().get(1).getDatapoints().get(0).getValue().toString();
                    Log.d("TAG", cdata_valude2);
                    //发送给handler
                    Message msg = new Message();
                    Bundle bundle = new Bundle();
                    bundle.putString("cdata_id1",cdata_id1);
                    bundle.putString("cdata_valude1",cdata_valude1);
                    bundle.putString("cdata_id2",cdata_id2);
                    bundle.putString("cdata_valude2",cdata_valude2);
                    msg.setData(bundle);
                    msg.what = get_Currentdata;
                    handler.sendMessage(msg);
                    */
                }
            });

        }
    }
}