package com.sgxy.webview_test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button BTN_go;
    WebView WB;

    AutoCompleteTextView ET_Url;

    //1.组织数据
    String[] url_data={"www.baidu.com",
            "www.sgu.edu.cn",
            "www.zhihu.com",
            "www.bilibili.com",
            "www.sg.gov.cn",
            "www.sg.gov.cn/czj/gkmlpt/index",
            "open.iot.10086.cn",
            "hunanapp.weather.com.cn/mweather/index.shtml?aid=101280209&location=now",
            "lmg.jj20.com/up/allimg/1112/112GPR626/1Q12FR626-9-1200.jpg",
            "www.cnki.net"};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ET_Url = findViewById(R.id.web_edit);
        BTN_go = findViewById(R.id.web_btn);
        WB = findViewById(R.id.web);

        WB.getSettings().setJavaScriptEnabled(true); //支持JavaScript
        WB.setWebViewClient(new WebViewClient()); //设置当前为默认浏览器
        WB.loadUrl("file:///android_asset/xxx.html");
        //2.初始化适配器
        ArrayAdapter adapter = new ArrayAdapter(MainActivity.this , android.R.layout.simple_list_item_1 ,url_data);
        //3.绑定控件
        ET_Url.setAdapter(adapter);
        BTN_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WB.loadUrl("https://"+ET_Url.getText().toString());
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (WB.canGoBack() && keyCode == KeyEvent.KEYCODE_BACK ){
            WB.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}